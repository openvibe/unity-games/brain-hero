# How To Add Resources

## Crosses (Folder `Crosses`)

**Filename** : `XX-Name Of My Cross.png` (only png are available)  
XX is the file number, identical numbers are allowed but strongly discouraged to avoid confusion.  
The file name will be displayed in the various menus (without the number) without changing the case.  
Each crosses is a 256x256 size image file. The background must be transparent (white will not be replaced by transparent if necessary to allow white crosses).  
*NB: It is advisable to leave the original files and add yours afterwards.*

## Partition (Folder `Partitions`)

Partition must respect rules of creations.

**Filename** : `XX-Name Of My Partition.json`  (only json are available)  
XX is the file number, identical numbers are allowed but strongly discouraged to avoid confusion.  
The file name will be displayed in the selection of predefined partitions (from number 01) without changing the case.  
`00 Procedural.json` should not be replaced as it will be recreated automatically.  
`Current.json` is the last run started.  
*NB: It is advisable to leave the original files and add yours afterwards.*

## Sounds (Folder `Sounds`)

**Filename** : `XX-Name Of My Sound.ext` (available extension are ....)  
XX is the file number, identical numbers are allowed but strongly discouraged to avoid confusion.  
The file name will be displayed in the various menus (without the number) without changing the case.  
*NB: It is advisable to leave the original files and add yours afterwards.*

## Visuals (Folder `Visuals`)

**Filename** : `XX-Name Of My Visual.png` (only png are available)  
XX is the file number, identical numbers are allowed but strongly discouraged to avoid confusion.  
The file name will be displayed in the various menus (without the number) without changing the case.  
Each visuals is a 256x256 size image file. The background must be transparent (white will not be replaced by transparent if necessary to allow white visuals).  
*NB: It is advisable to leave the original files and add yours afterwards.*
