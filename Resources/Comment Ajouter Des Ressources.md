# Comment Ajouter Des Ressources

## Croix (Dossier `Crosses`)

**Nom de fichier** : `XX-Nom De Ma Croix.png` (Seuls les png sont valides)  
XX est le numéro du fichier, les numéros identiques sont autorisés mais fortement déconseillé pour éviter des confusions.  
Le nom de fichier sera affiché dans les dufférents menus (sans le numéro) sans modification de la casse.  
Chaque réticule est un fichier image de taille 256x256. Le fond doit être transparent (le blanc ne sera pas remplacé par du transparent le cas échéant pour autoriser les réticules blancs).  
*NB : Il est conseillé de laisser les fichiers d'origines et d'ajouter les votres à la suite.*

## Partitions (Dossier `Partitions`)

Les partitions doivent respecter les règles de créations.

**Nom de fichier** : `XX-Nom De Ma Partition.json` (Seuls les json sont valides)  
XX est le numéro du fichier, les numéros identiques sont autorisés mais fortement déconseillé pour éviter des confusions.  
Le nom de fichier sera affiché dans la sélection de partitions prédéfinies (à partir du numéro 01) sans modification de la casse.  
`00 Procedural.json` ne doit pas être remplacé car il sera recréé automatiquement.  
`Current.json` est la dernière course lancée.  
*NB : Il est conseillé de laisser les fichiers d'origines et d'ajouter les votres à la suite.*

## Sounds (Dossier `Sounds`)

**Nom de fichier** : `XX-Nom Du Son.ext` (les extensiosn valides sont ...)  
XX est le numéro du fichier, les numéros identiques sont autorisés mais fortement déconseillé pour éviter des confusions.  
Le nom de fichier sera affiché dans les dufférents menus (sans le numéro) sans modification de la casse.  
*NB : Il est conseillé de laisser les fichiers d'origines et d'ajouter les votres à la suite.*

## Visuels (Dossier `Visuals`)

**Nom de fichier** : `XX-Nom Du Visuel.png` (Seuls les png sont valides)  
XX est le numéro du fichier, les numéros identiques sont autorisés mais fortement déconseillé pour éviter des confusions.  
Le nom de fichier sera affiché dans les dufférents menus (sans le numéro) sans modification de la casse.  
Chaque Visuel est un fichier image de taille 256x256. Le fond doit être transparent (le blanc ne sera pas remplacé par du transparent).  
*NB : Il est conseillé de laisser les fichiers d'origines et d'ajouter les votres à la suite.*
