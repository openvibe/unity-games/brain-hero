# -*- coding: utf-8 -*-
##
##   [2017] Fabien Lotte - Inria
##

from __future__ import print_function, division

import os
import csv

import numpy as np
import matplotlib.pyplot as plt

from scipy.signal import savgol_filter 
from datetime import datetime


class FrequencyBandSelectionBox(OVBox):
	def __init__(self):
		super(FrequencyBandSelectionBox, self).__init__()
		self.num_channels  = 0
		self.sampling_rate = 0
		self.num_samples   = 0
		self.last_time	 = 0
		self.stimulations  = []
		self.currentLabel  = -1
		self.labels		= []
		self.nbTrainData   = 0
		self.outputStimCode = 33287; #code for output stimulation 'OVTK_StimulationId_TrainCompleted'

	def initialize(self):
		assert len(self.input)  == 2,  'This box needs exactly 2 input'
		assert len(self.output) == 1,   'This box needs exactly 1 output'
		
		stim_label1	   = self.setting['Class 1 stimulation label']
		stim_label2	   = self.setting['Class 2 stimulation label']
		stim_train	   = self.setting['Train Stimulation']
		self.minFreq   = int(self.setting['MinFrequency'])
		self.maxFreq   = int(self.setting['MaxFrequency'])
		self.subjectID = self.setting['Subject ID']
		self.session   = self.setting['Session']
		self.filename  = self.setting['Output config filename']
	
		self.folder    = os.path.dirname(self.filename)
		self.parent    = os.path.abspath(os.path.join(self.folder, os.pardir))
		
		self.time = datetime.now().strftime("[%Y.%m.%d-%H.%M.%S]")

		self.codeClass1   = OpenViBE_stimulation[stim_label1]
		self.codeClass2   = OpenViBE_stimulation[stim_label2]
		self.codeTrain    = OpenViBE_stimulation[stim_train]  
		self.stimulations = []

	def uninitialize(self):
		pass

	def process(self):
		if not self.input[0] and not self.input[1]:
			return

		# obtain all stimulations
		stimulations = self.stimulations[:]
		while self.input[1]:
			chunk = self.input[1].pop()
			if type(chunk) == OVStimulationSet:
				for stim in chunk:					
					stimulations.append((stim.date,stim.identifier))
					if stim.identifier==self.codeClass1:
						self.currentLabel = -1
					elif stim.identifier==self.codeClass2:
						self.currentLabel = 1

					if stim.identifier==self.codeTrain:
						#print('Training time...')
						#print('Nb trials:' + str(self.nbTrainData))
						
						#1 - computing the correlation between each frequency band power and the class labels, for each channel
						scoreC = np.zeros((self.nbFreqs,self.num_channels))
						for f in range(self.minFreqIdx,self.maxFreqIdx+1):
							for c in range(self.num_channels):
								r2 = np.corrcoef(self.spectrums[:,f,c],self.labels)
								scoreC[f,c] = r2[0,1]

						#2 - identifying the best frequency for discrimination 
						sumScoreC = np.sum(np.abs(scoreC),1)

						fmax = sumScoreC.argmax()

						# print results
						#print(fmax, sumScoreC)
						
						#3 - checking whether this best correlation is positive or negative
						scoreCStar = np.zeros((self.nbFreqs,self.num_channels))
						for c in range(self.num_channels):
							if scoreC[fmax,c]>0:
								scoreCStar[:,c] = scoreC[:,c]
							else:
								scoreCStar[:,c] = -scoreC[:,c]

						#4 - computing the overall score for each frequency
						fScore = np.sum(scoreCStar,1)
						fScoreSmooth = savgol_filter(fScore, 3, 1)
						fMaxStar = sumScoreC.argmax()

						print('...done!')
						print('Best individual frequency = ' + str(self.freq[fMaxStar]) + ' score: ' + str(fScore[fMaxStar]))
					
						f0 = fMaxStar
						f1 = fMaxStar

						#5 - computing the min and max of the best band
						delta = 0.05
						while fScore[f0 - 1] >= (fScore[fMaxStar] * delta): f0 = f0 - 1
						while fScore[f1 + 1] >= (fScore[fMaxStar] * delta): f1 = f1 + 1

						#5bis - Adjustment : We add 0.5Hz margin on each side (to avoid a pass-band with a single frequency if f0=f1)
						freqMin = str(self.freq[f0] - 0.5)
						freqMax = str(self.freq[f1] + 0.5)			
						print('Best band = ' + freqMin + '-' + freqMax + ' Hz') 

						#5ter - Show and Save Vizualisation
						fig, (ax1, ax2) = plt.subplots(2,1)
						fig.set(figwidth=16, figheight=9)
						ax1.plot(self.freq[range(self.minFreqIdx,self.maxFreqIdx+1)],fScore[range(self.minFreqIdx,self.maxFreqIdx+1)])
						ax1.axvline(x=float(freqMin), color='r', linestyle='--', linewidth = 1)
						ax1.axvline(x=float(freqMax), color='r', linestyle='--', linewidth = 1)
						ax1.set_title("Frequency Score")
						#ax1.xaxis.grid(True)
						ax2.plot(self.freq[range(self.minFreqIdx,self.maxFreqIdx+1)],fScoreSmooth[range(self.minFreqIdx,self.maxFreqIdx+1)])
						ax2.set_title("Frequency Score Smooth")
						ax2.axvline(x=float(freqMin), color='r', linestyle='--', linewidth = 1)
						ax2.axvline(x=float(freqMax), color='r', linestyle='--', linewidth = 1)
						#ax2.xaxis.grid(True)
						plt.show()
						fig.savefig(self.parent + '/Signals/' + self.subjectID +'_'+ self.session + '_FrequencyScore-' + self.time + '.png')


						#6 - saving the obtained frequency band parameters to a config file for a temporal filter
						strToWrite = """<OpenViBE-SettingsOverride>
											<SettingValue>Band pass</SettingValue>
											<SettingValue>5</SettingValue>
											<SettingValue>""" + freqMin + """</SettingValue>
											<SettingValue>""" + freqMax + """</SettingValue>
										</OpenViBE-SettingsOverride>"""
						file  = open(self.filename, 'w')
						file.write(strToWrite)
						file.close()
						print('Config file saved!')

						#6bis - saving fScore
						d=[self.freq, sumScoreC, fScore]
						export_data=zip(*d)
						with open(self.parent + '/Signals/' + self.subjectID +'_'+ self.session + '_FScore-' + self.time + '.csv', 'w') as file:
							writer = csv.writer(file, lineterminator = '\n')
							writer.writerow(("Frequency", "SumScoreC", "FScore"))
							writer.writerows(export_data)
						file.close()
						
						#6Ter - saving frequencies
						with open(self.folder + '/' + 'Frequencies selected.csv', 'a') as file:
							writer = csv.writer(file, lineterminator = '\n')
							#Date, Subject, Session, Score, Best, Ferquency min, Frequency Max
							writer.writerow((datetime.now().strftime("%d/%m/%y %H:%M:%S"), self.subjectID, self.session, str(fScore[fMaxStar]), str(self.freq[fMaxStar]), freqMin, freqMax))
						file.close()

						#7 - sending a stimulation to indicate that the training is complete
						stimSet = OVStimulationSet(self.getCurrentTime(), self.getCurrentTime()+1./self.getClock())
						stimSet.append(OVStimulation(self.outputStimCode, self.getCurrentTime(), 0.))
						self.output[0].append(stimSet) # Send chunk
						

		# obtain all chunks of signals received in the first input		
		while self.input[0]:
			chunk = self.input[0].pop()

			if type(chunk) == OVSignalHeader:

				# handle header: we are sending the same information
				# so the output header should be the same
				input_header  = chunk
				self.sampling_rate = input_header.samplingRate
				self.num_channels  = input_header.dimensionSizes[0]
				self.num_samples   = input_header.dimensionSizes[1]

				#comuting an hamming window of the size of the chunks and the frequency of samples of the corresponding FFT
				self.hamWindow = np.hamming(self.num_samples)
				self.freq = np.fft.rfftfreq(self.num_samples,1./self.sampling_rate)
				self.nbFreqs = self.freq.size

				#identifying the index of the min and max frequencies to investigate
				self.minFreqIdx = (np.abs(self.freq-self.minFreq)).argmin()
				self.maxFreqIdx = (np.abs(self.freq-self.maxFreq)).argmin()
								
			elif type(chunk) == OVSignalBuffer:

				input_signal = np.reshape(chunk, (self.num_channels, self.num_samples)) #getting the EEG chunk

				#computing the Power Spectral Density using FFT, for each channel
				psds = np.zeros((1,self.nbFreqs,self.num_channels))
				for c in range(self.num_channels):
					psds[0,:,c] = np.log(np.abs(np.fft.rfft(input_signal[c,:]*self.hamWindow,self.num_samples))**2); #PSD based on FFT

				#storing them
				if self.nbTrainData == 0:
					self.spectrums = psds
				else:
					self.spectrums = np.append(self.spectrums,psds,axis=0)

				self.nbTrainData+=1
				self.labels.append(self.currentLabel)
						

box = FrequencyBandSelectionBox()
