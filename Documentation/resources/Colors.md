# Color List

| Name   |  T   S   L  |  R   V   B  |  Hexa   | Example |
| :----- | :---------: | :---------: | :-----: | :------:|
| Rouge  | 000 065 085 | 217 076 076 | #d94c4c | <span style="background-color:#d94c4c;color:#d94c4c">AAA</span> |
| Orange | 040 065 085 | 217 170 076 | #d9aa4c | <span style="background-color:#d9aa4c;color:#d9aa4c">AAA</span> |
| Vert   | 120 065 085 | 076 217 076 | #4cd94c | <span style="background-color:#4cd94c;color:#4cd94c">AAA</span> |
| Menthe | 160 065 085 | 076 217 170 | #4cd9aa | <span style="background-color:#4cd9aa;color:#4cd9aa">AAA</span> |
| Ciel   | 200 065 085 | 076 170 217 | #4caad9 | <span style="background-color:#4caad9;color:#4caad9">AAA</span> |
| Bleu   | 240 065 085 | 076 076 217 | #4c4cd9 | <span style="background-color:#4c4cd9;color:#4c4cd9">AAA</span> |
| Violet | 280 065 085 | 170 076 217 | #aa4cd9 | <span style="background-color:#aa4cd9;color:#aa4cd9">AAA</span> |
| Fushia | 320 065 085 | 217 076 170 | #d94c4c | <span style="background-color:#d94caa;color:#d94caa">AAA</span> |
