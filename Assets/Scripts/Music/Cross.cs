﻿namespace Music
{
/// <summary> Class for Cross Beat (show a cross on screen). </summary>
/// <seealso cref="Music.Beat" />
[System.Serializable] public class Cross : Beat
{
	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="T:Music.Cross" /> class. </summary>
	/// <param name="duration"> The duration of the Beat. </param>
	/// <param name="stimulation"> The stimulation to send with the Beat (by default <c>-1</c> so none). </param>
	/// <inheritdoc />
	public Cross(float duration = 1.0F, int stimulation = -1) : base(duration, stimulation) { Type = "Cross"; }
}
}
