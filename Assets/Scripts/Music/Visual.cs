﻿namespace Music
{
/// <summary> Class for Visual Beat (show a visual as instruction on screen). </summary>
/// <seealso cref="Music.Beat" />
[System.Serializable] public class Visual : Beat
{
	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="T:Music.Visual" /> class. </summary>
	/// <param name="duration"> The duration of the Beat. </param>
	/// <param name="stimulation"> The stimulation to send with the Beat. </param>
	/// <param name="visualIdx"> Index of the visual. </param>
	/// <param name="soundIdx"> Index of the sound. </param>
	/// <inheritdoc />
	public Visual(float duration = 1.0F, int stimulation = -1, int visualIdx = -1, int soundIdx = -1) : base(duration, stimulation, -1, visualIdx, soundIdx)
	{
		Type = "Visual";
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts to string. </summary>
	/// <returns> A <see cref="System.String" /> that represents this instance. </returns>
	public override string ToString() { return Type + " (" + VisualIdx + ", " + SoundIdx + ") : " + Duration + "s : " + Stimulation; }
}
}
