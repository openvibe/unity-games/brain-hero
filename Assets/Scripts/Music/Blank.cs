﻿namespace Music
{
/// <summary> Class for Blank Beat (for rest for example). </summary>
/// <seealso cref="Music.Beat" />
[System.Serializable] public class Blank : Beat
{
	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="T:Music.Blank" /> class. </summary>
	/// <param name="duration"> The duration of the Beat. </param>
	/// <param name="stimulation"> The stimulation to send with the Beat (by default <c>-1</c> so none). </param>
	/// <param name="type"> Type of blank Type (by default <c>Blank</c>). </param>
	/// <inheritdoc />
	public Blank(float duration = 1.0F, int stimulation = -1, string type = "Blank") : base(duration, stimulation) { Type = type; }
}
}
