﻿namespace Music
{
//public enum BeatType { Blank, Cross, Note, Visual }
/// <summary> Base Class for Beat in Music Sheet. </summary>
[System.Serializable] public class Beat
{
	public string Type;
	public float  Duration;
	public int    Stimulation, NoteIdx, VisualIdx, SoundIdx;

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="Beat"/> class. </summary>
	/// <param name="duration"> The duration of the Beat. </param>
	/// <param name="stimulation"> The stimulation to send with the Beat. </param>
	/// <param name="noteIdx"> Index of the note. </param>
	/// <param name="visualIdx"> Index of the visual. </param>
	/// <param name="soundIdx"> Index of the sound. </param>
	public Beat(float duration = 0.0F, int stimulation = -1, int noteIdx = -1, int visualIdx = -1, int soundIdx = -1)
	{
		Type        = "Beat";
		Duration    = duration;
		Stimulation = stimulation;
		NoteIdx     = noteIdx;
		VisualIdx   = visualIdx;
		SoundIdx    = soundIdx;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts to string. </summary>
	/// <returns> A <see cref="System.String" /> that represents this instance. </returns>
	public override string ToString() { return Type + " : " + Duration + "s : " + Stimulation; }
}
}
