using System.Collections.Generic;
using System.Linq;
using SerializeField = UnityEngine.SerializeField;
using Debug = UnityEngine.Debug;
using JsonUtility = UnityEngine.JsonUtility;
using Random = UnityEngine.Random;
using CultureInfo = System.Globalization.CultureInfo;
using Stimulations = LSL4Unity.OV.Stimulations;

namespace Music
{
/// <summary> Classe to Manage, Save, Create Sheet. </summary>
[System.Serializable] public class Sheet
{
	#region Members

	// Generic settings
	public string Name        = "Sheet";
	public string Description = "Sheet Description";
	public string Date        = System.DateTime.Now.ToString(CultureInfo.CurrentCulture);

	public bool ShowFinalScreen    = true;
	public bool ShowScoreBar       = true;
	public int  ScoreDisplay       = 0;
	public int  FeedbackDisplay    = 0;
	public int  ContinuousFeedback = 0;
	public int  DiscreteFeedback   = 0;

	public int ContinuousSmoothPrecision      = 0;
	public int DiscreteGrownTargetSteps       = 0;
	public int DiscreteGrownTargetWindowSize  = 0;
	public int DiscreteGrownTargetFailAllowed = 0;
	public int DiscreteGrownTargetDownMode    = 0;

	public                  float               Speed    = 3.0F;
	public                  bool                Sound    = true;
	public                  int                 CrossIdx = -1;
	[SerializeField] public List<Settings.Note> Notes    = new List<Settings.Note>(); //must be SerializeField for JsonUtility and List<T>
	[SerializeField] public List<Beat>          Beats    = new List<Beat>();          //must be SerializeField for JsonUtility and List<T>

	// Static beats
	private static readonly Blank StartExperimentBeat = new Blank(5.0F, (int) Stimulations.EXPERIMENT_START,   "Experiment Start");
	private static readonly Blank TrainBeat           = new Blank(5.0F, (int) Stimulations.TRAIN,              "Train");
	private static readonly Blank StopExperimentBeat  = new Blank(1.0F, (int) Stimulations.EXPERIMENT_STOP,    "Experiment Stop");
	private static readonly Blank StartTrialBeat      = new Blank(0.0F, (int) Stimulations.GDF_START_OF_TRIAL, "Trial Start");
	private static readonly Blank StopTrialBeat       = new Blank(0.0F, (int) Stimulations.GDF_END_OF_TRIAL,   "Trial Stop");

	#endregion

	#region Music Sheet Creation

	//----------------------------------------------------------------------------------------------------
	/// <summary> Adds the note to the list. </summary>
	/// <param name="note"> The note. </param>
	public void AddNote(Settings.Note note)
	{
		note.Stimulation = Settings.Defines.StimulationsDictionary[Settings.Defines.Stimulations[note.Stimulation]];
		Notes.Add(note);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Clears the sheet. </summary>
	private void ClearSheet() { Beats.Clear(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Begins the sheet with a Experiment Start Beat. </summary>
	private void BeginSheet() { Beats.Add(StartExperimentBeat); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Ends the sheet with a Experiment Stop Beat. </summary>
	private void EndSheet()
	{
		Beats.Add(TrainBeat);
		Beats.Add(StopExperimentBeat);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Generates the free Sheet. </summary>
	/// <param name="crossDuration"> Duration of the cross. </param>
	/// <param name="totalDuration"> Duration of the free. </param>
	/// <param name="restDuration"> Duration of rest between parts. </param>
	/// <param name="steps"> Number of parts. </param>
	public void GenerateFree(float crossDuration, float totalDuration, float restDuration, int steps)
	{
		Name        = "Free Sheet";
		Description = "Free Sheet Automatically Generated";
		Date        = System.DateTime.Now.ToString(CultureInfo.CurrentCulture);

		ClearSheet();
		BeginSheet();

		var totalRest = restDuration * steps;
		if (totalRest > totalDuration) { totalDuration = totalRest + 1; } // Enough to avoid bug but bad Sheet
		var trialD = (totalDuration - totalRest) / steps;
		for (var i = 0; i < steps; ++i) {
			AddTrial(new Cross(crossDuration, (int) Stimulations.GDF_CROSS_ON_SCREEN), new Visual(0), new Note(trialD, (int) Stimulations.GDF_BEEP),
					 new Blank(restDuration, -1, "Rest"));
		}

		EndSheet();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Generates the procedural Sheet. </summary>
	/// <param name="trialNumber"> The trial number. </param>
	/// <param name="crossDuration"> Duration of the cross. </param>
	/// <param name="visualDuration"> Duration of the visual. </param>
	/// <param name="feedbackDurationMin"> The feedback duration minimum. </param>
	/// <param name="feedbackDurationMax"> The feedback duration maximum. </param>
	/// <param name="restDurationMin"> The rest duration minimum. </param>
	/// <param name="restDurationMax"> The rest duration maximum. </param>
	public void GenerateProcedural(int   trialNumber,     float crossDuration, float visualDuration, float feedbackDurationMin, float feedbackDurationMax,
								   float restDurationMin, float restDurationMax)
	{
		Name        = "Procedural Sheet";
		Description = "Procedural Sheet Automatically Generated";
		Date        = System.DateTime.Now.ToString(CultureInfo.CurrentCulture);

		//feedbackStimulation depend of feedback compute methode (a GetStim or other way)
		const int feedbackStimulation = (int) Stimulations.GDF_FEEDBACK_CONTINUOUS; // For continuous feedback
		//const int FeedbackStimulation = (int)LSL4Unity.OV.Stimulations.GDF_FEEDBACK_DISCRETE;	 // For discrete feedback
		var cross = new Cross(crossDuration, (int) Stimulations.GDF_CROSS_ON_SCREEN);

		// Sheet Generation
		ClearSheet();
		BeginSheet();
		var sequence = GenerateRandomsequence(Notes.Count, trialNumber);
		foreach (var s in sequence) {
			AddTrial(cross, new Visual(visualDuration, Notes[s].Stimulation, Notes[s].Visual, Notes[s].Sound),
					 new Note(Random.Range(feedbackDurationMin, feedbackDurationMax), feedbackStimulation, s),
					 new Blank(Random.Range(restDurationMin,    restDurationMax), -1, "Rest"));
		}
		EndSheet();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Adds a classical trial sequence. </summary>
	/// <param name="cross"> The cross beat. </param>
	/// <param name="visual"> The visual beat. </param>
	/// <param name="note"> The note beat. </param>
	/// <param name="rest"> The rest beat. </param>
	private void AddTrial(Cross cross, Visual visual, Note note, Blank rest)
	{
		Beats.Add(StartTrialBeat);
		Beats.Add(cross);
		Beats.Add(visual);
		Beats.Add(note);
		Beats.Add(StopTrialBeat);
		Beats.Add(rest);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Generates a randomize sequence for MT-BCI task (in O(n) complexity). </summary>
	/// <param name="noteNumber"> The number of notes. </param>
	/// <param name="trialNumber"> The number of trial per notes. </param>
	/// <returns> The randomized sequence. </returns>
	private static List<int> GenerateRandomsequence(int noteNumber, int trialNumber)
	{
		// Init
		var res = new List<int>();
		for (var i = 0; i < trialNumber; ++i) {
			for (var j = 0; j < noteNumber; ++j) { res.Add(j); }
		}

		// O(n) Randomization
		var count = res.Count;
		var last  = count - 1;
		for (var i = 0; i < last; ++i) {
			var j = Random.Range(i, count);
			(res[i], res[j]) = (res[j], res[i]);
		}

		return res;
	}

	#endregion

	#region Save/Load/Print

	//----------------------------------------------------------------------------------------------------
	/// <summary> Saves the sheet on the specified filename. </summary>
	/// <param name="filename"> The filename. </param>
	public void Save(string filename)
	{
		Debug.Log($"Save {filename}...");
		Debug.Log(JsonUtility.ToJson(this,                                                            true));
		System.IO.File.WriteAllText(Settings.Defines.GetSheetPath(filename), JsonUtility.ToJson(this, true));
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the sheet on specified filename. </summary>
	/// <param name="filename"> The filename. </param>
	public void Load(string filename)
	{
		Debug.Log($"Before Load {filename} : ");
		JsonUtility.FromJsonOverwrite(System.IO.File.ReadAllText(Settings.Defines.GetSheetPath(filename)), this);
		Debug.Log($"After Load {filename} : ");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts to string. </summary>
	/// <returns> A <see cref="System.String" /> that represents this instance. </returns>
	public override string ToString()
	{
		var res = "Music Sheet : " + SettingToString() + "\n\t Notes : \n";
		res =  Notes.Aggregate(res, (current, note) => current + "\t\t" + note + "\n");
		res += "\n\t Beats : \n";
		res =  Beats.Aggregate(res, (current, beat) => current + "\t\t" + beat + "\n");

		return res;
	}

	public string SettingToString(bool breakline = true, string begin = "\t")
	{
		var start = (breakline ? "\n" : "") + begin;
		return $"{start}Name : {Name}{start}Description : {Description}{start}Date : {Date}" +
			   $"{start}Show Final Screen : {ShowFinalScreen}{start}Show Score Bar : {ShowScoreBar}" +
			   $"{start}Score Display Method : {ScoreDisplay} - {Settings.Defines.ScoreDisplay[ScoreDisplay]}" +
			   $"{start}Feedback Display Method : {FeedbackDisplay} - {Settings.Defines.FeedbacksDisplay[FeedbackDisplay]}" +
			   $"{start}Continuous Feedback Method : {ContinuousFeedback} - {Settings.Defines.FeedbacksContinuous[ContinuousFeedback]}" +
			   $"{start}Continuous Feedback Smooth Precision : {ContinuousSmoothPrecision}" +
			   $"{start}Discrete Feedback Method : {DiscreteFeedback} - {Settings.Defines.FeedbacksDiscrete[DiscreteFeedback]}" +
			   $"{start}Discrete Feedback Setting : Steps - {DiscreteGrownTargetSteps}, " +
			   $"Window Size - {DiscreteGrownTargetWindowSize}, Fail Allowed - {DiscreteGrownTargetFailAllowed}, " +
			   $"Down Mode - {DiscreteGrownTargetDownMode} ({Settings.Defines.DownGradeModes[DiscreteGrownTargetDownMode]})" +
			   $"{start}Discrete Feedback Setting : {DiscreteFeedback} - {Settings.Defines.FeedbacksDiscrete[DiscreteFeedback]}" +
			   $"{start}Speed : {Speed}{start}Sound : {Sound}{start}Cross : {CrossIdx} - {Settings.Defines.Crosses[CrossIdx].Name}";
	}

	public string DescriptionToString() { return $"Name : {Name}\nDate : {Date}\nDescription : {Description}"; }

	#endregion
}
}
