﻿namespace Music
{
/// <summary> Class for Note Beat (for time when we must try to make note). </summary>
/// <seealso cref="Music.Beat" />
[System.Serializable] public class Note : Beat
{
	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="T:Music.Note" /> class. </summary>
	/// <param name="duration"> The duration of the Beat. </param>
	/// <param name="stimulation"> The stimulation to send with the Beat. </param>
	/// <param name="noteIdx"> Index of the note. </param>
	/// <inheritdoc />
	public Note(float duration = 1.0F, int stimulation = -1, int noteIdx = -1) : base(duration, stimulation, noteIdx) { Type = "Note " + NoteIdx; }
}
}
