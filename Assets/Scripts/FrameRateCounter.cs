﻿using System.Collections;
using UnityEngine;

public class FrameRateCounter : MonoBehaviour
{
	private float _updateCount      = 0;
	private float _fixedUpdateCount = 0;
	private float _lateUpdateCount  = 0;
	private float _updateUpdateCountPerSecond;
	private float _updateFixedUpdateCountPerSecond;
	private float _updateLateUpdateCountPerSecond;

	private void Awake()
	{
		// Uncommenting this will cause framerate to drop to 10 frames per second.
		// This will mean that FixedUpdate is called more often than Update.
		//Application.targetFrameRate = 10;
		StartCoroutine(Loop());
	}

	// Increase the number of calls to Update.
	private void Update() { _updateCount += 1; }

	// Increase the number of calls to FixedUpdate.
	private void FixedUpdate() { _fixedUpdateCount += 1; }

	// Increase the number of calls to LateUpdate.
	private void LateUpdate() { _lateUpdateCount += 1; }

	// Show the number of calls to both messages.
	private void OnGUI()
	{
		var fontSize = new GUIStyle(GUI.skin.GetStyle("label")) { fontSize = 24 };
		GUI.Label(new Rect(100, 100, 200, 50), "Update: " + _updateUpdateCountPerSecond,           fontSize);
		GUI.Label(new Rect(100, 150, 200, 50), "FixedUpdate: " + _updateFixedUpdateCountPerSecond, fontSize);
		GUI.Label(new Rect(100, 200, 200, 50), "LateUpdate: " + _updateLateUpdateCountPerSecond,   fontSize);
	}

	// Update both CountsPerSecond values every second.
	private IEnumerator Loop()
	{
		while (true) {
			yield return new WaitForSeconds(1);
			_updateUpdateCountPerSecond      = _updateCount;
			_updateFixedUpdateCountPerSecond = _fixedUpdateCount;
			_updateLateUpdateCountPerSecond  = _lateUpdateCount;

			_updateCount      = 0;
			_fixedUpdateCount = 0;
			_lateUpdateCount  = 0;
		}
	}
}
