﻿using System.Collections.Generic;

namespace Game
{
/// <summary> SheetPlayer is the class to manage the rythm during the game. </summary>
public static class SheetPlayer
{
	#region Members

	public static  Music.Sheet Sheet     { get; private set; } = null;              // Sheet to play
	public static  List<float> NoteTimes { get; }              = new List<float>(); // List of expected notes (at beginning and ending of Continuous feedback)
	private static float       _totalTime   = float.MaxValue;                       // Time total of the sheet
	private static float       _startTime   = 0.0F;                                 // Start time of sheet (Unity time when we start play)
	private static float       _nextTime    = 0.0F;                                 // Time of next beat
	private static int         _currentBeat = 0;                                    // Current beat index
	private static bool        _isStarted   = false;                                // IsStarted toggle
	public static  bool        IsInit       = false;                                // IsInit toggle

	#endregion

	#region Initialization

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initialize the Sheet Player. </summary>
	public static void Init()
	{
		ResetMembers();
		Sheet = Settings.SettingsManager.Sheet;
		SheetChecker();
		IsInit = true;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset All Values. </summary>
	public static void Destroy() { ResetMembers(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset All members. </summary>
	private static void ResetMembers()
	{
		Sheet = null;
		NoteTimes.Clear();
		_startTime   = _nextTime = 0.0F;
		_totalTime   = float.MaxValue;
		_currentBeat = 0;
		IsInit       = false;
		_isStarted   = false;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Check if Sheet is valide with current Setting. </summary>
	private static void SheetChecker()
	{
		//Cross Checker
		ValidateValue(ref Sheet.CrossIdx, Settings.Defines.Crosses.Count - 1, "Cross Index");

		//Note Checker
		foreach (var n in Sheet.Notes) {
			ValidateValue(ref n.Visual, Settings.Defines.Visuals.Count - 1, "Visual Index");
			ValidateValue(ref n.Color,  Settings.Defines.Colors.Count - 1,  "Color Index");
		}

		_totalTime = 0.0F;
		//Beat Checker
		foreach (var beat in Sheet.Beats) {
			if (beat.Type == "Visual") {
				ValidateValue(ref beat.VisualIdx, -1, Settings.Defines.Visuals.Count - 1, "Visual Index");
				ValidateValue(ref beat.SoundIdx,  0,  Settings.Defines.Sounds.Count - 1,  "Sound Index");
			}
			else if (beat.Type.Substring(0, 4) == "Note") {
				ValidateValue(ref beat.NoteIdx, -1, Settings.NotesSettings.Notes.Count() - 1, "Note Index");
				NoteTimes.Add(_totalTime);
				NoteTimes.Add(_totalTime + beat.Duration);
			}
			_totalTime += beat.Duration;
		}

		for (var i = 0; i < NoteTimes.Count; ++i) { NoteTimes[i] /= _totalTime; }
		//UnityEngine.Debug.Log("_totalTime : " + _totalTime + ".");
	}

	#endregion

	#region Player

	//----------------------------------------------------------------------------------------------------
	/// <summary> Starts the Sheet. </summary>
	/// <returns> The First Beat of the Sheet. </returns>
	public static Music.Beat Start()
	{
		_isStarted = true;
		_startTime = UnityEngine.Time.time;
		_nextTime  = Sheet.Beats[_currentBeat].Duration;
		UnityEngine.Debug.Log("Beat : " + Sheet.Beats[_currentBeat] + ", end at " + _nextTime);
		return Sheet.Beats[_currentBeat];
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the time and return the new beat, if previous is finished. </summary>
	/// <returns> The new beat, if previous is finished. </returns>
	public static Music.Beat Update()
	{
		var time = GetCurrentTime();

		//Check the beat change
		if (_nextTime >= time || _currentBeat >= Sheet.Beats.Count - 1) { return null; }
		_currentBeat++;
		_nextTime += Sheet.Beats[_currentBeat].Duration;
		UnityEngine.Debug.Log("Beat : " + Sheet.Beats[_currentBeat] + ", end at " + _nextTime);
		return Sheet.Beats[_currentBeat];
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Determines whether the sheet is finished. </summary>
	/// <returns> <c>true</c> if this instance is finished; otherwise, <c>false</c>. </returns>
	public static bool IsFinished() { return _isStarted && GetCurrentTime() >= _totalTime; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the current time of the sheet. </summary>
	/// <returns> </returns>
	public static float GetCurrentTime() { return _isStarted ? UnityEngine.Time.time - _startTime : 0; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the current time ratio of the sheet. </summary>
	/// <returns> </returns>
	public static float GetTimeRatio() { return GetCurrentTime() / _totalTime; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Return the value truncated by min and max. </summary>
	/// <param name="value"> The value. </param>
	/// <param name="min"> The minimum. </param>
	/// <param name="max"> The maximum. </param>
	/// <returns></returns>
	private static int GetValidValue(int value, int min, int max) { return System.Math.Max(System.Math.Min(value, max), min); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Validates the value. </summary>
	/// <param name="value"> The value. </param>
	/// <param name="min"> The minimum. </param>
	/// <param name="max"> The maximum. </param>
	/// <param name="msg"> The message for Debug. </param>
	private static void ValidateValue(ref int value, int min, int max, string msg = "")
	{
		var tmp = GetValidValue(value, min, max);
		if (tmp != value && msg != "") { UnityEngine.Debug.Log("Error:" + msg + " is out of range : " + value + " => " + tmp); }
		value = tmp;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Validates the value. </summary>
	/// <param name="value"> The value. </param>
	/// <param name="max"> The maximum. </param>
	/// <param name="msg"> The message for Debug. </param>
	private static void ValidateValue(ref int value, int max, string msg = "") { ValidateValue(ref value, 0, max, msg); }

	#endregion
}
}
