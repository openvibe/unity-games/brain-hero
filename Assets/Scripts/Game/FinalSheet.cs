﻿using System.Collections.Generic;
using UnityEngine;
using NoteList = System.Collections.Generic.List<(int id, int expected, float start, float end)>;

namespace Game
{
/// <summary> FinalManager is the class to manage UI elements of Final Screen. </summary>
public class FinalSheet : MonoBehaviour
{
	#region Members

	private const int   N_ROW               = 6;              // Number of Row in the Sheet
	private const float TIME_BY_ROW         = 120.0F;         // Default Maximum Time per Row (can be increase if total time > N_ROW * TIME_BY_ROW
	private const float NOTE_Y_SHIFT        = 20;             // Shift Position Y of Note from object center
	private const float NOTE_BAR_X_SHIFT    = 6;              // Shift Position X of Bar between 2 notes (for eighth notes)
	private const float NOTE_BAR_Y_SHIFT    = 25;             // Shift Position Y of Bar between 2 notes (for eighth notes)
	private const float NOTE_BAR_SIZE_SHIFT = 2;              // Size Shift of Bar between 2 notes (for eighth notes)
	private const float NOTE_WIDTH          = 20;             // Default Note Width
	private const float NOTE_HEIGHT         = 50;             // Default Note Height
	private const float DOUBLE_EPSILON      = NOTE_WIDTH + 5; // Epsilon Limit to change note by a eighth note

	//-------------------------------------
	//---------- Serialize Field ----------
	//-------------------------------------
	[SerializeField] private GameObject _colorSquare = null;
	[SerializeField] private GameObject _note        = null;
	[SerializeField] private GameObject _failedNote  = null;
	[SerializeField] private GameObject _missedNote  = null;

	//-----------------------------
	//---------- Members ----------
	//-----------------------------
	private readonly List<GameObject> _rows           = new List<GameObject>(); // List of Row in Sheet
	private readonly List<Transform>  _rowsFrame      = new List<Transform>();  // Frame Of Each Row
	private readonly List<Transform>  _rowsNotes      = new List<Transform>();  // Notes "folder" Of Each Row
	private readonly List<bool>       _trialsHaveNote = new List<bool>();       // List for each trial if they are a note or not (for missed note)

	private static Rect    _noteRect;    // Rectangle Size of Notes "folder"
	private static float[] _posY = null; // Position of Each line (relative to parent so Notes "folder") one line per class (nodalities, note types)

	private static readonly List<(int id, float start, float end)>     Expected  = new List<(int, float, float)>();   // List of Expected Notes
	private static readonly List<(float start, float end, float size)> RowLimits = new List<(float, float, float)>(); // Times of each row (begin, end, size)

	#endregion

	#region Initialization

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="FinalSheet" /> class. </summary>
	public void Init()
	{
		ResetLists();
		FindObjects();
		InitMembers();
		InitSheet();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset All Lists. </summary>
	private void ResetLists()
	{
		_rows.Clear();
		_rowsFrame.Clear();
		_rowsNotes.Clear();
		_trialsHaveNote.Clear();
		Expected.Clear();
		RowLimits.Clear();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Finds the GameObject in Base group. </summary>
	private void FindObjects()
	{
		for (var i = 1; i <= N_ROW; ++i) {
			var row = transform.Find($"Row Sheet {i}");
			_rows.Add(row.gameObject);
			_rowsFrame.Add(row.Find("Frame"));
			_rowsNotes.Add(row.Find("Notes"));
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the static member. </summary>
	private void InitMembers()
	{
		//***** Set Positions & Sizes *****
		_noteRect = _rowsNotes[0].GetComponent<RectTransform>().rect;
		var step = _noteRect.height / (BoardManager.NoteNumber + 1);
		var y    = -(_noteRect.height / 2) + step;
		_posY = new float[BoardManager.NoteNumber];
		for (var i = 0; i < BoardManager.NoteNumber; ++i, y += step) { _posY[i] = y; }

		//***** Set Expected notes *****
		var totalTime   = 0.0F;
		var startTrials = new List<float>();
		foreach (var beat in SheetPlayer.Sheet.Beats) {
			if (beat.Type == "Trial Start") { startTrials.Add(totalTime); }
			else if (beat.Type.Substring(0, 4) == "Note") {
				Expected.Add((beat.NoteIdx, totalTime, totalTime + beat.Duration));
				_trialsHaveNote.Add(false);
			}
			totalTime += beat.Duration;
		}

		//***** Set Repartition *****
		// Each row is start/end with a start trial (except is last trial is longer than row time)
		// The end of row is same as start of next row
		// Each Row can't exceed row time but Row have not necessary same size
		// If we cut by trial it's to avoid a begin of trial at end of row and end trial at start of row
		// Last Row have maximum size (row time) to avoid row with one short trial take all the line
		var timeByrow    = totalTime > N_ROW * TIME_BY_ROW ? totalTime / N_ROW : TIME_BY_ROW;
		var lastRowStart = startTrials[0];
		var lastStart    = lastRowStart;
		totalTime += 1; // To have a little margin at the end
		foreach (var t in startTrials) {
			// If Trial is longer than row time (Free mode or REALLY special case)
			while (t - lastStart > timeByrow) {
				RowLimits.Add((lastRowStart, lastRowStart + timeByrow, timeByrow));
				lastRowStart += timeByrow;
				lastStart    =  lastRowStart;
			}
			if (t - lastRowStart > timeByrow) {
				RowLimits.Add((lastRowStart, lastStart, lastStart - lastRowStart));
				lastRowStart = lastStart;
			}
			lastStart = t;
		}
		var lastTime = System.Math.Max(totalTime, lastRowStart + timeByrow);
		while (lastTime - lastRowStart > timeByrow) {
			var tmp = lastRowStart + timeByrow;
			RowLimits.Add((lastRowStart, tmp, timeByrow));
			lastRowStart = tmp;
		}
		lastTime = System.Math.Max(totalTime, lastRowStart + timeByrow);
		RowLimits.Add((lastRowStart, lastTime, lastTime - lastRowStart));
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the sheet (lines for notes, beat separation, hide unused row). </summary>
	private void InitSheet()
	{
		// Lines of notes
		for (var row = 0; row < _rowsFrame.Count; ++row) {
			for (var index = 0; index < BoardManager.NoteNumber; ++index) { AddLine(row, index, _posY[index]); }
		}

		// Separation of Beats
		for (var i = 0; i < Expected.Count; ++i) {
			var pos = GetPosition(Expected[i].start);
			AddSeparator(pos.row, pos.ratio);
			pos = GetPosition(Expected[i].end);
			AddSeparator(pos.row, pos.ratio);
		}

		//Hide unused Row
		for (var i = RowLimits.Count; i < N_ROW; ++i) { _rows[i].SetActive(false); }
	}

	#endregion

	#region Position Finder

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the row of this time. </summary>
	/// <param name="time"> The time. </param>
	/// <returns> The Row concernend by this time. </returns>
	private static int GetRow(float time)
	{
		for (var i = 0; i < RowLimits.Count; ++i) {
			if (time < RowLimits[i].end) { return i; }
		}
		return RowLimits.Count - 1;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the position on sheet of this time. </summary>
	/// <param name="time"> The time. </param>
	/// <returns> Double value, int for the Row and float for the ratio (0 to 1) in this line. </returns>
	private static (int row, float ratio) GetPosition(float time)
	{
		var row   = GetRow(time);
		var ratio = (time - RowLimits[row].start) / RowLimits[row].size;
		return (row, ratio);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Convert ratio (0 to 1) to X position. </summary>
	/// <param name="ratio"> The ratio (0 to 1). </param>
	/// <returns> </returns>
	private static float RatioToX(float ratio) { return ratio * _noteRect.width - _noteRect.width; }

	#endregion

	#region Drawing

	//----------------------------------------------------------------------------------------------------
	/// <summary> Adds a line. </summary>
	/// <param name="row"> The row concerned. </param>
	/// <param name="index"> The index of note. </param>
	/// <param name="y"> The y Coordinate. </param>
	private void AddLine(int row, int index, float y)
	{
		var tmp = Instantiate(_colorSquare, _rowsFrame[row]);
		tmp.name = "Line Note " + (index + 1);

		// Reset Anchor
		tmp.GetComponent<RectTransform>().anchorMin     = new Vector2(0,    0.5F);
		tmp.GetComponent<RectTransform>().anchorMax     = new Vector2(1,    0.5F);
		tmp.GetComponent<RectTransform>().pivot         = new Vector2(0.5F, 0.5F);
		tmp.GetComponent<RectTransform>().localPosition = new Vector3(0, y, 0);
		tmp.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 1);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Adds a vertical separator. </summary>
	/// <param name="row"> The row concerned. </param>
	/// <param name="ratio"> The ratio position in row. </param>
	private void AddSeparator(int row, float ratio)
	{
		var tmp = Instantiate(_colorSquare, _rowsNotes[row]);
		tmp.name = "Separator " + (ratio * (RowLimits[row].end - RowLimits[row].start) + RowLimits[row].start) + " s";

		// Reset Anchor
		tmp.GetComponent<RectTransform>().anchorMin     = new Vector2(0,    0);
		tmp.GetComponent<RectTransform>().anchorMax     = new Vector2(0,    1);
		tmp.GetComponent<RectTransform>().pivot         = new Vector2(0.5F, 0.5F);
		tmp.GetComponent<RectTransform>().localPosition = new Vector3(RatioToX(ratio), 0, 0);
		tmp.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 1);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary>Adds a note.</summary>
	/// <param name="row"> The row concerned. </param>
	/// <param name="id"> The index of note (if -1, it's a note not played). </param>
	/// <param name="expected"> The expected note (if -1 it's a free note). </param>
	/// <param name="ratio"> The ratio position in row. </param>
	/// <param name="y"> The y Coordinate. </param>
	private void AddNote(int row, int id, int expected, float ratio, float y)
	{
		if (id == -1 && expected == -1) { return; }
		GameObject note;
		if (expected == -1 || id == expected) { note = _note; }
		else if (id == -1) { note                    = _missedNote; }
		else { note                                  = _failedNote; }

		var tmp = Instantiate(note, _rowsNotes[row]);
		tmp.name = "Note " + (id == -1 ? id.ToString() : "Missed (" + expected + ")") + " : " + ratio;

		// Reset Anchor
		tmp.GetComponent<RectTransform>().anchorMin     = new Vector2(0.5F, 0.5F);
		tmp.GetComponent<RectTransform>().anchorMax     = new Vector2(0.5F, 0.5F);
		tmp.GetComponent<RectTransform>().pivot         = new Vector2(0.5F, 0.5F);
		tmp.GetComponent<RectTransform>().localPosition = new Vector3(RatioToX(ratio), y, 0);
		tmp.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, NOTE_WIDTH);
		tmp.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,   NOTE_HEIGHT);
		//var tmp = FinalManager.InstantiateObject(RowsNotes[row], FinalManager.Note, "Note " + id + " : " + x, new Rect(x, y, NOTE_WIDTH, NOTE_HEIGHT));
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Adds the bar between 2 notes (for eighth notes).</summary>
	/// <param name="row"> The row concerned. </param>
	/// <param name="id"> The index of note (if -1, it's a note not played). </param>
	/// <param name="ratio"> The ratio position in row. </param>
	/// <param name="y"> The y Coordinate. </param>
	/// <param name="size"> The size of the bar. </param>
	private void AddBarNote(int row, int id, float ratio, float y, float size)
	{
		var tmp = Instantiate(_colorSquare, _rowsNotes[row]);
		tmp.name = "Note " + id + " bar";

		// Reset Anchor
		tmp.GetComponent<RectTransform>().anchorMin     = new Vector2(0.5F, 0.5F);
		tmp.GetComponent<RectTransform>().anchorMax     = new Vector2(0.5F, 0.5F);
		tmp.GetComponent<RectTransform>().pivot         = new Vector2(0.5F, 0.5F);
		tmp.GetComponent<RectTransform>().localPosition = new Vector3(RatioToX(ratio) + NOTE_BAR_X_SHIFT, y + NOTE_BAR_Y_SHIFT, 0);
		tmp.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size + NOTE_BAR_SIZE_SHIFT);
		tmp.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,   4);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Draws the missed note. </summary>
	private void DrawMissed()
	{
		for (var i = 0; i < Expected.Count; ++i) {
			if (_trialsHaveNote[i]) { continue; }
			var mean = (Expected[i].start + Expected[i].end) / 2;
			var (row, ratio) = GetPosition(mean);
			var y = NOTE_Y_SHIFT + _posY[Expected[i].id];
			AddNote(row, -1, Expected[i].id, ratio, y);
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Draws the notes of this list. </summary>
	/// <param name="notes"> List of Notes found. </param>
	private void DrawNotes(NoteList notes)
	{
		foreach (var (id, expected, start, end) in notes) {
			if (id < 0 || id >= BoardManager.NoteNumber) { continue; }
			var (rowStart, ratioStart) = GetPosition(start);
			var (rowEnd, ratioEnd)     = GetPosition(end);
			var x = (ratioStart + ratioEnd) / 2;
			var y = NOTE_Y_SHIFT + _posY[id];
			if (rowStart != rowEnd) { Debug.Log("*****TO MANAGE*****"); }
			else {
				var noteSize = (ratioEnd - ratioStart) * _noteRect.width;
				if (noteSize < DOUBLE_EPSILON) { AddNote(rowStart, id, expected, x, y); }
				else {
					AddNote(rowStart, id, expected, ratioStart, y);
					AddNote(rowEnd,   id, expected, ratioEnd,   y);
					AddBarNote(rowStart, id, x, y, noteSize);
				}

				// Update expected boolean if we have a note during a trial
				var mean = (start + end) / 2;
				for (var i = 0; i < Expected.Count; ++i) {
					if (Expected[i].id == -1 || (Expected[i].start <= mean && mean <= Expected[i].end)) { _trialsHaveNote[i] = true; }
				}
			}
		}
		DrawMissed();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Draws the founded notes on sheet. </summary>
	public void MakeSheet(NoteList notes) { DrawNotes(notes); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Takes the screenshot of Sheet but wait End of Frame. </summary>
	/// <returns> <c>IEnumerator</c> because function is asynchronous. </returns>
	public System.Collections.IEnumerator TakeScreenshot()
	{
		yield return new WaitForEndOfFrame();
		var date     = "[" + System.DateTime.Now.ToString("yyyy.MM.dd-HH.mm.ss") + "]";
		var filename = System.IO.Path.Combine(System.IO.Directory.GetParent(Application.dataPath).FullName, "Screenshot-" + date + ".png");
		var rect     = transform.GetComponent<RectTransform>().rect;
		var tex      = new Texture2D((int) rect.width, (int) rect.height, TextureFormat.RGB24, false);
		rect.x = (Screen.width - rect.width) / 2;
		rect.y = (Screen.height - rect.height) / 2;
		tex.ReadPixels(rect, 0, 0);
		tex.LoadRawTextureData(tex.GetRawTextureData());
		tex.Apply();
		System.IO.File.WriteAllBytes(filename, tex.EncodeToPNG());
	}

	#endregion
}
}
