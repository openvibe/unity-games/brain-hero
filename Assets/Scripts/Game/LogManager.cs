﻿using Score = Game.Feedback.Score;

namespace Game
{
/// <summary> Class to Manage Log during the game. </summary>
public static class LogManager
{
	#region Members

	private static System.IO.StreamWriter _streamWriter; // The stream writer.
	private static float                  _startTime;    // The time of game starting (when the game receive the signal form LSL).

	#endregion

	#region Common

	//----------------------------------------------------------------------------------------------------
	/// <summary> Format time in string. </summary>
	/// <param name="time"> Time to convert (in second). </param>
	/// <returns> String in format MM:SS:MS. </returns>
	private static string FormatTime(float time)
	{
		var minutes      = (int) time / 60;
		var seconds      = (int) time - 60 * minutes;
		var milliseconds = (int) (1000 * (time - (int) time));
		return $"{minutes:00}:{seconds:00}:{milliseconds:000}";
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Format the date in string. </summary>
	/// <returns> String in format [yyyy.MM.dd-HH.mm.ss].</returns>
	private static string GetDateString() { return "[" + System.DateTime.Now.ToString("yyyy.MM.dd-HH.mm.ss") + "]"; }

	/// <summary> Format the time since start (<see cref="_startTime"/>) in string. </summary>
	/// <returns> String in format [MM:SS:MS]. </returns>
	private static string GetTimeString() { return "[" + FormatTime(UnityEngine.Time.time - _startTime) + "]"; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Opens the file. </summary>
	private static void OpenFile()
	{
		var filename = System.IO.Path.Combine(System.IO.Directory.GetParent(UnityEngine.Application.dataPath).FullName, "Log-" + GetDateString() + ".log");
		_streamWriter           = System.IO.File.CreateText(filename);
		_streamWriter.AutoFlush = true;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Closes the file. </summary>
	private static void CloseFile() { _streamWriter.Close(); }

	#endregion

	#region Writer

	//----------------------------------------------------------------------------------------------------
	/// <summary> Writes a line. </summary>
	/// <param name="line"> The line to write. </param>
	/// <param name="time"> If set to <c>true</c> time is set at the beginning of the line (default : true). </param>
	/// <seealso cref="GetTimeString"/>
	public static void WriteLine(string line, bool time = true)
	{
		var msg = (time ? GetTimeString() + " " : "") + line;
		_streamWriter.WriteLine(msg);
		_streamWriter.Flush(); // Force Flush (in case of crash)
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Writes the header of log (open File in same time). </summary>
	public static void WriteHeader()
	{
		OpenFile();
		_startTime = UnityEngine.Time.time;
		WriteLine($"Run Start at {GetDateString()} with Sheet : {SheetPlayer.Sheet.SettingToString()}");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Writes the ender of log (Close File in same time). </summary>
	public static void WriteEnder(Score score)
	{
		var n = BoardManager.NoteNumber;
		WriteScores((int) score.GlobalScore);
		WriteMatrix("Confusion Matrix (Trial wise)", score.TrialConfusionMatrix, n, n);
		WriteLine($"Accuracy (Trial wise): {Score.GetAccuracy(score.TrialConfusionMatrix, n)}");
		WriteMatrix("Confusion Matrix (Epoch wise)", score.EpochConfusionMatrix, n, n);
		WriteLine($"Accuracy (Epoch wise): {Score.GetAccuracy(score.EpochConfusionMatrix, n)}");
		WriteMatrix("Confusion Matrix (Chunk wise)", score.ChunkConfusionMatrix, n, n);
		WriteLine($"Accuracy (Chunk wise): {Score.GetAccuracy(score.ChunkConfusionMatrix, n)}");
		CloseFile();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Starts the trial. </summary>
	/// <param name="expected"> The expected note. </param>
	public static void StartTrial(int expected) { WriteLine($"Start Trial for class {expected}."); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Stops the trial. </summary>
	/// <param name="expected"> The expected note. </param>
	/// <param name="trialWise"> The trial wise result. </param>
	/// <param name="epochWise"> The epoch wise result. </param>
	/// <param name="score"> Score to write. </param>
	/// <param name="maxScore"> The theorical score max. </param>
	public static void StopTrial(int expected, int trialWise, int epochWise, float score, float maxScore)
	{
		var str = maxScore == 0 ? "" : $", Score : {score}/{maxScore} ({((100.0F * score) / maxScore):#.00} %)";
		WriteLine($"Stop Trial for class {expected}, Trial wise result is : {trialWise}, Epoch wise result is : {epochWise}{str}.");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Line for Experiment Stop. </summary>
	public static void StopExperiment() { WriteLine("Experiment Stop."); }


	//----------------------------------------------------------------------------------------------------
	/// <summary> Writes the scores. </summary>
	/// <param name="score"> The score. </param>
	private static void WriteScores(int score) { WriteLine($"Score : {score}"); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Writes the matrix. </summary>
	/// <param name="name"> The name of the matrix. </param>
	/// <param name="matrix"> The matrix to write. </param>
	/// <param name="row"> The number of row. </param>
	/// <param name="col"> The number of col. </param>
	private static void WriteMatrix(string name, int[,] matrix, int row, int col)
	{
		WriteLine(name + " : ");
		for (var i = 0; i < row; ++i) {
			var tmp = matrix[i, 0].ToString();
			for (var j = 1; j < col; ++j) { tmp += "\t" + matrix[i, j]; }
			WriteLine(tmp, false);
		}
	}

	#endregion
}
}
