﻿using UnityEngine;
using HUDTexts = System.Collections.Generic.Dictionary<string, TMPro.TMP_Text>;
using Image = UnityEngine.UI.Image;

namespace Game
{
/// <summary> HUDManager is the class to manage UI elements. </summary>
public class HUDManager : MonoBehaviour
{
	#region Members

	private static Image      _centralImage = null; // Central image used to display instruction.
	private static GameObject _scoreBar     = null; // Parent score bar GameObject
	private static Transform  _bar          = null; // Score bar GameObject
	private static float      _barWidth     = 0;    // Width of Score Bar

	private static readonly string[] HUDNames = { "Name", "Score", "Time" }; // Names of text area in HUD.
	private static readonly HUDTexts HUD      = new HUDTexts();              // Dictionary of text area in HUD.

	#endregion

	#region Initialization

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initialize the HUD Manager. </summary>
	public void Init()
	{
		if (!SheetPlayer.IsInit) { return; }
		ResetMembers();
		FindObjects();
		UpdateName(SheetPlayer.Sheet.Name);
		UpdateScore();
		UpdateTime();
		SetActiveText(false, false, false);
		SetActiveScoreBar(false);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset All Values. </summary>
	public static void Destroy() { ResetMembers(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset All members. </summary>
	private static void ResetMembers()
	{
		_centralImage = null;
		_scoreBar     = null;
		_bar          = null;
		_barWidth     = 0;
		HUD.Clear();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Finds the GameObject in Base group. </summary>
	private void FindObjects()
	{
		_centralImage = transform.Find("Central Image").GetComponent<Image>();
		_scoreBar     = transform.Find("Score Bar").gameObject;
		_bar          = _scoreBar.transform.Find("Bar");
		_barWidth     = ((RectTransform) _scoreBar.transform).rect.width;
		foreach (var s in HUDNames) { HUD.Add(s, transform.Find(s).GetComponent<TMPro.TMP_Text>()); }
	}

	#endregion

	#region Updates

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the displayed name of the sheet. </summary>
	/// <param name="name"> The name of the sheet. </param>
	public static void UpdateName(string name = "") { HUD[HUDNames[0]].text = name; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the displayed score. </summary>
	/// <param name="score"> The score. </param>
	public static void UpdateScore(int score = 0) { HUD[HUDNames[1]].text = score.ToString(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the score bar. </summary>
	/// <param name="ratio"> The ratio of score (0 to 1). </param>
	public static void UpdateScoreBar(float ratio)
	{
		_bar.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, ratio * _barWidth);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the time. </summary>
	/// <param name="time"> The time. </param>
	public static void UpdateTime(float time = 0.0F) { HUD[HUDNames[2]].text = System.Math.Round(time, 2) + " s"; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the central image. </summary>
	/// <param name="sprite"> The sprite of the image. </param>
	/// <remarks> If Sprite is null the GameObject is deactivated. </remarks>
	public static void UpdateCentralImage(Sprite sprite = null)
	{
		if (sprite == null) { _centralImage.gameObject.SetActive(false); }
		else {
			_centralImage.sprite = sprite;
			_centralImage.gameObject.SetActive(true);
		}
	}

	#endregion

	#region HUD Activation

	//----------------------------------------------------------------------------------------------------
	/// <summary> Activates/Deactivates the GameObject, depending on the given true or false value. </summary>
	/// <param name="activate"> Activate or deactivate the object, where true activates the GameObject and false deactivates the GameObject. </param>
	public void SetActive(bool activate = true) { gameObject.SetActive(activate); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Sets which text is displayed or not.</summary>
	/// <param name="name"> if set to <c>true</c> show name.</param>
	/// <param name="score"> if set to <c>true</c> show score.</param>
	/// <param name="time"> if set to <c>true</c> show time.</param>
	private static void SetActiveText(bool name = true, bool score = true, bool time = true)
	{
		SetActiveName(name);
		SetActiveScore(score);
		SetActiveTime(time);
	}

	//----------------------------------------------------------------------------------------------------
	public static void SetActiveName(bool     activate = true) { HUD[HUDNames[0]].gameObject.SetActive(activate); }
	public static void SetActiveScore(bool    activate = true) { HUD[HUDNames[1]].gameObject.SetActive(activate); }
	public static void SetActiveTime(bool     activate = true) { HUD[HUDNames[2]].gameObject.SetActive(activate); }
	public static void SetActiveScoreBar(bool activate = true) { _scoreBar.SetActive(activate); }

	#endregion
}
}
