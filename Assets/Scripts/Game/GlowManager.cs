﻿using UnityEngine;

/// <summary> Class to toggle Glow Material. </summary>
public class GlowManager : MonoBehaviour
{
	[SerializeField] private Material _glowMaterial    = null;  // The glow material
	[SerializeField] private Material _nonGlowMaterial = null;  // The non glow material
	private                  bool     _isGlowing       = false; // The is glowing toggle

	//----------------------------------------------------------------------------------------------------
	/// <summary> Toggles the glow material. </summary>
	public void ToggleGlow()
	{
		if (_isGlowing) { UnGlow(); }
		else { Glow(); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Set Glow Material. </summary>
	public void Glow()
	{
		gameObject.GetComponent<Renderer>().material = _glowMaterial;
		_isGlowing                                   = true;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Set Non Glow Material. </summary>
	public void UnGlow()
	{
		gameObject.GetComponent<Renderer>().material = _nonGlowMaterial;
		_isGlowing                                   = false;
	}
}
