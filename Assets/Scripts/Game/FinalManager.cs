﻿using System.Collections.Generic;
using UnityEngine;
using NoteList = System.Collections.Generic.List<(int id, int expected, float start, float end)>;

namespace Game
{
/// <summary> FinalManager is the class to manage UI elements of Final Screen. </summary>
public class FinalManager : MonoBehaviour
{
	private static FinalSheet _sheet;

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initialize the Final Screen Manager. </summary>
	public void Init()
	{
		_sheet = transform.Find("Sheet").GetComponent<FinalSheet>();
		_sheet.Init();
		SetActive(false);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset All Values. </summary>
	public static void Destroy() { }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Draws the founded notes on sheet. </summary>
	/// <param name="notes"> List of Notes found. </param>
	public static void MakeSheet(NoteList notes) { _sheet.MakeSheet(notes); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Takes the screenshot of Sheet but wait End of Frame. </summary>
	/// <returns> <c>IEnumerator</c> because function is asynchronous. </returns>
	public void TakeScreenshot() { StartCoroutine(_sheet.TakeScreenshot()); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Activates/Deactivates the GameObject, depending on the given true or false value. </summary>
	/// <param name="activate"> Activate or deactivate the object, where true activates the GameObject and false deactivates the GameObject. </param>
	public void SetActive(bool activate = true) { gameObject.SetActive(activate); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Activates the GameObject, depending on the given true or false value but deactivate sheet. </summary>
	/// <param name="activate"> Activate or deactivate the object, where true activates the GameObject and false deactivates the GameObject. </param>
	public void SetActiveBackground(bool activate = true)
	{
		gameObject.SetActive(activate);
		transform.Find("Sheet").gameObject.SetActive(false);
	}
}
}
