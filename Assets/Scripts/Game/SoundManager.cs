﻿using UnityEngine;

namespace Game
{
/// <summary> Sound Manager for the Game. </summary>
public class SoundManager : MonoBehaviour
{
	private static AudioSource _source       = null; // The audio source.
	private static int         _defaultSound = 1;    // The default sound.

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initialize the Sound Manager. </summary>
	public void Init()
	{
		if (!SheetPlayer.IsInit) { return; }
		_source       = transform.GetComponent<AudioSource>();
		_defaultSound = SheetPlayer.Sheet.Sound ? (int) Settings.GeneralSettings.GetSetting("Sound") + 1 : -1; // + 1 to avoid default choice);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset All Values. </summary>
	public static void Destroy()
	{
		_source       = null;
		_defaultSound = 1;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Plays the specified sound. </summary>
	/// <param name="sound"> The sound index in Sound list (if 0 default sound is played). </param>
	public static void Play(int sound = 0)
	{
		var clip = Settings.Defines.Sounds[sound == 0 ? _defaultSound : sound].Object;
		if (_defaultSound == -1 || clip == null) { return; }
		_source.clip = clip;
		_source.Play();
	}
}
}
