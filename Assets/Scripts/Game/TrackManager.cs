﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
/// <summary> Builder for the Track. </summary>
/// <seealso cref="UnityEngine.MonoBehaviour" />
public class TrackManager : MonoBehaviour
{
	#region Members

	private const  float TIME_SCALE  = 100;        // Scale Ratio between second and coordinate
	private static float _speedScale = TIME_SCALE; // Ratio for displayed note (between button center and top of ground

	private static List<GameObject> Notes { get; } = new List<GameObject>(); // List of notes created

	[SerializeField] private GameObject _note = null;

	#endregion

	#region Functions

	/// <summary> Updates Callback for track's position. </summary>
	private void Update() { UpdatePosition(SheetPlayer.GetCurrentTime()); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset All Values. </summary>
	public static void Destroy() { Notes.Clear(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Builds the track. </summary>
	public void Build()
	{
		//----- Initialization -----
		if (!BoardManager.IsBuild) { return; }
		_speedScale = (BoardManager.GROUND_L - BoardManager.NotePosY) / SheetPlayer.Sheet.Speed;
		float totalTime = 0, visualTime = 0;
		Notes.Clear();
		foreach (var b in SheetPlayer.Sheet.Beats) {
			if (b.Type == "Visual") { visualTime = totalTime; }
			if (b.Type.Substring(0, 4) == "Note" && b.NoteIdx != -1) { CreateNote(BoardManager.NotePosX[b.NoteIdx], visualTime, b.Duration); }
			totalTime += b.Duration;
		}
		UpdatePosition(0);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Creates a note. </summary>
	/// <param name="x"> The x coordinate of note. </param>
	/// <param name="time"> The time to set y coordinate. </param>
	/// <param name="duration"> The duration of the note. </param>
	private void CreateNote(float x, float time, float duration)
	{
		// Create note
		var note = Instantiate(_note, transform);
		note.name               = "Note " + (Notes.Count + 1).ToString("00");
		note.transform.position = new Vector3(x, time * _speedScale, -3 * BoardManager.VIEW_STEP);

		//Update Trail
		var trail  = note.transform.Find("Trail");
		var length = duration * _speedScale;
		trail.localScale = new Vector3(trail.localScale.x, length,                                 trail.localScale.z);
		trail.position   = new Vector3(trail.position.x,   note.transform.position.y + length / 2, trail.position.z);

		Notes.Add(note);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the track position. </summary>
	/// <param name="time"> The time. </param>
	private void UpdatePosition(float time) { transform.position = new Vector3(0, BoardManager.GROUND_L - time * _speedScale, 0); }

	#endregion
}
}
