﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ButtonList = System.Collections.Generic.List<(UnityEngine.Transform Target, UnityEngine.Transform Button, GlowManager GlowManager)>;

namespace Game
{
/// <summary> Builder for the Board Game. </summary>
/// <seealso cref="UnityEngine.MonoBehaviour" />
public class BoardManager : MonoBehaviour
{
	#region Members

	//----- Constants -----
	// Ground
	private const float CENTER_XY     = 500;  // Center of the ground
	private const float GROUND_MAX_W  = 500;  // Limit of Width to be on screen
	public const  float GROUND_L      = 1000; // Size of Board in Length (camera position can change it)
	private const float GROUND_H      = 100;  // Size of Board in Heigh/Depth
	private const float SECTION_MAX_W = 200;  // Max Size allowed per note

	// Ruler / Timer
	private const float RULER_W      = 10;              // Width of Ruler (timer)
	private const float RULER_MARK_W = 0.75F;           // Width of Ruler Mark (timer)
	private const float TIMER_W      = 0.75F * RULER_W; // Width of Timer on X based on RULER_W (timer)
	private const float TIMER_H      = 0.5F * RULER_W;  // Heigh of Timer on Z based on RULER_W (timer)

	// Separators
	private const float VISUAL_SEPARATOR_W   = 5; // Width of Separator
	private const float VERTICAL_SEPARATOR_W = 2; // Width of Vertical Separator

	// Visuals
	public const  float VISUAL_AREA_Y          = 50;                                               // Size Y (length) of Area for visuals
	private const float MARGIN                 = 5;                                                // Margin beetween separator and object
	private const float VISUAL_Y               = RULER_W + VISUAL_AREA_Y / 2;                      // XY are in center of object
	private const float VISUAL_SEPARATOR_Y     = RULER_W + VISUAL_AREA_Y + VISUAL_SEPARATOR_W / 2; // Coordinate Y of visual separator
	public const  float VISUAL_SEPARATOR_TOP_Y = VISUAL_SEPARATOR_Y + VISUAL_SEPARATOR_W / 2;      // Coordinate Y after  visual separator

	private static readonly Color Blank = new Color(1,    1,    1);
	private static readonly Color Grey  = new Color(0.4F, 0.4F, 0.4F);

	// Buttons
	private const float BUTTON_H      = 7.5F;                     // Size of BUtton in Z Coordinate
	private const float BUTTON_BASE_H = 0.5F;                     // Ratio of button base height
	private const float BUTTON_RATIO  = 0.75F;                    // Ratio of button (circle at base)
	private const float TARGET_Z      = 2 * BUTTON_H + VIEW_STEP; // Coordinate Z of Target in top of BUtton

	// Misc
	public const float VIEW_STEP = 0.1F; // Step to avoid object in same plane

	//----- Usefull members to avoid re-computes -----
	// Founded/Created GameObjects
	private static readonly ButtonList       Buttons = new ButtonList();
	private static readonly List<GameObject> Bars    = new List<GameObject>();
	private static readonly List<GameObject> Visuals = new List<GameObject>();
	private static          Transform        _window = null;
	private static          Transform        _timer  = null;
	private static          Transform        _time   = null;

	// Computed positions
	public static List<float> NotePosX   { get; }              = new List<float>();
	public static float       NotePosY   { get; private set; } = 0;
	public static int         NoteNumber { get; private set; } = 0;

	// Computed Sizes
	private static float _buttonW   = 0; // Size of Button in width
	private static float _groundW   = 0; // Size of Board in width
	private static float _timerCoef = 0; // Coefficient which multiplicate ratio of time (0 to 1) for Timer Bar (half of _groundW)
	private static float _minX      = 0; // Pos X Min (left of Board)

	public static bool IsBuild = false;

	[SerializeField] private GameObject _separator = null;
	[SerializeField] private GameObject _visual    = null;
	[SerializeField] private GameObject _button    = null;
	[SerializeField] private GameObject _bar       = null;
	[SerializeField] private GameObject _rulerMark = null;

	private void Update() { UpdateTimer(SheetPlayer.GetTimeRatio()); }

	#endregion

	#region Builder

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset All Lists and build flag. </summary>
	public static void Destroy()
	{
		ResetLists();
		IsBuild = false;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset All Lists. </summary>
	private static void ResetLists()
	{
		Buttons.Clear();
		Bars.Clear();
		Visuals.Clear();
		NotePosX.Clear();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Builds the board. </summary>
	public void Build()
	{
		//----- Initialization -----
		ResetLists();
		NoteNumber = Settings.SettingsManager.Sheet.Notes.Count;
		if (NoteNumber == 0 || !SheetPlayer.IsInit) { return; }
		FindObjects();

		//----- Marks -----
		_groundW   = Mathf.Min(SECTION_MAX_W * NoteNumber, GROUND_MAX_W);
		_timerCoef = _groundW / 2;
		_minX      = CENTER_XY - _groundW / 2;
		var maxX     = _minX + _groundW;
		var noteStep = _groundW / NoteNumber;
		_buttonW = System.Math.Min(noteStep, VISUAL_AREA_Y) - MARGIN * 2; // Allow Max size VISUAL_SEP (including min margin)

		// Center X pos of each note
		var x = _minX - noteStep / 2;
		for (var i = 0; i < NoteNumber; ++i) {
			x += noteStep;
			NotePosX.Add(x);
		}
		NotePosY = VISUAL_SEPARATOR_TOP_Y + MARGIN + BUTTON_RATIO * _buttonW;
		//NotePosY = VISUAL_SEP + size / 2 + MARGIN;

		//----- Ground -----
		var ground = transform.Find("Ground");
		ground.position   = new Vector3(CENTER_XY, CENTER_XY, GROUND_H / 2);
		ground.localScale = new Vector3(_groundW,  GROUND_L,  GROUND_H);

		//----- Frame -----
		var frame     = transform.Find("Frame");
		var frameDown = frame.Find("Down");
		var scale     = frameDown.localScale;
		scale.y                      = _groundW + 2 * scale.x;
		frameDown.localScale         = scale;
		frame.Find("Left").position  = new Vector3(_minX - scale.x / 2, CENTER_XY, 0);
		frame.Find("Right").position = new Vector3(maxX + scale.x / 2,  CENTER_XY, 0);

		//----- Separators -----
		scale = new Vector3(VERTICAL_SEPARATOR_W, GROUND_L, 1);
		var position = new Vector3(_minX, GROUND_L / 2, -2 * VIEW_STEP);

		for (var i = 0; i < NoteNumber - 1; ++i) {
			position.x += noteStep;
			InstantiateGameObject(_separator, "Separator " + (i + 1), position, scale);
		}

		//----- Visuals Separator -----
		// Separator between visual and Sheet
		scale    = new Vector3(VISUAL_SEPARATOR_W, _groundW,           1);
		position = new Vector3(CENTER_XY,          VISUAL_SEPARATOR_Y, -2 * VIEW_STEP);
		var sepVisual = InstantiateGameObject(_separator, "Visual Separator", position, scale);
		sepVisual.transform.eulerAngles = new Vector3(0, 0, 90); // Euler Angles if rotation with vector3 instead of quaternion


		//----- Timer -----
		InitTimer();

		//----- Visuals -----
		scale    = new Vector3(_buttonW, _buttonW, 1);
		position = new Vector3(0,        VISUAL_Y, -VIEW_STEP); // The origin of the quad is in its center.
		for (var i = 0; i < NoteNumber; ++i) {
			position.x = NotePosX[i];
			var tmp = InstantiateGameObject(_visual, "Visual " + (i + 1), position, scale);
			tmp.GetComponent<Renderer>().material.mainTexture = Settings.Defines.Visuals[Settings.SettingsManager.GetNotes()[i].Visual].Texture;
			Visuals.Add(tmp);
		}

		//----- Buttons -----
		for (var i = 0; i < NoteNumber; ++i) { InstantiateButton("Button " + (i + 1), _buttonW, NotePosX[i]); }

		//----- Bars -----
		scale    = new Vector3(noteStep, 1,                                    1);
		position = new Vector3(0,        scale.y / 2 + VISUAL_SEPARATOR_TOP_Y, -VIEW_STEP); // The origin of the Bar is in its center.
		for (var i = 0; i < NoteNumber; ++i) {
			position.x = NotePosX[i];
			var tmp = InstantiateGameObject(_bar, "Bar " + (i + 1), position, scale);
			tmp.GetComponent<Renderer>().material.color = Settings.Defines.ColorsDictionary.ElementAt(Settings.SettingsManager.GetNotes()[i].Color).Value;
			Bars.Add(tmp);
		}

		//----- Track Window -----
		_window.localScale = new Vector3(GROUND_MAX_W, GROUND_L - NotePosY,                   1);
		_window.position   = new Vector3(CENTER_XY,    GROUND_L - (_window.localScale.y / 2), 0);

		IsBuild = true;
	}

	//----------------------------------------------------------------------------------------------------	
	/// <summary> Finds the gameobjects. </summary>
	private void FindObjects()
	{
		_window = transform.Find("Track Window");
		_timer  = transform.Find("Timer");
		_time   = _timer.Find("Time");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initialize the timer. </summary>
	private void InitTimer()
	{
		var position = _timer.position;
		position.y      = RULER_W / 2;
		_timer.position = position;

		var tmp   = _timer.Find("Base");
		var scale = tmp.localScale;
		scale.x        = _groundW;
		scale.y        = RULER_W;
		tmp.localScale = scale;

		scale            = _time.localScale;
		scale.x          = TIMER_W;
		scale.z          = TIMER_H;
		_time.localScale = scale;

		// Set ruler marks
		foreach (var time in SheetPlayer.NoteTimes) { InstantiateRulerMark(time); }

		UpdateTimer(0);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Instantiates a ruler Mark. </summary>
	/// <param name="time"> The position of the ruler mark (0 to 1). </param>
	private void InstantiateRulerMark(float time)
	{
		var o        = Instantiate(_rulerMark, _timer);
		var position = o.transform.localPosition;
		var scale    = o.transform.localScale;
		position.x                = _minX + (time * _groundW);
		scale.x                   = RULER_MARK_W;
		scale.y                   = TIMER_H;
		o.transform.localPosition = position;
		o.transform.localScale    = scale;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Instantiates a generic GameObject. </summary>
	/// <param name="obj"> The object of GameObject. </param>
	/// <param name="objectName"> The name of GameObject. </param>
	/// <param name="position"> The position of GameObject. </param>
	/// <param name="scale"> The scale of GameObject. </param>
	/// <returns> The GameObject instantiate. </returns>
	private GameObject InstantiateGameObject(GameObject obj, string objectName, Vector3 position, Vector3 scale)
	{
		var res = Instantiate(obj, transform);
		res.name                 = objectName;
		res.transform.position   = position;
		res.transform.localScale = scale;
		return res;
	}


	//----------------------------------------------------------------------------------------------------
	/// <summary> Instantiates a button.</summary>
	/// <param name="buttonName"> The name of button. </param>
	/// <param name="size"> The size of button. </param>
	/// <param name="x"> The x position of button. </param>
	private void InstantiateButton(string buttonName, float size, float x)
	{
		var o = Instantiate(_button, transform);
		o.name               = buttonName;
		o.transform.position = new Vector3(x, NotePosY, 0); // base position

		// Base
		var socle = o.transform.Find("Base");
		socle.localScale = new Vector3(size, BUTTON_RATIO * size, BUTTON_BASE_H * size);

		// Button
		var button = o.transform.Find("Button");
		button.localScale = new Vector3(size, BUTTON_RATIO * size, BUTTON_H);

		// Target
		var target = o.transform.Find("Target");
		target.localPosition = new Vector3(0, 0, TARGET_Z);
		target.localScale    = new Vector3(0, 0, 1);

		Buttons.Add((target, button, socle.GetComponent<GlowManager>()));
	}

	#endregion

	#region Timer Updates

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the timer size with Ratio (0 to 1). </summary>
	/// <param name="ratio"> The ratio. </param>
	private static void UpdateTimer(float ratio)
	{
		ratio = System.Math.Max(_timerCoef * System.Math.Min(ratio, 1), 1); // Allow Minimum width of 1 and maximum ratio of 1...
		var position = _time.localPosition;
		var scale    = _time.localScale;
		scale.y             = ratio;
		position.x          = _minX + ratio;
		_time.localPosition = position;
		_time.localScale    = scale;
	}

	#endregion

	#region Bars Updates

	//----------------------------------------------------------------------------------------------------
	/// <summary> Resets the bar's sizes. (size of 0). </summary>
	public static void ResetBars()
	{
		foreach (var bar in Bars) { UpdateBar(bar.transform, 0); }
	}


	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the bar's sizes. </summary>
	/// <param name="sizes"> The sizes. </param>
	public static void UpdateBars(float[] sizes)
	{
		if (sizes.Length != Bars.Count) { return; }
		for (var i = 0; i < sizes.Length; ++i) { UpdateBar(Bars[i].transform, sizes[i]); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the bar's size. </summary>
	/// <param name="bar"> The bar to update. </param>
	/// <param name="size"> The size of the bar. </param>
	private static void UpdateBar(Transform bar, float size)
	{
		var scale    = bar.localScale;
		var position = bar.position;
		scale.y    = size;
		position.y = VISUAL_SEPARATOR_TOP_Y + scale.y / 2;

		bar.localScale = scale;
		bar.position   = position;
	}

	#endregion

	#region Buttons Updates

	//----------------------------------------------------------------------------------------------------
	/// <summary> Resets the buttons. </summary>
	public static void ResetButtons()
	{
		for (var i = 0; i < Buttons.Count; ++i) {
			HoldButton(i, false);
			UpdateButtonTarget(i, 0);
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the button target. </summary>
	/// <param name="i"> The index of the button to update. </param>
	/// <param name="ratio"> The ratio of the target (0 to 1). </param>
	public static void UpdateButtonTarget(int i, float ratio)
	{
		var size = _buttonW * ratio;
		Buttons[i].Target.localScale = new Vector3(size, BUTTON_RATIO * size, 1);
	}


	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates all the button's state (hold or not). </summary>
	/// <param name="hold"> States of buttons. </param>
	public static void HoldButtons(bool[] hold)
	{
		if (hold.Length != Buttons.Count) { return; }
		for (var i = 0; i < Buttons.Count; ++i) { HoldButton(i, hold[i]); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the button state (hold or not). </summary>
	/// <param name="i"> The index of the button to update. </param>
	/// <param name="hold"> States of buttons. </param>
	public static void HoldButton(int i, bool hold)
	{
		var z = -(hold ? BUTTON_H / 2 : 0);
		UpdateZ(Buttons[i].Button, z);
		UpdateZ(Buttons[i].Target, z + TARGET_Z);
		if (hold) { Buttons[i].GlowManager.Glow(); }
		else { Buttons[i].GlowManager.UnGlow(); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the z position of an object. </summary>
	/// <param name="obj"> The object. </param>
	/// <param name="z"> The new z position. </param>
	private static void UpdateZ(Transform obj, float z)
	{
		var position = obj.localPosition;
		position.z        = z;
		obj.localPosition = position;
	}

	#endregion

	#region Visuals Updates

	//----------------------------------------------------------------------------------------------------
	/// <summary>Resets the visuals.</summary>
	public static void ResetVisuals()
	{
		for (var i = 0; i < Visuals.Count; ++i) { UpdateVisual(i, false); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary>Updates the visuals.</summary>
	/// <param name="activate">The activate.</param>
	public static void UpdateVisuals(int activate = -1)
	{
		for (var i = 0; i < Visuals.Count; ++i) { UpdateVisual(i, i == activate || activate == -1); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary>Updates the visual state (darked or not). </summary>
	/// <param name="i"> The index of the button to update. </param>
	/// <param name="activate">if set to <c>true</c> [activate].</param>
	private static void UpdateVisual(int i, bool activate) { Visuals[i].GetComponent<Renderer>().material.color = activate ? Blank : Grey; }

	#endregion
}
}
