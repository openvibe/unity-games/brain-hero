﻿namespace Game.Feedback
{
/// <summary> List of Score's display. </summary>
public enum ScoreDisplays
{
	/// <summary> The <c>None</c> Score display never show the score. </summary>
	None,

	/// <summary> The <c>Always</c> Score display always show the score. </summary>
	Always,

	/// <summary> The <c>TrialEnd</c> Score display show the score at end of each trials. </summary>
	TrialEnd,

	/// <summary> The <c>SheetEnd</c> Score display show the score at end of the Sheet. </summary>
	SheetEnd
}

/// <summary> class to Manage Score during the Game. </summary>
/// <seealso cref="Feedback"/>
public class Score
{
	#region Members

	public  float  GlobalScore          { get; private set; } = 0.0F;
	public  float  TrialMaxScore        { get; private set; } = 0.0F;
	public  float  TrialScore           { get; private set; } = 0.0F;
	public  int[,] TrialConfusionMatrix { get; private set; } = null;
	public  int[,] EpochConfusionMatrix { get; private set; } = null;
	public  int[,] ChunkConfusionMatrix { get; private set; } = null;
	private float  _scale = 1.0F;

	public ScoreDisplays Type = ScoreDisplays.Always; // The Score Display Method (<see cref="ScoreDisplays"/>).

	#endregion

	#region Basic Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes this instance. </summary>
	/// <param name="n"> Note Number. </param>
	/// <param name="scale"> Scale of Score (by default 1 but we can add a factor to score to increase/decrease it). </param>
	public void Init(int n, float scale = 1.0F)
	{
		GlobalScore          = 0.0F;
		TrialMaxScore        = 0.0F;
		TrialScore           = 0.0F;
		_scale               = scale;
		TrialConfusionMatrix = new int[n, n];
		EpochConfusionMatrix = new int[n, n];
		ChunkConfusionMatrix = new int[n, n];
		for (var i = 0; i < n; ++i) {
			for (var j = 0; j < n; ++j) { TrialConfusionMatrix[i, j] = EpochConfusionMatrix[i, j] = ChunkConfusionMatrix[i, j] = 0; }
		}
	}


	//----------------------------------------------------------------------------------------------------
	/// <summary> Update The score and his display. </summary>
	/// <param name="value"> The value to add to scorer. </param>
	public void Update(float value)
	{
		TrialMaxScore += _scale;
		if (value < 0) { return; }
		if (value > 1) { value = 1; } // theoretically impossible But Unity
		GlobalScore += value * _scale;
		TrialScore  += value * _scale;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Starts the trial. </summary>
	public void StartTrial() { TrialMaxScore = TrialScore = 0.0F; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Stops the trial. </summary>
	/// <param name="expected"> The expected note. </param>
	/// <param name="real"> The real note. </param>
	/// <param name="trialWise"> The trial wise accuracy note. </param>
	/// <param name="epochWise">the epoch wise accuracy note. </param>
	public void StopTrial(int expected, int real, int trialWise, int epochWise)
	{
		if (expected == -1) { return; }
		if (real != -1) { UpdateMatrix(ChunkConfusionMatrix, expected, real); }
		UpdateMatrix(TrialConfusionMatrix, expected, trialWise);
		UpdateMatrix(EpochConfusionMatrix, expected, epochWise);
	}

	#endregion

	#region Static Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the confusion matrix (in Row what we have, in column what we want). </summary>
	/// <param name="matrix"> Matrix to update. </param>
	/// <param name="expected"> The expected class. </param>
	/// <param name="real"> The real (computed) class. </param>
	private static void UpdateMatrix(int[,] matrix, int expected, int real) { matrix[real, expected]++; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the accuracy from Confusion Matrix. </summary>
	/// <param name="matrix"> The confusion matrix. </param>
	/// <param name="n"> Size of matrix. </param>
	/// <returns> Accuracy in percent. </returns>
	public static float GetAccuracy(int[,] matrix, int n)
	{
		int predictions = 0, good = 0;
		for (var i = 0; i < n; ++i) {
			for (var j = 0; j < n; ++j) {
				predictions += matrix[i, j];
				if (i == j) { good += matrix[i, j]; }
			}
		}
		return (float) (predictions == 0 ? 0.0 : 100.0 * good / predictions);
	}

	#endregion
}
}
