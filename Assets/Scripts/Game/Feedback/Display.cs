﻿using System.Collections.Generic;
using System.Linq;

namespace Game.Feedback
{
/// <summary> List of Feedback's views. </summary>
public enum FeedbackDisplays
{
	/// <summary> The None Feedback View show nothing (no bar and no button hold). </summary>
	None,

	/// <summary> The Positive Feedback View show only the bar and the button holding of expected note of trial. </summary>
	Positive,

	/// <summary> The All Feedback View show all bars and buttons holding. </summary>
	All,

	/// <summary> The Best Feedback View show only the bar and the button holding of best note. </summary>
	Best
}

/// <summary> Base class to Manage Feedback during the Game. </summary>
/// <seealso cref="Feedback"/>
/// <seealso cref="FeedbackDisplays"/>
public class Display
{
	#region Members

	private const float BAR_ORIGIN_Y = BoardManager.VISUAL_SEPARATOR_TOP_Y; //The position of the bottom of the bar.
	private const float MIN_SIZE     = BoardManager.VISUAL_AREA_Y; // The minimum size of the bar (correspond of 1/NoteNumber in probability).
	private const float MAX_SIZE     = BoardManager.GROUND_L - BAR_ORIGIN_Y - MIN_SIZE; // The maximum size of the bar (correspond of 1 in probability).

	private float _activationLimit = 1.0F; // The maximum step of the target.
	private float _factorBias      = 0.0F; // Factor Biasto change scale from probabilities (0 to 1) to sizes
	private int   _noteNumber      = 0;    // The note number

	private float[] _displayed    = null; // Size of bars displayed.
	public  float[] DisplayedUnit = null; // Size of Bars normalized with maximum size is 1

	public FeedbackDisplays Type = FeedbackDisplays.Positive; // The Feedback Display Method (<see cref="FeedbackDisplays"/>).

	#endregion

	#region Common Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the View. </summary>
	public void Init(int noteNumber, float activationLimit)
	{
		_noteNumber      = noteNumber;
		_activationLimit = activationLimit;
		_factorBias      = MAX_SIZE / (1.0F - _activationLimit);
		ResetView();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Update Bar Display. </summary>
	/// <param name="input"> The input array (typically output of classifier with probability of each note). </param>
	/// <param name="expected"> The expected note. </param>
	/// <exception cref="System.ArgumentOutOfRangeException">display - null</exception>
	/// <remarks> Input must be normalized to 1 before. </remarks>
	public void Update(float[] input, int expected)
	{
		UpdateSizes(input, expected);
		BoardManager.UpdateBars(_displayed);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Starts the trial. </summary>
	/// <param name="expected"> The expected note (by default -1 to none expected). </param>
	public void StartTrial(int expected = -1)
	{
		ResetView();
		BoardManager.UpdateVisuals(expected);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Stops the trial. </summary>
	public void StopTrial() { ResetView(); }

	#endregion

	#region Types View Management

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts to display sizes. </summary>
	/// <param name="input"> The input array (typically output of classifier with probability of each note). </param>
	/// <param name="expected"> The expected note. </param>
	/// <exception cref="System.ArgumentOutOfRangeException">display - null</exception>
	/// <remarks> Input must be normalized to 1 before. </remarks>
	private void UpdateSizes(float[] input, int expected)
	{
		_displayed    = ToView(input, expected, _activationLimit, _factorBias);
		DisplayedUnit = ToUnit(_displayed);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Resets the state of objects (bar size to 0 and button unhold). </summary>
	/// <returns> The viewed bar sizes (array of 0). </returns>
	private void ResetView()
	{
		BoardManager.ResetButtons();
		BoardManager.ResetBars();
		BoardManager.ResetVisuals();
		_displayed    = Feedback.ToZero(new float[_noteNumber]);
		DisplayedUnit = Feedback.ToZero(new float[_noteNumber]);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts input probabilities to bar sizes. </summary>
	/// <param name="input"> The input probabilities. </param>
	/// <param name="expected"> The expected note. </param>
	/// <param name="limit"> The limit to valid action. </param>
	/// <param name="bias"> The bias of display (a minimum to be displayed if value is validate). </param>
	/// <returns> The bar sizes in an array. </returns>
	/// <exception cref="System.ArgumentOutOfRangeException">DisplayType - null</exception>
	private float[] ToView(float[] input, int expected, float limit, float bias)
	{
		input = ToBias(input, limit, bias);
		switch (Type) {
			case FeedbackDisplays.None:     return Feedback.ToZero(input);
			case FeedbackDisplays.Positive: return Feedback.ToPositive(input, expected);
			case FeedbackDisplays.All:      return input;
			case FeedbackDisplays.Best:     return Feedback.ToBest(input);
			default:                        throw new System.ArgumentOutOfRangeException(nameof(Type), Type, null);
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Bias the input probabilities.
	/// If the probability is less than the required minimum, it goes to 0, otherwise it is biased to a minimum size up to the maximum size. </summary>
	/// <param name="input">The input probabilities.</param>
	/// <param name="limit"> The limit to valid action. </param>
	/// <param name="bias"> The bias of display (a minimum to be displayed if value is validate). </param>
	/// <returns> The biased bar sizes in an array. </returns>
	private static float[] ToBias(float[] input, float limit, float bias)
	{
		for (var i = 0; i < input.Length; ++i) {
			input[i] = (input[i] - limit) * bias;       // Pass from space 0=>1 to -MAX_SIZE => MAX_SIZE
			if (input[i] < 0) { input[i] =  0; }        // All negative (< to Activation Limit) is set to 0
			else { input[i]              += MIN_SIZE; } // For other add min bar size
		}
		return input;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Views to unit. </summary>
	/// <param name="input"> The input array (bar's sizes). </param>
	/// <returns> The input normalized. </returns>
	/// <remarks> With feedback's display, we remove sometimes the size of a part of notes so we can't just normalized the array. </remarks>
	private static float[] ToUnit(IEnumerable<float> input) { return input.Select(x => x / (MAX_SIZE + MIN_SIZE)).ToArray(); }

	#endregion
}
}
