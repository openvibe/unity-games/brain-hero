﻿using System.Collections.Generic;
using System.Linq;
using FeedbackSettings = Settings.FeedbackSettings;
using FloatList = System.Collections.Generic.List<float[]>;
using NoteList = System.Collections.Generic.List<(int id, int expected, float start, float end)>;

namespace Game.Feedback
{
/// <summary> Base class to Manage Feedback during the Game. </summary>
/// <seealso cref="Game.Feedback.Continuous"/>
/// <seealso cref="Game.Feedback.Discrete"/>
/// <seealso cref="Game.Feedback.Display"/>
/// <seealso cref="Game.Feedback.Score"/>
public class Feedback
{
	#region Members

	public static    int       ExpectedNote { get; private set; } = 0; // The note of current Trial.
	private static   float     _activationLimit = 0;                   // The activation limit for Button and score.
	private readonly FloatList _stack           = new FloatList();     // The Input Stack (probabilities).

	public bool ShowScoreBar = false; // <c>True</c> if we display the score bar between trials.

	private Continuous.Continuous _continuous;
	private Discrete.Discrete     _discrete;
	private Display               _display;
	public  Score                 Score { get; private set; }

	#endregion

	#region Basic Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the Feedback. </summary>
	public void Init()
	{
		if (!SheetPlayer.IsInit || !BoardManager.IsBuild) { return; }
		_activationLimit = 1.0F / BoardManager.NoteNumber;
		_stack.Clear();
		InitSettings((bool) FeedbackSettings.GetSetting("Use Setting"));
		_continuous.Init(BoardManager.NoteNumber);
		_discrete.Init(BoardManager.NoteNumber);
		_display.Init(BoardManager.NoteNumber, _activationLimit);
		Score.Init(BoardManager.NoteNumber);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the settings. </summary>
	/// <param name="useSheetSetting"> if set to <c>true</c> the setting use is setting in the sheet (Game setting otherwise). </param>
	private void InitSettings(bool useSheetSetting)
	{
		Continuous.Types continuousType;
		int              precision;
		Discrete.Types   discreteType;

		Score    = new Score();
		_display = new Display();

		if (useSheetSetting) {
			Score.Type     = (ScoreDisplays) SheetPlayer.Sheet.ScoreDisplay;
			_display.Type  = (FeedbackDisplays) SheetPlayer.Sheet.FeedbackDisplay;
			continuousType = (Continuous.Types) SheetPlayer.Sheet.ContinuousFeedback;
			discreteType   = (Discrete.Types) SheetPlayer.Sheet.DiscreteFeedback;
			ShowScoreBar   = SheetPlayer.Sheet.ShowScoreBar;

			precision = SheetPlayer.Sheet.ContinuousSmoothPrecision;
		}
		else {
			Score.Type     = (ScoreDisplays) FeedbackSettings.GetSetting("Score Display");
			_display.Type  = (FeedbackDisplays) FeedbackSettings.GetSetting("Feedback Display");
			continuousType = (Continuous.Types) FeedbackSettings.GetSetting("Continuous Feedback");
			discreteType   = (Discrete.Types) FeedbackSettings.GetSetting("Discrete Feedback");
			ShowScoreBar   = (bool) FeedbackSettings.GetSetting("Show Bar Score");

			precision = (int) FeedbackSettings.GetSetting("Precision");
		}

		switch (continuousType) {
			case Continuous.Types.None:
				_continuous = new Continuous.None();
				break;
			case Continuous.Types.Raw:
				_continuous = new Continuous.Raw();
				break;
			case Continuous.Types.Smooth:
				_continuous = new Continuous.Smooth();
				break;
			default: throw new System.ArgumentOutOfRangeException();
		}
		_continuous.Precision = precision;

		switch (discreteType) {
			case Discrete.Types.None:
				_discrete = new Discrete.None();
				break;
			case Discrete.Types.GrownTarget:
				_discrete = new Discrete.GrownTarget();
				break;
			case Discrete.Types.Push:
				_discrete = new Discrete.Push();
				break;
			default: throw new System.ArgumentOutOfRangeException();
		}

		_discrete.InitSettings(useSheetSetting, _activationLimit);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset all Lists. </summary>
	public void Destroy()
	{
		_stack.Clear();
		_discrete.Destroy();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates with the specified input array. </summary>
	/// <param name="input"> The input array (typically output of classifier with probability of each note). </param>
	/// <returns> The viewed bar sizes. </returns>
	public void Update(float[] input)
	{
		UpdateStack(input);
		_continuous.Update(_stack);
		_discrete.Update(_stack, ExpectedNote);
		var good = ExpectedNote != -1 && (_activationLimit != 0 && _stack.Last()[ExpectedNote] >= _activationLimit ||
										  _activationLimit == 0 && GetMaxIndex(_stack.Last()) == ExpectedNote);
		//var msg = Stack.Last().ToList().Aggregate("Sample after stack (same normalement) at " + Time.time + " :", (current, i) => current + (" " + i));
		//Debug.Log(msg);
		Score.Update(good ? _stack.Last()[ExpectedNote] : 0);
		_display.Update(_continuous.Value, ExpectedNote);
		HUDManager.UpdateScore((int) Score.GlobalScore); //Update Text but not display if active is set to false
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the stack. </summary>
	/// <param name="input"> The input array (typically output of classifier with probability of each note). </param>
	private void UpdateStack(float[] input)
	{
		if (input.Length != BoardManager.NoteNumber) {
			throw new System.ArithmeticException($"Input has an unexpected size ({input.Length} instead of {BoardManager.NoteNumber}) ");
		}
		_stack.Add(ToUnit(input));
		//var msg = Stack.Last().ToList().Aggregate("Sample Stacked at " + Time.time + " :", (current, i) => current + (" " + i));
		//Debug.Log(msg);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Starts the trial. </summary>
	public void StartTrial(int expectedNote = -1)
	{
		ExpectedNote = expectedNote;
		LogManager.StartTrial(ExpectedNote);
		_discrete.StartTrial();
		_display.StartTrial(ExpectedNote);
		Score.StartTrial();
		_stack.Clear();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Stops the trial. </summary>
	public void StopTrial()
	{
		int trial = TrialWiseResult(), epoch = EpochWiseResult(), real = _discrete.StopTrial(ExpectedNote);
		_display.StopTrial();
		Score.StopTrial(ExpectedNote, real, trial, epoch);
		_stack.Clear();
		if (Score.TrialMaxScore != 0) { HUDManager.UpdateScoreBar(Score.TrialScore / Score.TrialMaxScore); }
		LogManager.StopTrial(ExpectedNote, trial, epoch, Score.TrialScore, Score.TrialMaxScore);
	}

	#endregion

	#region Results

	/// <summary> Get the trial wise result.</summary>
	/// <returns> Class result in trial wise. </returns>
	private int TrialWiseResult() { return GetMaxIndex(Smooth(_stack, 0)); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Get the epoch wise result.</summary>
	/// <returns> Class result in epoch wise. </returns>
	private int EpochWiseResult()
	{
		var res = new int[BoardManager.NoteNumber];
		for (var j = 0; j < BoardManager.NoteNumber; ++j) { res[j] = 0; }
		foreach (var s in _stack) { res[GetMaxIndex(s)]++; }
		return GetMaxIndex(res);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Get the Normalized Bar size displayed. </summary>
	/// <returns> Bar size normalized (maximum is 1 but not the sum if Display is biased. </returns>
	public float[] NormalizedDisplay() { return _display.DisplayedUnit; }

	//----------------------------------------------------------------------------------------------------
	public ScoreDisplays GetScoreDisplay() { return Score.Type; }

	//----------------------------------------------------------------------------------------------------
	public NoteList GetNoteFound() { return _discrete.NoteFound; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Get the probabilities, Normalized Bar size displayed and circle bouton's size. </summary>
	/// <returns> All values is normalized (maximum is 1 but not the sum each time. </returns>
	public float[] GetDataToSend() { return _stack.Last().Concat(NormalizedDisplay()).Concat(_discrete.GetValue()).ToArray(); }

	#endregion

	#region Array Management

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts to unit (sum of array = size). </summary>
	/// <param name="input"> The input array (typically output of classifier with probability of each note). </param>
	/// <param name="size"> The sum expected. </param>
	/// <returns> The input normalized with size. </returns>
	private static float[] ToUnit(float[] input, float size = 1.0F)
	{
		if (input.Sum() == 0) { return ToZero(input); }
		var ratio = size / input.Sum();

		//var msg = input.ToList().Aggregate("Sample at " + Time.time + " :", (current, i) => current + (" " + i));
		//Debug.Log(msg + "\tSum : " + input.Sum() + "\tRatio : " + ratio);

		//var res  = input.Select(x => x * ratio).ToArray();
		//var msg2 = res.ToList().Aggregate("Sample normalized at " + Time.time + " :", (current, i) => current + (" " + i));
		//Debug.Log(msg2);

		return input.Select(x => x * ratio).ToArray();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Set all value to 0. </summary>
	/// <param name="input"> The input. </param>
	/// <returns> The Cleaned Array. </returns>
	public static float[] ToZero(IEnumerable<float> input) { return input.Select(x => 0.0F).ToArray(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Keep Only positive values. </summary>
	/// <param name="input"> The input array (bar's sizes). </param>
	/// <param name="expected"> The expected note we want. </param>
	/// <returns> The viewed bar sizes (array of 0 for all except expected Note). </returns>
	public static float[] ToPositive(float[] input, int expected)
	{
		for (var i = 0; i < input.Length; ++i) {
			if (i != expected) { input[i] = 0; }
		}
		return input;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Keep Only the best. </summary>
	/// <param name="input"> The input array (bar's sizes). </param>
	/// <returns> The viewed bar sizes (array of 0 for all except the best value). </returns>
	public static float[] ToBest(float[] input) { return ToPositive(input, input.ToList().IndexOf(input.Max())); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the index of the maximum value (first occurence). </summary>
	/// <typeparam name="T"> Any numeric type. </typeparam>
	/// <param name="input"> The input. </param>
	/// <returns> The index of the maximum value. </returns>
	public static int GetMaxIndex<T>(T[] input) { return System.Array.IndexOf(input, input.Max()); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the valid elements (if element is over the limit). </summary>
	/// <param name="input"> The input. </param>
	/// <param name="limit"> The limit. </param>
	/// <returns> Bool array with <c>true</c> if element is over the limit, <c>false</c> otherwise. </returns>
	public static bool[] GetValid(IEnumerable<float> input, float limit) { return input.Select(x => x >= limit).ToArray(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Means the specified number of last element. </summary>
	/// <param name="stack"> Stack to Smooth. </param>
	/// <param name="precision"> The number of last element to take (0 to take all element). </param>
	/// <returns> The mean of N last elements in the stack. </returns>
	public static float[] Smooth(FloatList stack, int precision = 5)
	{
		precision = precision == 0 ? stack.Count : precision;    // If we take all values in stack
		var start = System.Math.Max(stack.Count - precision, 0); // If precision > Feedback.Stack.Count()
		var n     = stack[0].Length;
		var res   = new float[n];
		for (var j = 0; j < n; ++j) {
			res[j] = 0;
			for (var i = start; i < stack.Count; ++i) { res[j] += stack[i][j]; }
			res[j] /= precision;
		}
		return res;
	}

	#endregion
}
}
