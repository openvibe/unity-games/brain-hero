﻿using System.Linq;
using FeedbackSettings = Settings.FeedbackSettings;
using FloatList = System.Collections.Generic.List<float[]>;

namespace Game.Feedback.Discrete
{
/// <summary> DownGrade methods for Grown Target Feedback. </summary>
public enum DownModes { Downgrade, Reset, Keep }

/// <summary> Class to Manage Discrete Feedback during the Game with the grown target. </summary>
/// <seealso cref="Discrete" />
public class GrownTarget : Discrete
{
	#region Members

	private const int UP   = 1;  // Step number to increase each time.
	private const int DOWN = -1; // Step number to decrease each time.

	private int _stepMax     = 10; // The maximum step of the target.
	private int _nBack       = 8;  // The number of epoch we take to validate.
	private int _failAllowed = 4;  // The number of fail allowed to validate.

	private DownModes _downMode = DownModes.Downgrade; // The DownGrade Method for Grown Target Feedback (<see cref="DownModes"/>).

	private int[]  _holds       = null; // The number of epoch when Target is at maximum.
	private int[]  _steps       = null; // The step of each Class.
	private int[,] _stack       = null; // The epoch stack validatation.
	private int[]  _resultStack = null; // The number of epoch succeed for each note.
	private int    _idx         = 0;    // The index of stack (for circle array).
	private int    _downStep    = DOWN; // The number of step to downgrade in case of fail.
	private int[]  _sumSteps    = null; // The step's sum of each Class (for mean).
	private int    _nbEpochs    = 0;    // The number of epochs during trial.

	#endregion

	#region Basic Functions

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void Init(int n)
	{
		base.Init(n);
		_holds       = new int[n];
		_steps       = new int[n];
		_sumSteps    = new int[n];
		_stack       = new int[_nBack, n];
		_resultStack = new int[n];
		_downStep    = GetDownStep();
		StartTrial();
	}


	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void InitSettings(bool useSheetSetting, float activationLimit)
	{
		base.InitSettings(useSheetSetting, activationLimit);
		if (useSheetSetting) {
			_stepMax     = SheetPlayer.Sheet.DiscreteGrownTargetSteps;
			_nBack       = SheetPlayer.Sheet.DiscreteGrownTargetWindowSize;
			_failAllowed = SheetPlayer.Sheet.DiscreteGrownTargetFailAllowed;
			_downMode    = (DownModes) SheetPlayer.Sheet.DiscreteGrownTargetDownMode;
		}
		else {
			_stepMax     = (int) FeedbackSettings.GetSetting("Step Max");
			_nBack       = (int) FeedbackSettings.GetSetting("Window Size");
			_failAllowed = (int) FeedbackSettings.GetSetting("Fail Allowed");
			_downMode    = (DownModes) FeedbackSettings.GetSetting("Downgrade Mode");
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void Update(FloatList stack, int expected)
	{
		base.Update(stack, expected);
		UpdateStack(stack);
		var fail = GetFail();
		for (var i = 0; i < NoteNumber; ++i) { UpdateStep(i, fail[i] ? UP : _downStep, expected); }
		_nbEpochs++;
		_idx++;
		if (_idx >= _nBack) { _idx = 0; }
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void StartTrial()
	{
		base.StartTrial();
		for (var i = 0; i < NoteNumber; ++i) {
			_steps[i]       = 0;
			_sumSteps[i]    = 0;
			_holds[i]       = 0;
			_resultStack[i] = 0;
			for (var j = 0; j < _nBack; ++j) { _stack[j, i] = -1; }
		}
		_idx      = 0;
		_nbEpochs = 0;
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override int StopTrial(int expected)
	{
		var msgFinal = "Final Step for each Class :\t|\t\t";
		var msgMean  = "Step Mean for each Class :\t|\t";
		for (var i = 0; i < NoteNumber; ++i) {
			if (_steps[i] == _stepMax) {
				LogManager.WriteLine($"Button {i} released after {_holds[i]} epochs (end of Trial). ");
				AddNote(i, expected);
			}
			msgFinal += $"{_steps[i]}\t\t|\t\t";
			msgMean  += ((float) _sumSteps[i] / _nbEpochs).ToString("F6") + "\t|\t";
		}
		var real = Feedback.GetMaxIndex(_resultStack);
		LogManager.WriteLine($"Grown Target Discrete Feedback (Chunk wise accuracy : Classe {real}): \n\t{msgFinal}\n\t{msgMean}");
		BoardManager.ResetButtons();
		ResetArray();
		return real;
	}

	#endregion

	#region Computes

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the epoch Status.</summary>
	private void UpdateStack(FloatList stack)
	{
		if (ActivationLimit == 0) {
			var idx = Feedback.GetMaxIndex(stack.Last());
			for (var i = 0; i < NoteNumber; ++i) { _stack[_idx, i] = i == idx ? 1 : 0; }
		}
		else {
			var states = Feedback.GetValid(stack.Last(), ActivationLimit);
			for (var i = 0; i < NoteNumber; ++i) { _stack[_idx, i] = states[i] ? 1 : 0; }
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the steps.</summary>
	/// <param name="index"> Index to updates. </param>
	/// <param name="step"> The step to move. </param>
	private void UpdateStep(int index, int step, int expected)
	{
		if (step <= 0) {
			if (_steps[index] == _stepMax) {
				LogManager.WriteLine($"Button {index} released after {_holds[index]} epochs");
				BoardManager.HoldButton(index, false);
				AddNote(index, expected);
			}
			_holds[index] = 0;
		}
		else { _resultStack[index]++; }

		_steps[index] += step;
		if (_steps[index] < 0) { _steps[index] = 0; }
		else if (_steps[index] >= _stepMax) {
			if (_steps[index] == _stepMax) {
				LogManager.WriteLine($"Button {index} Pressed.");
				BoardManager.HoldButton(index, true);
				Times[index] = SheetPlayer.GetCurrentTime();
			}
			else { _steps[index] = _stepMax; }
			_holds[index]++;
		}
		_sumSteps[index] += _steps[index];
		Values[index]    =  (float) _steps[index] / _stepMax;
		BoardManager.UpdateButtonTarget(index, Values[index]);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the fail of each note. </summary>
	/// <returns> Bool array for each note <c>true</c> if note succeed, <c>false</c> otherwise. </returns>
	private bool[] GetFail()
	{
		var fail = new int[NoteNumber];
		for (var i = 0; i < NoteNumber; ++i) { fail[i] = 0; }

		for (var i = 0; i < NoteNumber; ++i) {
			if (_stack[_idx, i] == 1) { continue; } // We valid this epoch in any case the actual epoch is valid
			for (var j = 0; j < _nBack; ++j)        // in other case we check previous epoch
			{
				if (_stack[j, i] == 0) { fail[i]++; } // other case 1 it's a good epoch or -1 we have less than nBack epoch
			}
		}
		return fail.Select(x => x <= _failAllowed).ToArray();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets down step depend of Down Mode (<see cref="DownModes"/>. </summary>
	/// <returns> The number of step to downgrade in case of fail. </returns>
	/// <exception cref="System.ArgumentOutOfRangeException"></exception>
	private int GetDownStep()
	{
		switch (_downMode) {
			case DownModes.Downgrade: return DOWN;
			case DownModes.Reset:     return -_stepMax;
			case DownModes.Keep:      return 0;
			default:                  throw new System.ArgumentOutOfRangeException();
		}
	}

	#endregion
}
}
