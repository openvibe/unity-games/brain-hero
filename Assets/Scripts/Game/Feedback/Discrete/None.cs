﻿using System.Linq;
using FeedbackSettings = Settings.FeedbackSettings;
using FloatList = System.Collections.Generic.List<float[]>;

namespace Game.Feedback.Discrete
{
/// <summary> Class to Manage Discrete Feedback during the Game with Nothing. </summary>
/// <seealso cref="Discrete" />
public class None : Discrete
{
	/// <inheritdoc />
	public override int StopTrial(int expected) { return -1; }
}
}
