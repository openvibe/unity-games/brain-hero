﻿using System.Linq;
using NoteList = System.Collections.Generic.List<(int id, int expected, float start, float end)>;
using FloatList = System.Collections.Generic.List<float[]>;

namespace Game.Feedback.Discrete
{
/// <summary> List of Discrete Types. </summary>
public enum Types
{
	/// <summary> No Discrete Feedback. </summary>
	None,

	/// <summary> The GrownTarget Feedback increase a circle on button and push button when it's needed. </summary>
	GrownTarget,

	/// <summary> The Push Feedback Push button if value exceed limit. </summary>
	Push
}


/// <summary> Base class to Manage Discrete Feedback during the Game. </summary>
/// <seealso cref="Feedback"/>
public abstract class Discrete
{
	#region Members

	/// <summary> List of Note Found. </summary>
	public readonly NoteList NoteFound = new NoteList();

	/// <summary> The time when we begin a note for each note. </summary>
	public float[] Times = null;

	/// <summary> The limit to valid action. </summary>
	protected float ActivationLimit = 1.0F;

	/// <summary> The note number. </summary>
	protected int NoteNumber = 0;

	protected float[] Values = null;

	#endregion

	#region Initialization

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the Discrete Feedback. </summary>
	/// <param name="n"> Note Number. </param>
	public virtual void Init(int n)
	{
		NoteNumber = n;
		NoteFound.Clear();
		Times  = new float[NoteNumber];
		Values = new float[NoteNumber];
		ResetArray();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Reset all Lists. </summary>
	public void Destroy() { NoteFound.Clear(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the settings. </summary>
	/// <param name="useSheetSetting"> if set to <c>true</c> the setting use is setting in the sheet (Game setting otherwise). </param>
	public virtual void InitSettings(bool useSheetSetting, float activationLimit) { ActivationLimit = activationLimit; }

	#endregion

	#region Computes

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the discrete feedback. </summary>
	public virtual void Update(FloatList stack, int expected)
	{
		if (!stack.Any()) { throw new System.ArithmeticException("Can't compute Feedback With Empty Stack. "); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Starts the trial. </summary>
	public virtual void StartTrial() { ResetArray(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Stops the trial. </summary>
	public abstract int StopTrial(int expected);

	//----------------------------------------------------------------------------------------------------
	/// <summary> Adds the founded note to the list (with the expected note, the start time and end time of this note). </summary>
	/// <param name="founded"> The founded note. </param>
	/// <param name="expected"> The expected note. </param>
	protected void AddNote(int founded, int expected)
	{
		NoteFound.Add((founded, expected, Times[founded], SheetPlayer.GetCurrentTime()));
		Times[founded] = 0;
	}

	#endregion

	public float[] GetValue() { return Values; }

	protected void ResetArray()
	{
		for (var i = 0; i < NoteNumber; ++i) {
			Times[i]  = 0.0F;
			Values[i] = 0.0F;
		}
	}
}
}
