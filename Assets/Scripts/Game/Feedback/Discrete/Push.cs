﻿using System.Linq;
using FloatList = System.Collections.Generic.List<float[]>;

namespace Game.Feedback.Discrete
{
/// <summary> Class to Manage Discrete Feedback during the Game with the only push Button. </summary>
/// <seealso cref="Discrete" />
public class Push : Discrete
{
	#region Members

	private int[]  _holds      = null; // The number of epoch when Button is pushed.
	private int[]  _localHolds = null; // The number of epoch when Button is continuous pushed.
	private bool[] _states     = null; // Actual state of buttons.
	private int    _nbEpochs   = 0;    // The number of epochs during trial.

	#endregion

	#region Basic Functions

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void Init(int n)
	{
		base.Init(n);
		_holds      = new int[NoteNumber];
		_localHolds = new int[NoteNumber];
		_states     = new bool[NoteNumber];
		StartTrial();
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void Update(FloatList stack, int expected)
	{
		base.Update(stack, expected);
		var states = Feedback.GetValid(stack.Last(), ActivationLimit);
		for (var i = 0; i < NoteNumber; ++i) {
			if (states[i]) {
				_holds[i]++;
				_localHolds[i]++;
				if (_states[i]) { continue; }
				_states[i] = true;
				Values[i]  = 1;
				Times[i]   = SheetPlayer.GetCurrentTime();
				LogManager.WriteLine($"Button {i} Pressed.");
			}
			else if (_states[i]) {
				LogManager.WriteLine($"Button {i} released after {_localHolds[i]} epochs");
				AddNote(i, expected);
				_states[i]     = false;
				Values[i]      = 0;
				_localHolds[i] = 0;
				Times[i]       = 0;
			}
		}
		_nbEpochs++;
		BoardManager.HoldButtons(_states);
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void StartTrial()
	{
		base.StartTrial();
		for (var i = 0; i < NoteNumber; ++i) {
			_holds[i]      = 0;
			_localHolds[i] = 0;
			_states[i]     = false;
		}
		_nbEpochs = 0;
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override int StopTrial(int expected)
	{
		var msgMean = "Activation percent for each Class :\t|\t";
		for (var i = 0; i < NoteNumber; ++i) {
			if (_states[i]) { LogManager.WriteLine($"Button {i} released after {_localHolds[i]} epochs (end of Trial). "); }
			msgMean += (100.0F * ((float) _holds[i] / _nbEpochs)).ToString("F2") + "\t|\t";
		}
		var real = Feedback.GetMaxIndex(_holds);
		LogManager.WriteLine($"Push Discrete Feedback (Chunk wise accuracy : Classe {real}): \t{msgMean}");
		BoardManager.ResetButtons();
		ResetArray();
		return real;
	}

	#endregion
}
}
