﻿using System.Linq;
using FloatList = System.Collections.Generic.List<float[]>;

namespace Game.Feedback.Continuous
{
/// <summary> List of Continuous Types. </summary>
public enum Types
{
	/// <summary> No Continuous Feedback. </summary>
	None,

	/// <summary> The Raw Feedback show directly what we receive. </summary>
	Raw,

	/// <summary> The Smooth Feedback show the mean of 5 last elements. </summary>
	Smooth
}

/// <summary> Base class to Manage Continuous Feedback during the Game. </summary>
/// <seealso cref="Feedback"/>
public abstract class Continuous
{
	#region Members

	/// <summary> Smooth Precision (how many last value we take (0 for All). </summary>
	public int Precision = 5;

	/// <summary> Current Value of continuous Feedback. </summary>
	public float[] Value { get; private set; } = null;

	#endregion

	#region Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes this instance. </summary>
	/// <param name="n"> Note Number. </param>
	public void Init(int n) { Value = Feedback.ToZero(new float[n]); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates this instance. </summary>
	/// <exception cref="System.ArithmeticException"> Can't compute Feedback With Empty Stack. </exception>
	public void Update(FloatList stack)
	{
		if (!stack.Any()) { throw new System.ArithmeticException("Can't compute Feedback With Empty Stack. "); }
		Value = GetValue(stack);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Get the normalized bar's size depending on the continuous Feedback chosen. </summary>
	/// <param name="stack"> Stack used. </param>
	/// <returns> The normalized Bar's size. </returns>
	protected abstract float[] GetValue(FloatList stack);

	#endregion
}
}
