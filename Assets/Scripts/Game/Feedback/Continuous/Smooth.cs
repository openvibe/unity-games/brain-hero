﻿using FloatList = System.Collections.Generic.List<float[]>;

namespace Game.Feedback.Continuous
{
/// <inheritdoc />
public class Smooth : Continuous
{
	/// <inheritdoc />
	protected override float[] GetValue(FloatList stack) { return Feedback.Smooth(stack, Precision); }
}
}
