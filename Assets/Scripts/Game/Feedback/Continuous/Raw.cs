﻿using System.Linq;
using FloatList = System.Collections.Generic.List<float[]>;

namespace Game.Feedback.Continuous
{
/// <inheritdoc />
public class Raw : Continuous
{
	/// <inheritdoc />
	protected override float[] GetValue(FloatList stack) { return stack.Last(); }
}
}
