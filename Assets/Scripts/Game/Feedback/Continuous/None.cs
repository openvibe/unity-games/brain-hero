﻿using System.Linq;
using FloatList = System.Collections.Generic.List<float[]>;

namespace Game.Feedback.Continuous
{
/// <inheritdoc />
public class None : Continuous
{
	/// <inheritdoc />
	protected override float[] GetValue(FloatList stack) { return Feedback.ToZero(stack.Last()); }
}
}
