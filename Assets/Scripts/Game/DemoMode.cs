﻿using UnityEngine;
using LSL4Unity;

namespace Game
{
/// <summary> List of Demo Modes. </summary>
public enum DemoModes { Cosinus, SmoothRandom }

/// <summary> Class to manage a demo mode on the game. </summary>
/// <seealso cref="UnityEngine.MonoBehaviour" />
public class DemoMode : MonoBehaviour
{
	#region Members

	[SerializeField] private bool      _activeDemoMode = true;                       // Option to set Demo mode or not
	[SerializeField] private string    _streamName     = "Brain Hero Probabilities"; // The LSL stream name
	[SerializeField] private int       _frequencyInHz  = 16;                         // The frequency in hz
	[SerializeField] private DemoModes _mode           = DemoModes.Cosinus;          // The method of demo mode

	[Header("Random Settings")] [SerializeField] [Range(0.01F, 0.99F)]
	private int _smoothSize = 8; // The smooth size of Smooth Random Method

	[SerializeField] private float _goodRate = 0.5F; // The good rate to bias random
	private                  float _failRate = 0;    // The fail rate (1 - _goodRate)

	private StreamOutlet _stream = null; // The stream

	private float _deltaT   = 0; // The delta time (1 / _frequencyInHz)
	private float _currentT = 0; // The current time
	private float _nextT    = 0; // The next time

	private int      _n      = 0;    // The number of modalities (notes)
	private float[]  _buffer = null; // The buffer to send
	private float[,] _stack  = null; // The stack of last values
	private int      _idx    = 0;    // The index of the stack (for circle array)

	private float _limitSuccess = 0; // The limit success (1 / _n)
	private float _coefFail     = 0; // The coef fail of bias random
	private float _coefSuccess  = 0; // The coef success of bias random

	#endregion

	#region Callbacks

	//----------------------------------------------------------------------------------------------------
	/// <summary> Start is run more often than Awake (in case). </summary>
	private void Start()
	{
		_activeDemoMode = (bool) Settings.GeneralSettings.GetSetting("Demo Mode");
		_mode           = (DemoModes) Settings.GeneralSettings.GetSetting("Demo Mode Method");
		_smoothSize     = (int) Settings.GeneralSettings.GetSetting("Demo Mode Smooth Size");
		_goodRate       = (float) Settings.GeneralSettings.GetSetting("Demo Mode Good Rate");

		if (!_activeDemoMode) { return; }
		_n = Settings.SettingsManager.Sheet.Notes.Count;
		var info = new StreamInfo(_streamName, "signal", _n, LSL4Unity.LSL.IRREGULAR_RATE, ChannelFormat.Float32, "signal");
		_stream = new StreamOutlet(info);
		_deltaT = 1.0F / _frequencyInHz;
		_nextT  = Time.time;

		_failRate     = 1 - _goodRate;
		_limitSuccess = 1.0F / _n;
		_coefFail     = _limitSuccess / _failRate;
		_coefSuccess  = (1 - _limitSuccess) / _goodRate;

		InitBuffer();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Update at fixed Time. </summary>
	private void FixedUpdate()
	{
		if (!_activeDemoMode) { return; }
		_currentT = Time.time;
		if (_nextT < _currentT) { return; }
		_nextT = _currentT + _deltaT;
		UpdateBuffer();
		_stream.PushSample(_buffer, _currentT); // Send samples with time since first data received from OV
	}

	#endregion

	#region Computes

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the buffers. </summary>
	private void InitBuffer()
	{
		_buffer = new float[_n];
		_stack  = new float[_smoothSize, _n];
		_idx    = 0;
		for (var i = 0; i < _n; ++i) {
			_buffer[i] = 0;
			for (var j = 0; j < _smoothSize; ++j) { _stack[j, i] = 0; }
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the buffer. </summary>
	private void UpdateBuffer()
	{
		if (_mode == DemoModes.Cosinus) { Cosinus(); }
		else { SmoothRandom(); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Cosinus Method for Demo Mode. </summary>
	private void Cosinus()
	{
		for (var i = 0; i < _n; ++i) {
			var coef = (System.Math.Floor(i / 2.0F) + 1) / 4;
			if (i % 2 == 0) { _buffer[i] = (float) System.Math.Abs(System.Math.Cos(coef * _currentT)); }
			else { _buffer[i]            = (float) System.Math.Abs(System.Math.Sin(coef * _currentT)); }
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Smooth random for Demo Mode. </summary>
	private void SmoothRandom()
	{
		// 0 to failrate	((1/N) / (1-GoodRate)) * R
		// failrate to 1	((1-1/N) / (GoodRate)) * (R-(1-GoodRate)) + 1/N
		var random   = Random.value;
		var expected = (random <= _failRate) ? _coefFail * random : (_coefSuccess * (random - _failRate)) + _limitSuccess;
		var rest     = (1 - expected) / (_n - 1);
		for (var i = 0; i < _n; ++i) { _stack[_idx, i] = (i == Feedback.Feedback.ExpectedNote) ? expected : rest; }
		_idx++;
		if (_idx >= _smoothSize) { _idx = 0; }
		StackMean();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Mean of stack for Smooth Random. </summary>
	private void StackMean()
	{
		for (var i = 0; i < _n; ++i) {
			_buffer[i] = 0;
			for (var j = 0; j < _smoothSize; ++j) { _buffer[i] += _stack[j, i]; }
			_buffer[i] /= _smoothSize;
		}
	}

	#endregion
}
}
