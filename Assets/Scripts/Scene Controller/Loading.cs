﻿using UnityEngine;
using Locale = UnityEngine.Localization.Locale;
using LocalizationSettings = UnityEngine.Localization.Settings.LocalizationSettings;

namespace Scene_Controller
{
/// <summary> Loading Scene Controller. </summary>
/// <seealso cref="UnityEngine.MonoBehaviour" />
public class Loading : MonoBehaviour
{
	#region Members

	private TMPro.TMP_Text _progressText = null;
	//private static bool           _localechanged = false;

	#endregion

	#region Callbacks

	//----------------------------------------------------------------------------------------------------
	private void OnEnable()
	{
		_progressText = transform.Find("Loading Text").GetComponent<TMPro.TMP_Text>();
		//if (!LocalizationSettings.InitializationOperation.IsDone) { StartCoroutine(Preload()); }
	}

	//----------------------------------------------------------------------------------------------------
	private void Update()
	{
		//if (!LocalizationSettings.InitializationOperation.IsDone) return;
		//	StartCoroutine(Preload(Settings.SettingsManager.GetLanguage()));
		//if (!_localechanged) return;
		//LocalizationSettings.SelectedLocale = LocalizationSettings.SelectedLocaleAsync.Result;
		_progressText.text = $"Loading resources  : {(int) Settings.Defines.GetProgress()}%";
		if (Settings.Defines.IsResourcesLoaded()) { UnityEngine.SceneManagement.SceneManager.LoadScene("Menu"); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Preloads the specified locale. </summary>
	/// <param name="locale"> The locale. </param>
	/// <returns> Asynchronous IEnumerator. </returns>
	private System.Collections.IEnumerator Preload(Locale locale = null)
	{
		//if (locale != null) { _localechanged = true; }
		var operation = LocalizationSettings.InitializationOperation;

		do {
			// When we first initialize the Selected Locale will not be available however
			// it is the first thing to be initialized and will be available before the InitializationOperation is finished.
			if (locale == null) { locale = LocalizationSettings.SelectedLocaleAsync.Result; }
			_progressText.text = $"{operation.PercentComplete * 100}%";
			yield return null;
		} while (!operation.IsDone);

		if (operation.Status != UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus.Failed) yield break;
		_progressText.text  = operation.OperationException.ToString();
		_progressText.color = Color.red;
	}

	#endregion
}
}
