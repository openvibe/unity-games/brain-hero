using LSL4Unity;
using UnityEngine;
using SheetPlayer = Game.SheetPlayer;
using BoardManager = Game.BoardManager;
using TrackManager = Game.TrackManager;
using SoundManager = Game.SoundManager;
using HUDManager = Game.HUDManager;
using FinalManager = Game.FinalManager;
using LogManager = Game.LogManager;
using Feedback = Game.Feedback.Feedback;
using ScoreDisplays = Game.Feedback.ScoreDisplays;
using Defines = Settings.Defines;

namespace Scene_Controller
{
/// <summary> Game Scene Controller. </summary>
/// <seealso cref="UnityEngine.MonoBehaviour" />
public class Game : MonoBehaviour
{
	#region Members

	[Header("LSL")] [SerializeField] private string         _markerOutletName = "Brain Hero Markers";
	[SerializeField]                 private string         _signalOutletName = "Brain Hero Displayed";
	[SerializeField]                 private LSL.FloatInlet _signalInlet      = null;

	//private static readonly LSLManager LSL   = new LSLManager();
	private StreamOutlet _outletMarker = null, _outletSignal = null;
	private double       _lastTime     = 0.0;
	private bool         _checkLSL     = false;
	private bool         _isStarted    = false;
	private bool         _isFinished   = false;

	// Scripts
	private static BoardManager _boardManager = null;
	private static TrackManager _trackManager = null;
	private static FinalManager _finalManager = null;
	private static SoundManager _soundManager = null;
	private static HUDManager   _hudManager   = null;

	private static readonly Feedback Feedback = new Feedback();

	#endregion

	#region Callbacks

	//----------------------------------------------------------------------------------------------------	
	/// <summary> Awake Callback. </summary>
	private void Awake()
	{
		ResetMembers();
		FindObjects();
		//----- Sheet Management -----
		SheetPlayer.Init();

		//----- Build Game -----
		_boardManager.Build();
		_trackManager.Build();

		//----- Feedback Initialization -----
		Feedback.Init();

		//----- Visual and Sound Initialization -----
		_soundManager.Init();
		_hudManager.Init();
		_finalManager.Init();

		//----- LSL Initialization -----
		LSLStart();

		if (Feedback.GetScoreDisplay() == ScoreDisplays.Always) { HUDManager.SetActiveScore(); }
		else { HUDManager.SetActiveScore(false); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> OnDestroy Callback. </summary>
	private void OnDestroy()
	{
		ResetMembers();
		Feedback.Destroy();
		BoardManager.Destroy();
		TrackManager.Destroy();
		SheetPlayer.Destroy();
		SoundManager.Destroy();
		HUDManager.Destroy();
		FinalManager.Destroy();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> FixedUpdate Callback. </summary>
	private void FixedUpdate()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) { UnityEngine.SceneManagement.SceneManager.LoadScene("Menu"); }
		if (_isFinished) { return; }  // After a Loop Finished we stop
		if (SheetPlayer.IsFinished()) // We have finished the Sheet with Beat Experiment Stop
		{
			_isFinished = true;
			LogManager.WriteEnder(Feedback.Score);
			if ((bool) Settings.FeedbackSettings.GetSetting("Show Final Screen")) {
				_hudManager.SetActive(false);
				_finalManager.SetActive();
				FinalManager.MakeSheet(Feedback.GetNoteFound());
				_finalManager.TakeScreenshot();
			}
			else if (Feedback.GetScoreDisplay() != ScoreDisplays.None) { HUDManager.SetActiveScore(); }
			else { _finalManager.SetActiveBackground(); }
			return;
		}
		if (!_signalInlet.IsSolved()) { _signalInlet.ResolveStream(); } // Try to resolve an other time
		if (!_signalInlet.IsSolved()) { return; }                       // Always unsolved
		if (!_isStarted)                                                // First loop with signal inlet found
		{
			// Create Matrix Stream
			var info = new StreamInfo(_signalOutletName, "signal", _signalInlet.ChannelCount * 3, LSL4Unity.LSL.IRREGULAR_RATE, ChannelFormat.Float32, "signal");
			_outletSignal = new StreamOutlet(info);
			//Debug.Log($"Creating Stream : Name = {info.Name()}, Type = {info.Type()}, Channel Count = {info.ChannelCount()}, Format = {info.ChannelFormat()}");
			_isStarted = true;
			ChangeBeat(SheetPlayer.Start());
			LogManager.WriteHeader();
		}
		else {
			HUDManager.UpdateTime(SheetPlayer.GetCurrentTime());
			ChangeBeat(SheetPlayer.Update());
		}
		SignalReceive();
	}

	#endregion

	#region Initialization

	//----------------------------------------------------------------------------------------------------	
	/// <summary> Resets all members. </summary>
	private void ResetMembers()
	{
		_outletMarker = null;
		_outletSignal = null;
		_lastTime     = 0.0;
		_checkLSL     = false;

		_isStarted  = false;
		_isFinished = false;

		_boardManager = null;
		_trackManager = null;
		_finalManager = null;
		_soundManager = null;
		_hudManager   = null;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Find Script On GameObjects. </summary>
	private void FindObjects()
	{
		_boardManager = transform.Find("Board").GetComponent<BoardManager>();
		_trackManager = transform.Find("Track").GetComponent<TrackManager>();
		_finalManager = transform.Find("Final Screen").GetComponent<FinalManager>();
		_soundManager = transform.Find("Audio Source").GetComponent<SoundManager>();
		_hudManager   = transform.Find("HUD").GetComponent<HUDManager>();
	}

	#endregion

	#region Beat Management

	//----------------------------------------------------------------------------------------------------
	/// <summary> Change the behavior with the beat (change central Image, allow feedback, pley sound...). </summary>
	/// <param name="beat"> The beat. </param>
	private void ChangeBeat(Music.Beat beat)
	{
		if (beat == null) { return; }
		switch (beat.Type) {
			case "Trial Start":
				HUDManager.UpdateCentralImage(null);
				HUDManager.SetActiveScoreBar(false);
				if (Feedback.GetScoreDisplay() != ScoreDisplays.Always) { HUDManager.SetActiveScore(false); }
				break;

			case "Cross":
				HUDManager.UpdateCentralImage(beat.Duration == 0.0F ? null : Defines.Crosses[SheetPlayer.Sheet.CrossIdx].Object);
				break;

			case "Visual":
				if (beat.Duration == 0.0F || beat.VisualIdx == -1) {
					HUDManager.UpdateCentralImage(null);
					return;
				}
				HUDManager.UpdateCentralImage(Defines.Visuals[beat.VisualIdx].Object);
				SoundManager.Play(beat.SoundIdx);
				break;

			case "Trial Stop":
				_checkLSL = false;
				Feedback.StopTrial();
				// Send samples with time since first data received from OV
				_outletSignal.PushSample(Feedback.NormalizedDisplay(), SheetPlayer.GetCurrentTime());
				if (Feedback.GetScoreDisplay() == ScoreDisplays.TrialEnd) { HUDManager.SetActiveScore(); }
				if (Feedback.ShowScoreBar) { HUDManager.SetActiveScoreBar(); }
				break;

			case "Experiment Stop":
				LogManager.StopExperiment();
				break;

			//case "Train":
			default:
				HUDManager.UpdateCentralImage(null);
				if (beat.Type.Substring(0, 4) == "Note") {
					_checkLSL = true;
					Feedback.StartTrial(beat.NoteIdx);
				}
				break;
		}
		// For Visual and Cross beat, if time is 0 it's skipped so no stimulations, trial start/stop have a time of 0 but stimulation is send
		// Send Stimulation with unity time (just in case, we avoid sending back the experiment stop stimulation)
		if (beat.Stimulation != -1) { _outletMarker.PushSample(new[] { beat.Stimulation }, SheetPlayer.GetCurrentTime()); }
	}

	#endregion

	#region LSL Management

	//----------------------------------------------------------------------------------------------------
	/// <summary> Start LSL Stream for outlet Marker and try to find signal inlet. </summary>
	private void LSLStart()
	{
		var info = new StreamInfo(_markerOutletName, "Marker", 1, LSL4Unity.LSL.IRREGULAR_RATE, ChannelFormat.Int32, "stimulation");
		_outletMarker = new StreamOutlet(info);
		//Debug.Log($"Creating Stream : Name = {info.Name()}, Type = {info.Type()}, Channel Count = {info.ChannelCount()}, Format = {info.ChannelFormat()}");
		_signalInlet.ResolveStream();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Receive the Signal if we have found Stream in two direction, if it's usefull (in feedback mode for example) and if stream is updated. </summary>
	private void SignalReceive()
	{
		if (!_checkLSL || _signalInlet.LastSample == null || _signalInlet.LastSample.Length <= 0 || !_signalInlet.IsUpdate(_lastTime)) { return; }
		_lastTime = _signalInlet.Time;

		// Manage Visualization
		Feedback.Update(_signalInlet.LastSample);

		//Send to OV
		_outletSignal.PushSample(Feedback.GetDataToSend(), SheetPlayer.GetCurrentTime()); // Send samples with time since first data received from OV

		//Log
		//var msg = _signalInlet.LastSample.ToList().Aggregate("Sample at " + Time.time + " :", (current, i) => current + (" " + i)) + " With OV Time : " + _signalInlet.Time;
		//Debug.Log(msg);
	}

	#endregion
}
}
