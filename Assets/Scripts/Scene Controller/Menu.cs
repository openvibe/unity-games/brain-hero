﻿using UnityEngine;
using AudioMixer = UnityEngine.Audio.AudioMixer;
using SettingsManager = Settings.SettingsManager;
using GeneralSettings = Settings.GeneralSettings;
using ProceduralSettings = Settings.ProceduralSettings;
using FeedbackSettings = Settings.FeedbackSettings;
using SheetSelector = Settings.SheetSelector;
using LocalizationSettings = UnityEngine.Localization.Settings.LocalizationSettings;
using ObjectDictionnary = System.Collections.Generic.Dictionary<string, UnityEngine.GameObject>;
using FeedbackDisplays = Game.Feedback.FeedbackDisplays;

namespace Scene_Controller
{
/// <summary> Loading Scene Controller. </summary>
/// <seealso cref="UnityEngine.MonoBehaviour" />
public class Menu : MonoBehaviour
{
	#region Members

	[SerializeField] private AudioMixer _audio = null;

	private readonly ObjectDictionnary _mainParts    = new ObjectDictionnary();
	private readonly ObjectDictionnary _settingParts = new ObjectDictionnary();

	#endregion

	#region Callbacks

	//----------------------------------------------------------------------------------------------------
	private void Start()
	{
		//Main Parts
		foreach (var s in Settings.Defines.MenuPartsNames) { _mainParts.Add(s, transform.Find(s).gameObject); }
		SetMainUI();

		//Setting Parts
		foreach (var s in Settings.Defines.SettingspartsNames) {
			_settingParts.Add(s, _mainParts["Settings"].transform.Find("Main Place/Setting Place/" + s).gameObject);
		}
		SettingsManager.InitUI(_settingParts);
		SetGeneralUI();

		//Sheet Selector Parts
		SheetSelector.Init(_mainParts[Settings.Defines.MenuPartsNames[2]]);

		Debug.Log("Locale : " + LocalizationSettings.SelectedLocale);
	}

	#endregion

	#region UIs Activation

	//----------------------------------------------------------------------------------------------------
	public void SetMainUI()          { SetPartUI(true,  false, false); }
	public void SetSettingsUI()      { SetPartUI(false, true,  false); }
	public void SetSheetSelectorUI() { SetPartUI(false, false, true); }
	public void SetGeneralUI()       { SetSettingtUI(true,  false, false, false); }
	public void SetProceduralUI()    { SetSettingtUI(false, true,  false, false); }
	public void SetFeedbackUI()      { SetSettingtUI(false, false, true,  false); }
	public void SetInstructionsUI()  { SetSettingtUI(false, false, false, true); }

	//----------------------------------------------------------------------------------------------------
	private void SetPartUI(bool main, bool setting, bool sheetSelector)
	{
		_mainParts[Settings.Defines.MenuPartsNames[0]].SetActive(main);
		_mainParts[Settings.Defines.MenuPartsNames[1]].SetActive(setting);
		_mainParts[Settings.Defines.MenuPartsNames[2]].SetActive(sheetSelector);
	}

	//----------------------------------------------------------------------------------------------------
	private void SetSettingtUI(bool general, bool procedural, bool feedback, bool instructions)
	{
		_settingParts[Settings.Defines.SettingspartsNames[0]].SetActive(general);
		_settingParts[Settings.Defines.SettingspartsNames[1]].SetActive(procedural);
		_settingParts[Settings.Defines.SettingspartsNames[2]].SetActive(feedback);
		_settingParts[Settings.Defines.SettingspartsNames[3]].SetActive(instructions);
	}

	#endregion

	#region Play Functions

	//----------------------------------------------------------------------------------------------------
	public void PlayPredefMode()     { PlayGame(SetPredefinedSheet()); }
	public void PlayProceduralMode() { PlayGame(SetProceduralSheet()); }
	public void PlayFreeMode()       { PlayGame(SetFreeSheet()); }

	//----------------------------------------------------------------------------------------------------
	private static void PlayGame(Music.Sheet sheet)
	{
		SettingsManager.Sheet = sheet;
		UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
	}

	//----------------------------------------------------------------------------------------------------
	public void QuitGame()
	{
		Debug.Log("QUIT!");
		Application.Quit();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Create A procedural sheet with settings and save it in <c>00-Procedural.json</c> file. </summary>
	/// <returns> the Created Sheet. </returns>
	private static Music.Sheet SetProceduralSheet()
	{
		var sheet = new Music.Sheet
					{
						ShowFinalScreen                = (bool) FeedbackSettings.GetSetting("Show Final Screen"),
						ShowScoreBar                   = (bool) FeedbackSettings.GetSetting("Show Bar Score"),
						ScoreDisplay                   = (int) FeedbackSettings.GetSetting("Score Display"),
						FeedbackDisplay                = (int) FeedbackSettings.GetSetting("Feedback Display"),
						ContinuousFeedback             = (int) FeedbackSettings.GetSetting("Continuous Feedback"),
						DiscreteFeedback               = (int) FeedbackSettings.GetSetting("Discrete Feedback"),
						ContinuousSmoothPrecision      = (int) FeedbackSettings.GetSetting("Precision"),
						DiscreteGrownTargetSteps       = (int) FeedbackSettings.GetSetting("Step Max"),
						DiscreteGrownTargetWindowSize  = (int) FeedbackSettings.GetSetting("Window Size"),
						DiscreteGrownTargetFailAllowed = (int) FeedbackSettings.GetSetting("Fail Allowed"),
						DiscreteGrownTargetDownMode    = (int) FeedbackSettings.GetSetting("Downgrade Mode"),
						Speed                          = (float) ProceduralSettings.GetSetting("Speed"),
						Sound                          = (bool) ProceduralSettings.GetSetting("Note Sound"),
						CrossIdx                       = (int) GeneralSettings.GetSetting("Cross")
					};

		var notes = SettingsManager.GetNotes();
		var count = System.Math.Min((int) ProceduralSettings.GetSetting("Note Number"), notes.Count);
		for (var i = 0; i < count; ++i) { sheet.AddNote(notes[i]); }

		sheet.GenerateProcedural((int) ProceduralSettings.GetSetting("Trial Per Note"), (float) ProceduralSettings.GetSetting("Cross Duration"),
								 (float) ProceduralSettings.GetSetting("Note Duration"), (float) ProceduralSettings.GetSetting("Feedback Duration Min"),
								 (float) ProceduralSettings.GetSetting("Feedback Duration Max"), (float) ProceduralSettings.GetSetting("Rest Duration Min"),
								 (float) ProceduralSettings.GetSetting("Rest Duration Max"));
		sheet.Save("00-Procedural.json");
		return sheet;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Create A Freestyle sheet with settings and save it in <c>00-Free.json</c> file. </summary>
	/// <returns> the Created Sheet. </returns>
	private static Music.Sheet SetFreeSheet()
	{
		// Except for the Duration we use Procedural information for Free Run
		var sheet = new Music.Sheet
					{
						ShowFinalScreen                = (bool) FeedbackSettings.GetSetting("Show Final Screen"),
						ShowScoreBar                   = (bool) FeedbackSettings.GetSetting("Show Bar Score"),
						ScoreDisplay                   = (int) FeedbackSettings.GetSetting("Score Display"),
						FeedbackDisplay                = (int) FeedbackDisplays.All,
						ContinuousFeedback             = (int) FeedbackSettings.GetSetting("Continuous Feedback"),
						DiscreteFeedback               = (int) FeedbackSettings.GetSetting("Discrete Feedback"),
						ContinuousSmoothPrecision      = (int) FeedbackSettings.GetSetting("Precision"),
						DiscreteGrownTargetSteps       = (int) FeedbackSettings.GetSetting("Step Max"),
						DiscreteGrownTargetWindowSize  = (int) FeedbackSettings.GetSetting("Window Size"),
						DiscreteGrownTargetFailAllowed = (int) FeedbackSettings.GetSetting("Fail Allowed"),
						DiscreteGrownTargetDownMode    = (int) FeedbackSettings.GetSetting("Downgrade Mode"),
						Speed                          = (float) ProceduralSettings.GetSetting("Speed"),
						Sound                          = (bool) ProceduralSettings.GetSetting("Note Sound"),
						CrossIdx                       = (int) GeneralSettings.GetSetting("Cross")
					};

		var notes = SettingsManager.GetNotes();
		var count = System.Math.Min((int) ProceduralSettings.GetSetting("Note Number"), notes.Count);
		for (var i = 0; i < count; ++i) { sheet.AddNote(notes[i]); }

		sheet.GenerateFree((float) ProceduralSettings.GetSetting("Cross Duration"), (float) GeneralSettings.GetSetting("Free Duration"),
						   (float) GeneralSettings.GetSetting("Free Rest"), (int) GeneralSettings.GetSetting("Free Steps"));
		sheet.Save("00-Free.json");
		return sheet;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Get the selected predefined Sheet. </summary>
	/// <returns> the selected Sheet. </returns>
	private static Music.Sheet SetPredefinedSheet() { return SheetSelector.GetSheet(); }

	#endregion

	#region Settings Functions

	//----------------------------------------------------------------------------------------------------
	public void ResetSettings()
	{
		foreach (var p in Settings.Defines.SettingspartsNames) {
			if (_settingParts[p].activeSelf) { SettingsManager.ResetSettings(p); }
		}
	}

	//----------------------------------------------------------------------------------------------------
	public void SaveSettings()
	{
		foreach (var p in Settings.Defines.SettingspartsNames) {
			if (_settingParts[p].activeSelf) { SettingsManager.SaveSettings(p); }
		}
	}

	//----------------------------------------------------------------------------------------------------
	public void UpdateContinuousFeedback(int value) { FeedbackSettings.UpdateContinuousFeedback(value); }
	public void UpdateDiscreteFeedback(int   value) { FeedbackSettings.UpdateDiscreteFeedback(value); }

	//----------------------------------------------------------------------------------------------------
	public void NextSheet() { SheetSelector.UpdateSelected(true); }
	public void PrevSheet() { SheetSelector.UpdateSelected(false); }

	//----------------------------------------------------------------------------------------------------
	public void SetVolume(float    volume)       { _audio.SetFloat("Volume", volume); }
	public void SetFullscreen(bool isFullscreen) { SettingsManager.SetFullscreen(isFullscreen); }

	#endregion
}
}
