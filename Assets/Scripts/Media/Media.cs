﻿namespace Media
{
/// <summary> Base Class for Media Loader. </summary>
/// <typeparam name="T"> Type of Media (see inherited class for example). </typeparam>
[System.Serializable] public abstract class Media<T>
{
	/// <summary> Gets the name of the media. </summary>
	/// <value> The name. </value>
	public string Name { get; }

	/// <summary> Gets the path of the media. </summary>
	/// <value> The path. </value>
	public string Path { get; }

	/// <summary> Gets or sets the media object. </summary>
	/// <value> The object. </value>
	public T Object { get; protected set; }

	/// <summary> Gets or sets a value indicating whether the media is loaded. </summary>
	/// <value> <c>true</c> if the media is loaded; otherwise, <c>false</c>. </value>
	public bool IsLoaded { get; protected set; } = false;

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="Media{T}"/> class. </summary>
	/// <param name="name"> The name of the media. </param>
	/// <param name="path"> The path of the media. </param>
	protected Media(string name = "", string path = "")
	{
		Name = name;
		Path = path;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the media. </summary>
	/// <returns> Asynchronous task. </returns>
	public abstract System.Threading.Tasks.Task Load();

	//----------------------------------------------------------------------------------------------------
	/// <summary> Determines whether the media is loaded or blank (path blank). </summary>
	/// <returns> <c>true</c> if the media is loaded or blank (path blank); otherwise, <c>false</c>. </returns>
	public bool IsLoadedOrBlank() { return Path == "" || IsLoaded; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts to string. </summary>
	/// <returns> A <see cref="System.String" /> that represents this instance. </returns>
	public override string ToString() { return Name + " (" + (Path == "" ? "No file attached)" : Path + ", " + (IsLoaded ? "" : "not ") + "loaded)"); }
}
}
