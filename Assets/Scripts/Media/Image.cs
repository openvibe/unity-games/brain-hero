﻿using UnityEngine;

namespace Media
{
/// <summary> Class for Media Image Loader. </summary>
/// <seealso cref="Media.Media{UnityEngine.Sprite}" />
[System.Serializable] public sealed class Image : Media<Sprite>
{
	/// <summary> The Loaded texture of image. </summary>
	public Texture2D Texture = null;

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="T:Media.Image" /> class. </summary>
	/// <inheritdoc />
	public Image(string name = "", string path = "") : base(name, path) { Object = null; }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override async System.Threading.Tasks.Task Load()
	{
		if (Path == "") { return; }
		System.Diagnostics.Debug.Assert(System.IO.File.Exists(Path), "File (" + Path + ") doesn't exist.");
		Texture      = await LoadImage(Path);
		Texture.name = Name;
		Object       = Sprite.Create(Texture, new Rect(0.0f, 0.0f, Texture.width, Texture.height), new Vector2(0.5f, 0.5f));
		Object.name  = Name;
		IsLoaded     = true;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the image. </summary>
	/// <param name="path"> The path of the image. </param>
	/// <returns> Asynchronous task. </returns>
	public static async System.Threading.Tasks.Task<Texture2D> LoadImage(string path)
	{
		var bytes = await LoadImageBytes(path);
		var res   = new Texture2D(2, 2);
		res.LoadImage(bytes);
		return res;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the image bytes. </summary>
	/// <param name="path"> The path of the image. </param>
	/// <returns> Asynchronous task. </returns>
	public static async System.Threading.Tasks.Task<byte[]> LoadImageBytes(string path)
	{
		var stream = System.IO.File.OpenRead(path);
		var bytes  = new byte[stream.Length];
		await stream.ReadAsync(bytes, 0, bytes.Length);
		return bytes;
	}
}
}
