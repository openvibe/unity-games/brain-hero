﻿using WebRequest = UnityEngine.Networking.UnityWebRequest;
using WebRequestMultimedia = UnityEngine.Networking.UnityWebRequestMultimedia;
using DownloadHandlerAudioClip = UnityEngine.Networking.DownloadHandlerAudioClip;
using Debug = UnityEngine.Debug;

namespace Media
{
/// <summary> Class for Media Sound Loader. </summary>
/// <seealso cref="Media.Media{UnityEngine.AudioClip}" />
[System.Serializable] public sealed class Sound : Media<UnityEngine.AudioClip>
{
	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="T:Media.Sound" /> class. </summary>
	/// <inheritdoc />
	public Sound(string name = "", string path = "") : base(name, path) { Object = null; }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override async System.Threading.Tasks.Task Load()
	{
		if (Path == "") { return; }
		Debug.Assert(System.IO.File.Exists(Path),                         "File (" + Path + ") doesn't exist.");
		Debug.Assert(GetAudioType(Path) != UnityEngine.AudioType.UNKNOWN, "This audio file format is not allowed.");
		Object      = await LoadAudio(Path);
		Object.name = Name;
		IsLoaded    = true;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the audio. </summary>
	/// <param name="path"> The path of the sound. </param>
	/// <returns> Asynchronous task. </returns>
	public static async System.Threading.Tasks.Task<UnityEngine.AudioClip> LoadAudio(string path)
	{
		var www    = WebRequestMultimedia.GetAudioClip(path, GetAudioType(path));
		var result = www.SendWebRequest();
		while (!result.isDone) { await System.Threading.Tasks.Task.Delay(100); }

		if (www.result != WebRequest.Result.ConnectionError) { return DownloadHandlerAudioClip.GetContent(www); }
		Debug.Log(www.result);
		return null;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the type of the audio. </summary>
	/// <param name="file"> The file. </param>
	/// <returns> <see cref="UnityEngine.AudioType"/>. </returns>
	public static UnityEngine.AudioType GetAudioType(string file)
	{
		var extension = System.IO.Path.GetExtension(file);
		switch (extension) {
			case ".mp3": return UnityEngine.AudioType.MPEG;
			case ".ogg": return UnityEngine.AudioType.OGGVORBIS;
			case ".wav": return UnityEngine.AudioType.WAV;
		}
		return UnityEngine.AudioType.UNKNOWN;
	}
}
}
