﻿using System.Linq;

namespace Settings
{
/// <summary> Class to manage General Setting (save, load, view) </summary>
public static class GeneralSettings
{
	#region Members

	private const string OPTIONNAL_PATH = "Optionnal Setting/";
	private const string DEMO_MODE_PATH = OPTIONNAL_PATH + "Demo Mode/";

	private static UnityEngine.GameObject _parent = null;

	private static readonly UI.Dropdown[] Dropdowns =
	{
		new UI.Dropdown("Language"),
		new UI.Dropdown("Sound",                   1),
		new UI.Dropdown("Cross",                   1),
		new UI.Dropdown(DEMO_MODE_PATH + "Method", 1)
	};

	private static readonly UI.Toggle[] Toggles =
	{
		new UI.Toggle("Fullscreen"),
		new UI.Toggle("Demo Mode")
	};

	private static readonly UI.Slider[] Sliders =
	{
		new UI.Slider("Volume")
	};

	private static readonly UI.InputFloat[] InputFloats =
	{
		new UI.InputFloat("Free Duration",              300.0F),
		new UI.InputFloat("Free Rest",                  3.0F),
		new UI.InputFloat(DEMO_MODE_PATH + "Good Rate", 0.8F)
	};

	private static readonly UI.InputInt[] InputInts =
	{
		new UI.InputInt("Free Steps",                   4),
		new UI.InputInt(DEMO_MODE_PATH + "Smooth Size", 16)
	};

	#endregion

	#region Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Generic getter for all general setting. </summary>
	/// <param name="name"> The name of the setting. </param>
	/// <returns> The setting in generic Type <see cref="System.IConvertible"/>. </returns>
	public static System.IConvertible GetSetting(string name)
	{
		switch (name) {
			case "Language":              return Dropdowns[0].Value;
			case "Sound":                 return Dropdowns[1].Value;
			case "Cross":                 return Dropdowns[2].Value;
			case "Fullscreen":            return Toggles[0].Value;
			case "Volume":                return Sliders[0].Value;
			case "Free Duration":         return InputFloats[0].Value;
			case "Free Steps":            return InputInts[0].Value;
			case "Free Rest":             return InputFloats[1].Value;
			case "Demo Mode":             return Toggles[1].Value;
			case "Demo Mode Method":      return Dropdowns[3].Value;
			case "Demo Mode Smooth Size": return InputInts[1].Value;
			case "Demo Mode Good Rate":   return InputFloats[2].Value;
		}
		UnityEngine.Debug.Log("Setting with name \"" + name + "\" doesn't exist in general setting.");
		return false;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the general setting UI. </summary>
	/// <param name="parent"> The parent GameObject. </param>
	public static void InitUI(UnityEngine.GameObject parent)
	{
		System.Diagnostics.Debug.Assert(parent != null, "Missing gameObject of General Setting UI.");
		_parent = parent;
		FindGameObjects(_parent.transform);
		UpdateLists();
		SetButtonValues();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Finds the general setting GameObjects. </summary>
	private static void FindGameObjects(UnityEngine.Transform parent)
	{
		foreach (var d in Dropdowns) { d.FindObject(parent.Find(d.Name).gameObject); }
		foreach (var t in Toggles) { t.FindObject(parent.Find(t.Name).gameObject); }
		foreach (var s in Sliders) { s.FindObject(parent.Find(s.Name).gameObject); }
		foreach (var i in InputFloats) { i.FindObject(parent.Find(i.Name).gameObject); }
		foreach (var i in InputInts) { i.FindObject(parent.Find(i.Name).gameObject); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the lists of dropdown in the general setting. </summary>
	private static void UpdateLists()
	{
		Dropdowns[0].UpdateList(Defines.Languages);
		Dropdowns[1].UpdateList(Defines.GetSoundsName().GetRange(1, Defines.Sounds.Count - 1)); // Remove the first
		Dropdowns[2].UpdateList(Defines.GetCrossesName());
		Dropdowns[3].UpdateList(Defines.DemoModes);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Resets the settings. </summary>
	public static void ResetSettings()
	{
		if (!_parent.activeSelf) { return; }
		foreach (var d in Dropdowns) { d.ResetSetting(); }
		foreach (var t in Toggles) { t.ResetSetting(); }
		foreach (var s in Sliders) { s.ResetSetting(); }
		foreach (var i in InputFloats) { i.ResetSetting(); }
		foreach (var i in InputInts) { i.ResetSetting(); }
		SetButtonValues();
		Log("Reset General Settings");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the settings. </summary>
	public static void LoadSettings()
	{
		foreach (var d in Dropdowns) { d.LoadSetting(); }
		foreach (var t in Toggles) { t.LoadSetting(); }
		foreach (var s in Sliders) { s.LoadSetting(); }
		foreach (var i in InputFloats) { i.LoadSetting(); }
		foreach (var i in InputInts) { i.LoadSetting(); }
		Log("Load General Settings");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Saves the settings. </summary>
	public static void SaveSettings()
	{
		if (!_parent.activeSelf) { return; }
		GetButtonValues();
		foreach (var d in Dropdowns) { d.SaveSetting(); }
		foreach (var t in Toggles) { t.SaveSetting(); }
		foreach (var s in Sliders) { s.SaveSetting(); }
		foreach (var i in InputFloats) { i.SaveSetting(); }
		foreach (var i in InputInts) { i.SaveSetting(); }
		Log("Save General Settings");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Sets the value on button. </summary>
	private static void SetButtonValues()
	{
		foreach (var d in Dropdowns) { d.SetButtonValue(); }
		foreach (var t in Toggles) { t.SetButtonValue(); }
		foreach (var s in Sliders) { s.SetButtonValue(); }
		foreach (var i in InputFloats) { i.SetButtonValue(); }
		foreach (var i in InputInts) { i.SetButtonValue(); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the value on button. </summary>
	private static void GetButtonValues()
	{
		foreach (var d in Dropdowns) { d.GetButtonValue(); }
		foreach (var t in Toggles) { t.GetButtonValue(); }
		foreach (var s in Sliders) { s.GetButtonValue(); }
		foreach (var i in InputFloats) { i.GetButtonValue(); }
		foreach (var i in InputInts) { i.GetButtonValue(); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Logs for Debug. </summary>
	/// <param name="first"> The first line of the message before setting list. </param>
	private static void Log(string first = "")
	{
		var begin   = first.Length == 0 ? "" : "\t";
		var message = first.Length == 0 ? "" : first + "\n";
		message = Dropdowns.Aggregate(message, (current,   d) => current + begin + d + "\n");
		message = Toggles.Aggregate(message, (current,     t) => current + begin + t + "\n");
		message = Sliders.Aggregate(message, (current,     s) => current + begin + s + "\n");
		message = InputFloats.Aggregate(message, (current, i) => current + begin + i + "\n");
		message = InputInts.Aggregate(message, (current,   i) => current + begin + i + "\n");
		UnityEngine.Debug.Log(message);
	}

	#endregion
}
} // namespace Settings
