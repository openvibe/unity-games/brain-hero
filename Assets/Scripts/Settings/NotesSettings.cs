﻿using UnityEngine;
using UIList = System.Collections.Generic.List<Settings.UI.InputNote>;

namespace Settings
{
/// <summary> Class to manage Notes Setting (save, load, view). </summary>
public class NotesSettings : MonoBehaviour
{
	#region Members

	private const int MAX_NOTES = 8;

	private static string _file = "";

	private static GameObject _parent = null;

	private static readonly UIList    NotesUIs = new UIList();
	public static           ListNotes Notes { get; } = new ListNotes();

	#endregion

	#region Initialization

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the note setting UI. </summary>
	/// <param name="parent"> The parent GameObject of Notes Setting UI. </param>
	public static void InitUI(GameObject parent)
	{
		System.Diagnostics.Debug.Assert(parent != null, "Missing gameObject of Notes Setting UI.");
		_parent = parent;
		DefaultNotes();
		FindGameObjects();
		LoadSettings();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Finds the note setting GameObjects. </summary>
	private static void FindGameObjects()
	{
		NotesUIs.Clear();
		for (var i = 0; i < MAX_NOTES; ++i) {
			NotesUIs.Add(new UI.InputNote(Notes[i].Name, Notes[i], _parent.transform.Find(Notes[i].Name).gameObject));
			NotesUIs[i].FindObject();
			NotesUIs[i].UpdateLists(Defines.Stimulations, Defines.GetVisualsName(), Defines.GetSoundsName(), Defines.Colors);
			NotesUIs[i].SetButtonValue();
		}
	}

	#endregion

	#region Save/Load/Reset

	//----------------------------------------------------------------------------------------------------
	/// <summary> Resets the settings. </summary>
	public static void ResetSettings()
	{
		if (!_parent.activeSelf) { return; }
		for (var i = 0; i < Notes.Count(); ++i) {
			NotesUIs[i].ResetSetting();
			NotesUIs[i].SetButtonValue();
			Notes[i] = NotesUIs[i].Value;
		}
		Log("Reset Notes Settings");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the settings on specified filename. </summary>
	private static void LoadSettings()
	{
		_file = System.IO.Path.Combine(Defines.Path, "notes.json");
		if (System.IO.File.Exists(_file)) {
			var fileNotes = JsonUtility.FromJson<ListNotes>(System.IO.File.ReadAllText(_file));
			for (var i = 0; i < fileNotes.Count(); ++i) {
				Notes[i] = fileNotes[i];
				NotesUIs[i].SetValue(Notes[i]);
				NotesUIs[i].SetButtonValue();
			}
		}
		Log("Load Notes Settings");
	}


	//----------------------------------------------------------------------------------------------------
	/// <summary> Saves the settings on specified filename. </summary>
	public static void SaveSettings()
	{
		if (!_parent.activeSelf) { return; }
		Debug.Log("Save in File : " + _file);
		var toWrite = new ListNotes(); // To Write Only Activate Notes
		for (var i = 0; i < MAX_NOTES; ++i) {
			NotesUIs[i].GetButtonValue();
			Notes[i] = NotesUIs[i].Value;
			toWrite.Add(Notes[i]);
		}

		System.IO.File.WriteAllText(_file, JsonUtility.ToJson(toWrite, true));
		Log("Save Notes Settings");
	}

	#endregion

	#region Misc

	//----------------------------------------------------------------------------------------------------
	private static void DefaultNotes()
	{
		Notes.Clear();
		for (var i = 0; i < MAX_NOTES; ++i) { Notes.Add(new Note("Instruction " + (i + 1).ToString("00"), i + 1, i + 1, 0, i)); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Logs for Debug. </summary>
	/// <param name="first"> The first line of the message before setting list. </param>
	private static void Log(string first = "") { Debug.Log((first.Length == 0 ? "" : first + "\n") + Notes); }

	#endregion
}
} // namespace Settings
