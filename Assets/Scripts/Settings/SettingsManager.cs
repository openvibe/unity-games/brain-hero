﻿using System.Collections.Generic;
using UnityEngine;
using AudioMixer = UnityEngine.Audio.AudioMixer;
using LocalizationSettings = UnityEngine.Localization.Settings.LocalizationSettings;
using Locale = UnityEngine.Localization.Locale;
using ObjectDictionnary = System.Collections.Generic.Dictionary<string, UnityEngine.GameObject>;

namespace Settings
{
/// <summary> Class to manage Settings. </summary>
/// <remarks> This Class isn't destroy during scene changement. </remarks>
public class SettingsManager : MonoBehaviour
{
	#region Members

	[SerializeField] private AudioMixer _audio = null;

	private static SettingsManager _instance = null;
	public static  Music.Sheet     Sheet     = null;

	#endregion

	#region Initialization

	//----------------------------------------------------------------------------------------------------
	private void Awake()
	{
		if (_instance != null) { Destroy(gameObject); }
		else {
			_instance = this;
			DontDestroyOnLoad(gameObject);
			LoadSettings(); //Loading independant of UIs Object
		}
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary>Initializes the setting UI.</summary>
	/// <param name="settingParts"> Setting part parent GameObject (General, Procedural, Feedback, Instruction). </param>
	/// <param name="noteprefab"> Note setting UI prefab. </param>
	public static void InitUI(ObjectDictionnary settingParts = null)
	{
		if (settingParts == null) { return; }
		if (settingParts["General"] != null) { GeneralSettings.InitUI(settingParts["General"]); }
		if (settingParts["Procedural"] != null) { ProceduralSettings.InitUI(settingParts["Procedural"]); }
		if (settingParts["Feedback"] != null) { FeedbackSettings.InitUI(settingParts["Feedback"]); }
		if (settingParts["Instructions"] != null) { NotesSettings.InitUI(settingParts["Instructions"]); }
	}

	#endregion

	#region Getter/Setter

	//----------------------------------------------------------------------------------------------------
	/// <summary> Generic getter for all setting. </summary>
	/// <param name="part"> The Setting Main part (General or Procedural). </param>
	/// <param name="setting"> The name of the setting. </param>
	/// <returns> The setting in generic Type <see cref="System.IConvertible"/>. </returns>
	public static System.IConvertible GetSetting(string part, string setting)
	{
		switch (part) {
			case "General":    return GeneralSettings.GetSetting(setting);
			case "Procedural": return ProceduralSettings.GetSetting(setting);
			case "Feedback":   return FeedbackSettings.GetSetting(setting);
		}
		Debug.Log("Setting part with name \"" + part + "\" doesn't exist.");
		return false;
	}

	//----------------------------------------------------------------------------------------------------
	private       void SetVolume(float    volume)       { _audio.SetFloat("Volume", volume); }
	public static void SetFullscreen(bool isFullscreen) { Screen.fullScreen = isFullscreen; }

	//----------------------------------------------------------------------------------------------------
	public static void   SetLanguage(int language) { LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[language]; }
	public static Locale GetLanguage()             { return LocalizationSettings.AvailableLocales.Locales[(int) GeneralSettings.GetSetting("Language")]; }

	//----------------------------------------------------------------------------------------------------
	public static List<Note> GetNotes() { return NotesSettings.Notes.Notes; }

	#endregion

	#region Save/Load/Reset

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the settings. </summary>
	private void LoadSettings()
	{
		Defines.Init();
		GeneralSettings.LoadSettings();
		ProceduralSettings.LoadSettings();
		FeedbackSettings.LoadSettings();
		gameObject.AddComponent<NotesSettings>();
		SetVolume((float) GeneralSettings.GetSetting("Volume"));
		SetFullscreen((bool) GeneralSettings.GetSetting("Fullscreen"));
		//SetLanguage((int) GeneralSettings.GetSetting("Language"));
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Resets the settings. </summary>
	public static void ResetSettings(string setting)
	{
		if (setting == Defines.SettingspartsNames[0]) { GeneralSettings.ResetSettings(); }
		else if (setting == Defines.SettingspartsNames[1]) { ProceduralSettings.ResetSettings(); }
		else if (setting == Defines.SettingspartsNames[2]) { FeedbackSettings.ResetSettings(); }
		else if (setting == Defines.SettingspartsNames[3]) { NotesSettings.ResetSettings(); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Saves the settings. </summary>
	public static void SaveSettings(string setting)
	{
		if (setting == Defines.SettingspartsNames[0]) { GeneralSettings.SaveSettings(); }
		else if (setting == Defines.SettingspartsNames[1]) { ProceduralSettings.SaveSettings(); }
		else if (setting == Defines.SettingspartsNames[2]) { FeedbackSettings.SaveSettings(); }
		else if (setting == Defines.SettingspartsNames[3]) { NotesSettings.SaveSettings(); }
	}

	#endregion
}
} // namespace Settings
