﻿using System.Collections.Generic;
using System.Linq;

namespace Settings
{
/// <summary> Class to Store a lot of constant usefull for the game setting. </summary>
public static class Defines
{
	#region Members

	//---------- Path ----------
	private const string RES_PATH     = "Resources";
	private const string CROSSES_PATH = "Crosses";
	private const string SHEETS_PATH  = "Sheets";
	private const string SOUNDS_PATH  = "Sounds";
	private const string VISUALS_PATH = "Visuals";
	public static string Path { get; private set; } = "";

	//---------- Menu Parts Lists ----------
	public static readonly string[] MenuPartsNames     = { "Main", "Settings", "Sheet Selection" };
	public static readonly string[] SettingspartsNames = { "General", "Procedural", "Feedback", "Instructions" };

	//---------- Dropdown Lists ----------
	private static          string[] _string;
	private static readonly string[] StringFR = { "Aucun", "Par Defaut", "Positif", "Complet", "Moyenne" };
	private static readonly string[] StringEN = { "None", "Default", "Positif", "All", "Mean" };

	public static readonly List<string> Languages           = new List<string> { "Français", "English" };
	public static readonly List<string> ScoreDisplay        = System.Enum.GetNames(typeof(Game.Feedback.ScoreDisplays)).ToList();
	public static readonly List<string> FeedbacksDisplay    = System.Enum.GetNames(typeof(Game.Feedback.FeedbackDisplays)).ToList();
	public static readonly List<string> FeedbacksContinuous = System.Enum.GetNames(typeof(Game.Feedback.Continuous.Types)).ToList();
	public static readonly List<string> FeedbacksDiscrete   = System.Enum.GetNames(typeof(Game.Feedback.Discrete.Types)).ToList();
	public static readonly List<string> DownGradeModes      = System.Enum.GetNames(typeof(Game.Feedback.Discrete.DownModes)).ToList();
	public static readonly List<string> DemoModes           = System.Enum.GetNames(typeof(Game.DemoModes)).ToList();

	// For this Change by tuple or other container to save, path, name, prefab object ?
	// Store all Object is maybe useless for the game now we just need sound, visual and cross for the actual Run
	// But maybe it's usefull if we wan't to add a preview of each element
	public static readonly List<Media.Image> Crosses = new List<Media.Image>();
	public static readonly List<Media.Image> Visuals = new List<Media.Image>();
	public static readonly List<Media.Sound> Sounds  = new List<Media.Sound>();
	public static readonly List<Music.Sheet> Sheets  = new List<Music.Sheet>();

	public static readonly Dictionary<string, UnityEngine.Color> ColorsDictionary       = new Dictionary<string, UnityEngine.Color>();
	public static readonly Dictionary<string, int>               StimulationsDictionary = new Dictionary<string, int>();
	public static readonly List<string>                          Colors                 = new List<string>();
	public static readonly List<string>                          Stimulations           = new List<string>();

	#endregion

	#region Initialization

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes with the specified language (French or English). </summary>
	/// <param name="langFR"> If set to <c>true</c> [language fr]. </param>
	public static void Init(bool langFR = true)
	{
		// We can simply make Path.Combine(Application.dataPath, "..", "Resources") but I don't like ".."
		Path    = System.IO.Path.Combine(System.IO.Directory.GetParent(UnityEngine.Application.dataPath).FullName, RES_PATH);
		_string = langFR ? StringFR : StringEN;
		InitColors();
		InitCrosses();
		InitStimulations();
		InitSounds();
		InitVisuals();
		InitSheets();
		LoadResources();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary>Initializes the sounds with Files in path.</summary>
	private static void InitSounds()
	{
		if (Sounds.Count != 0) { return; }
		Sounds.Add(new Media.Sound(_string[1])); //  Default
		Sounds.Add(new Media.Sound(_string[0])); //  None

		string[] ext = { ".wav", ".ogg", ".mp3" };
		var files = System.IO.Directory.GetFiles(System.IO.Path.Combine(Path, SOUNDS_PATH), "*.*").Where(s => ext.Contains(System.IO.Path.GetExtension(s)))
						  .Select(System.IO.Path.GetFullPath).ToList();
		foreach (var file in files) { Sounds.Add(new Media.Sound(GetNameFromFile(file), file)); } // Update List
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the visuals with Files in path. </summary>
	private static void InitVisuals()
	{
		if (Visuals.Count != 0) { return; }
		Visuals.Add(new Media.Image(_string[0])); //  None
		var files = System.IO.Directory.GetFiles(System.IO.Path.Combine(Path, VISUALS_PATH), "*.png").ToList();
		foreach (var file in files) { Visuals.Add(new Media.Image(GetNameFromFile(file), file)); } // Update List
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the crosses with Files in path. </summary>
	private static void InitCrosses()
	{
		if (Crosses.Count != 0) { return; }
		Crosses.Add(new Media.Image(_string[0])); //  None
		var files = System.IO.Directory.GetFiles(System.IO.Path.Combine(Path, CROSSES_PATH), "*.png").ToList();
		foreach (var file in files) { Crosses.Add(new Media.Image(GetNameFromFile(file), file)); } // Update List
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the colors with Unity Primary Color. </summary>
	private static void InitColors()
	{
		if (ColorsDictionary.Count != 0) { return; }

		ColorsDictionary.Add("Menthe", new UnityEngine.Color((float) 076 / 255, (float) 217 / 255, (float) 170 / 255));
		ColorsDictionary.Add("Ciel",   new UnityEngine.Color((float) 076 / 255, (float) 170 / 255, (float) 217 / 255));
		ColorsDictionary.Add("Bleu",   new UnityEngine.Color((float) 076 / 255, (float) 076 / 255, (float) 217 / 255));
		ColorsDictionary.Add("Violet", new UnityEngine.Color((float) 170 / 255, (float) 076 / 255, (float) 217 / 255));
		ColorsDictionary.Add("Fushia", new UnityEngine.Color((float) 217 / 255, (float) 076 / 255, (float) 170 / 255));
		ColorsDictionary.Add("Rouge",  new UnityEngine.Color((float) 217 / 255, (float) 076 / 255, (float) 076 / 255));
		ColorsDictionary.Add("Orange", new UnityEngine.Color((float) 217 / 255, (float) 170 / 255, (float) 076 / 255));
		ColorsDictionary.Add("Vert",   new UnityEngine.Color((float) 076 / 255, (float) 217 / 255, (float) 076 / 255));
		//ColorsDictionary.Add("Jaune",  new UnityEngine.Color((float) 250 / 255, (float) 202 / 255, (float) 021 / 255));
		Colors.AddRange(ColorsDictionary.Keys.ToList());
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the stimulations with a part of <see cref="LSL4Unity.OV.Stimulations"/> in LSL4Unity OV Library. </summary>
	private static void InitStimulations()
	{
		if (StimulationsDictionary.Count != 0) { return; }

		StimulationsDictionary.Add("Left",     (int) LSL4Unity.OV.Stimulations.GDF_LEFT);
		StimulationsDictionary.Add("Right",    (int) LSL4Unity.OV.Stimulations.GDF_RIGHT);
		StimulationsDictionary.Add("Up",       (int) LSL4Unity.OV.Stimulations.GDF_UP);
		StimulationsDictionary.Add("Down",     (int) LSL4Unity.OV.Stimulations.GDF_DOWN);
		StimulationsDictionary.Add("Rest",     (int) LSL4Unity.OV.Stimulations.REST_START);
		StimulationsDictionary.Add("Rotation", (int) LSL4Unity.OV.Stimulations.GDF_ROTATION_CLOCKWISE);
		StimulationsDictionary.Add("Foot",     (int) LSL4Unity.OV.Stimulations.GDF_FOOT);
		StimulationsDictionary.Add("Tongue",   (int) LSL4Unity.OV.Stimulations.GDF_TONGUE);

		StimulationsDictionary.Add("Swallowing", (int) LSL4Unity.OV.Stimulations.GDF_SWALLOWING);
		StimulationsDictionary.Add("Eye Blink",  (int) LSL4Unity.OV.Stimulations.GDF_EYE_BLINK);
		StimulationsDictionary.Add("Artifact",   (int) LSL4Unity.OV.Stimulations.ARTIFACT);

		StimulationsDictionary.Add("Label 00", (int) LSL4Unity.OV.Stimulations.LABEL_00);
		StimulationsDictionary.Add("Label 01", (int) LSL4Unity.OV.Stimulations.LABEL_01);
		StimulationsDictionary.Add("Label 02", (int) LSL4Unity.OV.Stimulations.LABEL_02);
		StimulationsDictionary.Add("Label 03", (int) LSL4Unity.OV.Stimulations.LABEL_03);
		StimulationsDictionary.Add("Label 04", (int) LSL4Unity.OV.Stimulations.LABEL_04);
		StimulationsDictionary.Add("Label 05", (int) LSL4Unity.OV.Stimulations.LABEL_05);
		StimulationsDictionary.Add("Label 06", (int) LSL4Unity.OV.Stimulations.LABEL_06);
		StimulationsDictionary.Add("Label 07", (int) LSL4Unity.OV.Stimulations.LABEL_07);
		StimulationsDictionary.Add("Label 08", (int) LSL4Unity.OV.Stimulations.LABEL_08);

		Stimulations.AddRange(StimulationsDictionary.Keys.ToList());
	}

	private static void InitSheets()
	{
		if (Sheets.Count != 0) { return; }
		var files = System.IO.Directory.GetFiles(System.IO.Path.Combine(Path, SHEETS_PATH), "*.json").ToList();
		foreach (var file in files.Select(System.IO.Path.GetFileName)) {
			var n = GetNumberFromFile(file);
			if (n == 0) { continue; } // Avoid Free and Procedural Sheet with number 0
			var sheet = new Music.Sheet();
			sheet.Load(file);
			Sheets.Add(sheet);
		}
	}

	private static int GetNumberFromFile(string s, char separator = '-')
	{
		var split = System.IO.Path.GetFileNameWithoutExtension(s).Split(new[] { separator }, 2);
		return int.Parse(split[0]);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary>Gets the name of the Object from file.</summary>
	/// <param name="s"> The full filename. </param>
	/// <param name="separator"> The separator between number and name in filename. </param>
	/// <param name="fallback"> The fallback in case of error. </param>
	/// <returns> A clearly name from the File</returns>
	private static string GetNameFromFile(string s, char separator = '-', string fallback = "") { return GetNameFromFile(s, out _, separator, fallback); }

	//----------------------------------------------------------------------------------------------------
	/// <summary>Gets the name of the Object from file.</summary>
	/// <param name="s"> The full filename. </param>
	/// <param name="number"> The number in the begin of filename. </param>
	/// <param name="separator"> The separator between number and name in filename. </param>
	/// <param name="fallback"> The fallback in case of error. </param>
	/// <returns> A clearly name from the File</returns>
	private static string GetNameFromFile(string s, out int number, char separator = '-', string fallback = "")
	{
		var split = System.IO.Path.GetFileNameWithoutExtension(s).Split(new[] { separator }, 2);
		if (split.Length == 2) {
			number = int.Parse(split[0]);
			return split[1];
		}
		number = 0;
		return split.Length == 1 ? split[0] : fallback;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the dropdown with the list. </summary>
	/// <param name="dropdown"> The dropdown. </param>
	/// <param name="list"> The list. </param>
	public static void UpdateDropdownList(TMPro.TMP_Dropdown dropdown, List<string> list)
	{
		dropdown.ClearOptions();
		dropdown.AddOptions(list);
	}

	#endregion

	#region Getter

	//----------------------------------------------------------------------------------------------------
	public static List<string> GetCrossesName() { return Crosses.Select(cross => cross.Name).ToList(); }
	public static List<string> GetVisualsName() { return Visuals.Select(visual => visual.Name).ToList(); }
	public static List<string> GetSoundsName()  { return Sounds.Select(sound => sound.Name).ToList(); }

	//----------------------------------------------------------------------------------------------------
	public static string GetCrossPath(string  filename) { return System.IO.Path.Combine(System.IO.Path.Combine(Path, CROSSES_PATH), filename); }
	public static string GetSheetPath(string  filename) { return System.IO.Path.Combine(System.IO.Path.Combine(Path, SHEETS_PATH),  filename); }
	public static string GetSoundPath(string  filename) { return System.IO.Path.Combine(System.IO.Path.Combine(Path, SOUNDS_PATH),  filename); }
	public static string GetVisualPath(string filename) { return System.IO.Path.Combine(System.IO.Path.Combine(Path, VISUALS_PATH), filename); }

	//----------------------------------------------------------------------------------------------------
	public static bool IsResourcesLoaded()
	{
		return Crosses.All(cross => cross.IsLoadedOrBlank()) && Visuals.All(visual => visual.IsLoadedOrBlank()) && Sounds.All(sound => sound.IsLoadedOrBlank());
	}

	//----------------------------------------------------------------------------------------------------
	public static float GetProgress()
	{
		var total = Crosses.Count + Visuals.Count + Sounds.Count;
		var loaded = Crosses.Count(cross => cross.IsLoadedOrBlank()) + Visuals.Count(visual => visual.IsLoadedOrBlank()) +
					 Sounds.Count(sound => sound.IsLoadedOrBlank());
		return 100.0F * loaded / total;
	}

	//----------------------------------------------------------------------------------------------------
	private static async void LoadResources()
	{
		foreach (var cross in Crosses) { await cross.Load(); }
		foreach (var visual in Visuals) { await visual.Load(); }
		foreach (var sound in Sounds) { await sound.Load(); }
	}

	#endregion
}
} // namespace Settings
