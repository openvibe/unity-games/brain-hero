﻿using System.Linq;
using ObjectList = System.Collections.Generic.List<UnityEngine.GameObject>;
using GameObject = UnityEngine.GameObject;
using Transform = UnityEngine.Transform;
using RectTransform = UnityEngine.RectTransform;

namespace Settings
{
/// <summary> Class to manage Feedback Settings (save, load, view). </summary>
public static class FeedbackSettings
{
	#region Members

	private const string SCROLL_CONTENT_PATH = "Scroll View/Viewport/Content";
	private const string OPTIONNAL_PATH      = "Optionnal Setting/";
	private const string CONTINUOUS_PATH     = OPTIONNAL_PATH + "Continuous/";
	private const string DISCRETE_PATH       = OPTIONNAL_PATH + "Discrete/";
	private const string SMOOTH_PATH         = CONTINUOUS_PATH + "Smooth/";
	private const string GROWN_TARGET_PATH   = DISCRETE_PATH + "GrownTarget/";

	private const int SPACE_SETTING = 20;

	private static          GameObject _parent           = null;
	private static          Transform  _scrollContent    = null;
	private static          Transform  _optionnalContent = null;
	private static readonly ObjectList Optionnals        = new ObjectList { null, null };

	private static readonly UI.Toggle[] Toggles =
	{
		new UI.Toggle("Use Setting"),
		new UI.Toggle("Show Final Screen"),
		new UI.Toggle("Show Bar Score")
	};

	private static readonly UI.Dropdown[] Dropdowns =
	{
		new UI.Dropdown("Score Display",                      1),
		new UI.Dropdown("Feedback Display",                   1),
		new UI.Dropdown("Continuous Feedback",                1),
		new UI.Dropdown("Discrete Feedback",                  1),
		new UI.Dropdown(GROWN_TARGET_PATH + "Downgrade Mode", 0)
	};

	private static readonly UI.InputInt[] InputInts =
	{
		new UI.InputInt(SMOOTH_PATH + "Precision",          5),
		new UI.InputInt(GROWN_TARGET_PATH + "Step Max",     10),
		new UI.InputInt(GROWN_TARGET_PATH + "Window Size",  8),
		new UI.InputInt(GROWN_TARGET_PATH + "Fail Allowed", 4)
	};

	#endregion

	#region Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary> Generic getter for all procedural mode setting. </summary>
	/// <param name="name"> The name of the setting. </param>
	/// <returns> The setting in generic Type <see cref="System.IConvertible"/>. </returns>
	public static System.IConvertible GetSetting(string name)
	{
		switch (name) {
			case "Use Setting":         return Toggles[0].Value;
			case "Show Final Screen":   return Toggles[1].Value;
			case "Show Bar Score":      return Toggles[2].Value;
			case "Score Display":       return Dropdowns[0].Value;
			case "Feedback Display":    return Dropdowns[1].Value;
			case "Continuous Feedback": return Dropdowns[2].Value;
			case "Discrete Feedback":   return Dropdowns[3].Value;
			case "Downgrade Mode":      return Dropdowns[4].Value;
			case "Precision":           return InputInts[0].Value;
			case "Step Max":            return InputInts[1].Value;
			case "Window Size":         return InputInts[2].Value;
			case "Fail Allowed":        return InputInts[3].Value;
		}
		UnityEngine.Debug.Log("Setting with name \"" + name + "\" doesn't exist in Feedback settings.");
		return false;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the procedural mode setting UI. </summary>
	/// <param name="parent"> The parent GameObject. </param>
	public static void InitUI(GameObject parent)
	{
		System.Diagnostics.Debug.Assert(parent != null, "Missing gameObject of Feedback Setting UI.");
		_parent           = parent;
		_scrollContent    = _parent.transform.Find(SCROLL_CONTENT_PATH);
		_optionnalContent = _scrollContent.Find(OPTIONNAL_PATH);
		FindGameObjects(_scrollContent);
		UpdateLists();
		SetButtonValues();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Finds the procedural mode setting GameObjects. </summary>
	private static void FindGameObjects(Transform parent)
	{
		foreach (var d in Dropdowns) { d.FindObject(parent.Find(d.Name).gameObject); }
		foreach (var i in InputInts) { i.FindObject(parent.Find(i.Name).gameObject); }
		foreach (var t in Toggles) { t.FindObject(parent.Find(t.Name).gameObject); }
		Optionnals[0] = parent.Find(SMOOTH_PATH).gameObject;
		Optionnals[1] = parent.Find(GROWN_TARGET_PATH).gameObject;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the lists of dropdown in the procedural mode setting. </summary>
	private static void UpdateLists()
	{
		Dropdowns[0].UpdateList(Defines.ScoreDisplay);
		Dropdowns[1].UpdateList(Defines.FeedbacksDisplay);
		Dropdowns[2].UpdateList(Defines.FeedbacksContinuous);
		Dropdowns[3].UpdateList(Defines.FeedbacksDiscrete);
		Dropdowns[4].UpdateList(Defines.DownGradeModes);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the optionnal setting position. </summary>
	private static void UpdateOptionSettingPosition()
	{
		var height = 0.0F;
		if (Optionnals[0].activeSelf) { height += ((RectTransform) Optionnals[0].transform).rect.height + SPACE_SETTING; }
		if (Optionnals[1].activeSelf) {
			var position = Optionnals[1].transform.localPosition;
			position.y                            =  -height;
			Optionnals[1].transform.localPosition =  position;
			height                                += ((RectTransform) Optionnals[1].transform).rect.height + SPACE_SETTING;
		}
		height += UnityEngine.Mathf.Abs(_optionnalContent.localPosition.y) + SPACE_SETTING;
		_scrollContent.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, UnityEngine.Mathf.Abs(height));
	}


	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the display of optionnal settings for continuous feedback.</summary>
	/// <param name="value"> The value of continuous feedback. </param>
	public static void UpdateContinuousFeedback(int value)
	{
		Optionnals[0].SetActive(value == 2);
		UpdateOptionSettingPosition();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the display of optionnal settings for discrete feedback.</summary>
	/// <param name="value"> The value of discrete feedback. </param>
	public static void UpdateDiscreteFeedback(int value)
	{
		Optionnals[1].SetActive(value == 1);
		UpdateOptionSettingPosition();
	}


	//----------------------------------------------------------------------------------------------------
	/// <summary> Resets the settings. </summary>
	public static void ResetSettings()
	{
		if (!_parent.activeSelf) { return; }
		foreach (var d in Dropdowns) { d.ResetSetting(); }
		foreach (var i in InputInts) { i.ResetSetting(); }
		foreach (var t in Toggles) { t.ResetSetting(); }
		SetButtonValues();
		Log("Reset Feedback Settings");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the settings. </summary>
	public static void LoadSettings()
	{
		foreach (var d in Dropdowns) { d.LoadSetting(); }
		foreach (var i in InputInts) { i.LoadSetting(); }
		foreach (var t in Toggles) { t.LoadSetting(); }
		Log("Load Feedback Settings");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Saves the settings. </summary>
	public static void SaveSettings()
	{
		if (!_parent.activeSelf) { return; }
		GetButtonValues();
		foreach (var d in Dropdowns) { d.SaveSetting(); }
		foreach (var i in InputInts) { i.SaveSetting(); }
		foreach (var t in Toggles) { t.SaveSetting(); }
		Log("Save Feedback Settings");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Sets the value on button. </summary>
	private static void SetButtonValues()
	{
		foreach (var d in Dropdowns) { d.SetButtonValue(); }
		foreach (var i in InputInts) { i.SetButtonValue(); }
		foreach (var t in Toggles) { t.SetButtonValue(); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the value on button. </summary>
	private static void GetButtonValues()
	{
		foreach (var d in Dropdowns) { d.GetButtonValue(); }
		foreach (var i in InputInts) { i.GetButtonValue(); }
		foreach (var t in Toggles) { t.GetButtonValue(); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Logs for Debug. </summary>
	/// <param name="first"> The first line of the message before setting list. </param>
	private static void Log(string first = "")
	{
		var begin   = first.Length == 0 ? "" : "\t";
		var message = first.Length == 0 ? "" : first + "\n";
		message = Toggles.Aggregate(message, (current,   t) => current + begin + t + "\n");
		message = InputInts.Aggregate(message, (current, i) => current + begin + i + "\n");
		message = Dropdowns.Aggregate(message, (current, d) => current + begin + d + "\n");
		UnityEngine.Debug.Log(message);
	}

	#endregion
}
} // namespace Settings
