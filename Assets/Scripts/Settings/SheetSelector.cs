﻿using System.Collections.Generic;
using UnityEngine;

namespace Settings
{
/// <summary> Class to manage Settings. </summary>
/// <remarks> This Class isn't destroy during scene changement. </remarks>
public static class SheetSelector
{
	#region Members

	private static GameObject     _parent   = null;
	private static TMPro.TMP_Text _textArea = null;

	private static readonly List<string> Descriptions = new List<string>();
	private static          int          _current     = 0;

	#endregion

	#region Functions

	//----------------------------------------------------------------------------------------------------
	/// <summary>Initializes the specified parent.</summary>
	/// <param name="parent">The parent.</param>
	public static void Init(GameObject parent)
	{
		FindObjects(parent);
		UpdateDesc();
		UpdateText();
		SetActive(false);
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary>Finds the objects.</summary>
	/// <param name="parent">The parent.</param>
	private static void FindObjects(GameObject parent)
	{
		_parent   = parent;
		_textArea = parent.transform.Find("Carousel/Text").GetComponent<TMPro.TMP_Text>();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary>Updates the desc.</summary>
	private static void UpdateDesc()
	{
		foreach (var sheet in Defines.Sheets) { Descriptions.Add(sheet.DescriptionToString()); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary>Updates the text.</summary>
	private static void UpdateText() { _textArea.text = Descriptions[_current]; }

	//----------------------------------------------------------------------------------------------------
	/// <summary>Updates the selected.</summary>
	/// <param name="next">if set to <c>true</c> [next].</param>
	public static void UpdateSelected(bool next)
	{
		_current += next ? 1 : -1;
		if (_current >= Descriptions.Count) { _current = 0; }
		else if (_current < 0) { _current              = Descriptions.Count - 1; }
		UpdateText();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the sheet selected. </summary>
	/// <returns> the sheet selected. </returns>
	public static Music.Sheet GetSheet() { return Defines.Sheets[_current]; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Activates/Deactivates the GameObject, depending on the given true or false value. </summary>
	/// <param name="activate"> Activate or deactivate the object, where true activates the GameObject and false deactivates the GameObject. </param>
	public static void SetActive(bool activate = true) { _parent.SetActive(activate); }

	#endregion
}
} // namespace Settings
