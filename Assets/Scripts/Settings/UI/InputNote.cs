﻿using StringList = System.Collections.Generic.List<string>;

namespace Settings.UI
{
/// <summary> Class to manage Note Setting UI. </summary>
/// <seealso cref="Settings.UI.SettingUI{Settings.Note, UnityEngine.GameObject}" />
[System.Serializable] public class InputNote : SettingUI<Note, UnityEngine.GameObject>
{
	/// <summary> The name GameObject UI of the note. </summary>
	private TMPro.TMP_InputField _nameUI;

	/// <summary> The stimulation GameObject UI of the note. </summary>
	private TMPro.TMP_Dropdown _stimulationUI;

	/// <summary> The visual GameObject UI of the note. </summary>
	private TMPro.TMP_Dropdown _visualUI;

	/// <summary> The sound GameObject UI of the note. </summary>
	private TMPro.TMP_Dropdown _soundUI;

	/// <summary> The color GameObject UI of the note. </summary>
	private TMPro.TMP_Dropdown _colorUI;

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="InputNote"/> class. </summary>
	/// <inheritdoc />
	public InputNote(string name, Note defaultValue, UnityEngine.GameObject obj = null) : base(name, defaultValue, obj) { }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void FindObject(UnityEngine.GameObject obj)
	{
		Object = obj;
		FindObject();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Finds all the object of Note UI. </summary>
	public void FindObject()
	{
		System.Diagnostics.Debug.Assert(Object != null, "Can't find Object with Null object");
		_nameUI        = Object.transform.Find("Name").GetComponent<TMPro.TMP_InputField>();
		_stimulationUI = Object.transform.Find("Stimulation").GetComponent<TMPro.TMP_Dropdown>();
		_visualUI      = Object.transform.Find("Visual").GetComponent<TMPro.TMP_Dropdown>();
		_soundUI       = Object.transform.Find("Sound").GetComponent<TMPro.TMP_Dropdown>();
		_colorUI       = Object.transform.Find("Color").GetComponent<TMPro.TMP_Dropdown>();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Notes are Saved in an other way. </summary>
	/// <exception cref="System.NotImplementedException"></exception>
	/// <inheritdoc />
	public override void LoadSetting() { throw new System.NotImplementedException(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Notes are Saved in an other way. </summary>
	/// <exception cref="System.NotImplementedException"></exception>
	/// <inheritdoc />
	public override void SaveSetting() { throw new System.NotImplementedException(); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SetButtonValue()
	{
		_nameUI.text         = Value.Name;
		_stimulationUI.value = Value.Stimulation;
		_visualUI.value      = Value.Visual;
		_soundUI.value       = Value.Sound;
		_colorUI.value       = Value.Color;
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void GetButtonValue()
	{
		Value.Name        = _nameUI.text;
		Value.Stimulation = _stimulationUI.value;
		Value.Visual      = _visualUI.value;
		Value.Sound       = _soundUI.value;
		Value.Color       = _colorUI.value;
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override string ToString() { return Value.ToString(); }

	//----------------------------------------------------------------------------------------------------
	public void SetValue(Note value) { Value = value; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the lists of dropdowns. </summary>
	/// <param name="stimulations"> The stimulations. </param>
	/// <param name="visuals"> The visuals. </param>
	/// <param name="sounds"> The sounds. </param>
	/// <param name="colors"> The colors. </param>
	public void UpdateLists(StringList stimulations, StringList visuals, StringList sounds, StringList colors)
	{
		Defines.UpdateDropdownList(_stimulationUI, stimulations);
		Defines.UpdateDropdownList(_visualUI,      visuals);
		Defines.UpdateDropdownList(_soundUI,       sounds);
		Defines.UpdateDropdownList(_colorUI,       colors);
	}
}
}
