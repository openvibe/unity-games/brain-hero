﻿namespace Settings.UI
{
/// <summary> Class to manage Slider Setting UI. </summary>
/// <seealso cref="Settings.UI.SettingUI{float, UnityEngine.UI.Slider}" />
[System.Serializable] public class Slider : SettingUI<float, UnityEngine.UI.Slider>
{
	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	/// <summary> Initializes a new instance of the <see cref="Slider"/> class. </summary>
	public Slider(string name, float defaultValue = 0.0F, UnityEngine.UI.Slider obj = null) : base(name, defaultValue, obj) { }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void FindObject(UnityEngine.GameObject obj) { Object = obj.transform.Find("Slider").GetComponent<UnityEngine.UI.Slider>(); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void LoadSetting() { Value = UnityEngine.PlayerPrefs.GetFloat(Name, Value); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SaveSetting() { UnityEngine.PlayerPrefs.SetFloat(Name, Value); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SetButtonValue() { Object.value = Value; }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void GetButtonValue() { Value = Object.value; }
}
}
