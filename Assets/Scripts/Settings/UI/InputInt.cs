﻿namespace Settings.UI
{
/// <summary> Class to manage Int Setting UI. </summary>
/// <seealso cref="Settings.UI.SettingUI{int, TMPro.TMP_InputField}" />
[System.Serializable] public class InputInt : SettingUI<int, TMPro.TMP_InputField>
{
	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="InputInt"/> class. </summary>
	/// <inheritdoc />
	public InputInt(string name, int defaultValue = 0, TMPro.TMP_InputField obj = null) : base(name, defaultValue, obj) { }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void FindObject(UnityEngine.GameObject obj) { Object = obj.transform.Find("Input").GetComponent<TMPro.TMP_InputField>(); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void LoadSetting() { Value = UnityEngine.PlayerPrefs.GetInt(Name, Value); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SaveSetting() { UnityEngine.PlayerPrefs.SetInt(Name, Value); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SetButtonValue() { Object.text = Value.ToString(NumberFormat); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void GetButtonValue() { Value = int.Parse(Object.text, NumberFormat); }
}
}
