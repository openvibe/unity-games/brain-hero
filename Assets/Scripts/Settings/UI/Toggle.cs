﻿namespace Settings.UI
{
/// <summary> </summary>
/// <seealso cref="Settings.UI.SettingUI{bool, UnityEngine.UI.Toggle}" />
[System.Serializable] public class Toggle : SettingUI<bool, UnityEngine.UI.Toggle>
{
	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	/// <summary> Initializes a new instance of the <see cref="Toggle"/> class. </summary>
	public Toggle(string name, bool defaultValue = true, UnityEngine.UI.Toggle obj = null) : base(name, defaultValue, obj) { }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void FindObject(UnityEngine.GameObject obj) { Object = obj.transform.Find("Toggle").GetComponent<UnityEngine.UI.Toggle>(); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void LoadSetting() { Value = UnityEngine.PlayerPrefs.GetInt(Name, Value ? 1 : 0) != 0; }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SaveSetting() { UnityEngine.PlayerPrefs.SetInt(Name, Value ? 1 : 0); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SetButtonValue() { Object.isOn = Value; }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void GetButtonValue() { Value = Object.isOn; }
}
}
