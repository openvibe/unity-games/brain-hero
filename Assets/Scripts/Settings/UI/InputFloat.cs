﻿namespace Settings.UI
{
/// <summary> </summary>
/// <seealso cref="Settings.UI.SettingUI{float, TMPro.TMP_InputField}" />
[System.Serializable] public class InputFloat : SettingUI<float, TMPro.TMP_InputField>
{
	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="InputFloat"/> class. </summary>
	/// <inheritdoc />
	public InputFloat(string name, float defaultValue = 0.0F, TMPro.TMP_InputField obj = null) : base(name, defaultValue, obj) { }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void FindObject(UnityEngine.GameObject obj) { Object = obj.transform.Find("Input").GetComponent<TMPro.TMP_InputField>(); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void LoadSetting() { Value = UnityEngine.PlayerPrefs.GetFloat(Name, Value); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SaveSetting() { UnityEngine.PlayerPrefs.SetFloat(Name, Value); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SetButtonValue() { Object.text = Value.ToString(NumberFormat); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void GetButtonValue() { Value = float.Parse(Object.text, NumberFormat); }
}
}
