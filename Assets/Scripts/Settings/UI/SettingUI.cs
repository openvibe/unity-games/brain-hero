﻿namespace Settings.UI
{
/// <summary> Base Class For Setting UI. </summary>
/// <typeparam name="T1">The type of the setting value.</typeparam>
/// <typeparam name="T2">The type of the setting UI GameObject.</typeparam>
[System.Serializable] public abstract class SettingUI<T1, T2>
{
	#region Members

	/// <summary> Gets the name of the setting. </summary>
	/// <value> The name of the setting. </value>
	public string Name { get; }

	/// <summary> Gets the default value of the setting. </summary>
	/// <value> The default value of the setting. </value>
	public T1 DefaultValue { get; }

	/// <summary> Gets or sets the value of the setting. </summary>
	/// <value> The value of the setting. </value>
	public T1 Value { get; protected set; }

	/// <summary> Gets or sets the setting UI GameObject. </summary>
	/// <value> The setting UI GameObject. </value>
	public T2 Object { get; protected set; }

	/// <summary> The number format for string conversion. </summary>
	protected System.Globalization.NumberFormatInfo NumberFormat = System.Globalization.CultureInfo.InvariantCulture.NumberFormat;

	#endregion

	#region Constructor

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="SettingUI{T1, T2}"/> class. </summary>
	/// <param name="name"> The name of the setting. </param>
	/// <param name="defaultValue"> The default value. </param>
	/// <param name="obj"> The Setting UI GameObject. </param>
	protected SettingUI(string name, T1 defaultValue, T2 obj)
	{
		Name         = name;
		DefaultValue = defaultValue;
		Value        = defaultValue;
		Object       = obj;
	}

	#endregion

	#region Management

	//----------------------------------------------------------------------------------------------------
	/// <summary> Finds the usefull Setting UI GameObject (where the value is stored). </summary>
	/// <param name="obj">The object.</param>
	/// <remarks> In typicall case, it's a GetComponent of a child of base UI GameObject. </remarks>
	public abstract void FindObject(UnityEngine.GameObject obj);

	//----------------------------------------------------------------------------------------------------
	/// <summary> Resets the setting with the default value. </summary>
	public virtual void ResetSetting() { Value = DefaultValue; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the setting in <see cref="UnityEngine.PlayerPrefs"/>. </summary>
	public abstract void LoadSetting();

	//----------------------------------------------------------------------------------------------------
	/// <summary> Saves the setting in <see cref="UnityEngine.PlayerPrefs"/>. </summary>
	public abstract void SaveSetting();

	//----------------------------------------------------------------------------------------------------
	/// <summary> Sets the value on button UI. </summary>
	public abstract void SetButtonValue();

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the value on button UI. </summary>
	public abstract void GetButtonValue();

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts to string. </summary>
	/// <returns> A <see cref="System.String" /> that represents this instance. </returns>
	public override string ToString() { return Name + " : " + Value + "\t(default : " + DefaultValue + ")"; }

	#endregion
}
}
