﻿namespace Settings.UI
{
//public class InputIntervalFloat : SettingUI<Tuple<float, float>, Tuple<TMPro.TMP_InputField, TMPro.TMP_InputField>>
//public class InputIntervalFloat : SettingUI<(float min, float max), (TMPro.TMP_InputField min, TMPro.TMP_InputField max)>
/// <summary> Class to manage an interval of float Setting UI. </summary>
/// <seealso cref="Settings.UI.SettingUI{float, TMPro.TMP_InputField}" />
[System.Serializable] public class InputIntervalFloat : SettingUI<float, TMPro.TMP_InputField>
{
	#region Members

	/// <summary> The default value maximum </summary>
	public float DefaultValueMax;

	/// <summary> The value maximum </summary>
	public float ValueMax;

	/// <summary> The object maximum </summary>
	public TMPro.TMP_InputField ObjectMax;

	#endregion

	#region Constructor

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="T:Settings.UI.InputIntervalFloat" /> class. </summary>
	/// <param name="name"> The name of the setting. </param>
	/// <param name="minDefault"> The minimum default value. </param>
	/// <param name="maxDefault"> The maximum default value. </param>
	/// <param name="minObj"> The Setting UI GameObject of minimum. </param>
	/// <param name="maxObj"> The Setting UI GameObject of maximum. </param>
	/// <inheritdoc />
	public InputIntervalFloat(string               name, float minDefault = 0.0F, float maxDefault = 0.0F, TMPro.TMP_InputField minObj = null,
							  TMPro.TMP_InputField maxObj = null) : base(name, minDefault, minObj)
	{
		DefaultValueMax = maxDefault;
		ValueMax        = maxDefault;
		ObjectMax       = maxObj;
	}

	#endregion

	#region Management

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void FindObject(UnityEngine.GameObject obj)
	{
		Object    = obj.transform.Find("Input Min").GetComponent<TMPro.TMP_InputField>();
		ObjectMax = obj.transform.Find("Input Max").GetComponent<TMPro.TMP_InputField>();
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void ResetSetting()
	{
		base.ResetSetting();
		ValueMax = DefaultValueMax;
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void LoadSetting()
	{
		Value    = UnityEngine.PlayerPrefs.GetFloat(Name + " Min", Value);
		ValueMax = UnityEngine.PlayerPrefs.GetFloat(Name + " Max", ValueMax);
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SaveSetting()
	{
		UnityEngine.PlayerPrefs.SetFloat(Name + " Min", Value);
		UnityEngine.PlayerPrefs.SetFloat(Name + " Max", ValueMax);
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SetButtonValue()
	{
		Object.text    = Value.ToString(NumberFormat);
		ObjectMax.text = ValueMax.ToString(NumberFormat);
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void GetButtonValue()
	{
		Value    = float.Parse(Object.text,    NumberFormat);
		ValueMax = float.Parse(ObjectMax.text, NumberFormat);
	}

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override string ToString()
	{
		return Name + " Min : " + Value + "\t(default : " + DefaultValue + "),\t" + Name + " Max : " + ValueMax + "\t(default : " + DefaultValueMax + ")";
	}

	#endregion
}
}
