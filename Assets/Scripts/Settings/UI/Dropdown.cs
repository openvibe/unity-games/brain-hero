﻿namespace Settings.UI
{
/// <summary> Class to manage Dropdown Setting UI. </summary>
/// <seealso cref="Settings.UI.SettingUI{int, TMPro.TMP_Dropdown}" />
[System.Serializable] public class Dropdown : SettingUI<int, TMPro.TMP_Dropdown>
{
	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="T:Settings.UI.Dropdown" /> class. </summary>
	/// <inheritdoc />
	public Dropdown(string name, int defaultValue = 0, TMPro.TMP_Dropdown obj = null) : base(name, defaultValue, obj) { }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void FindObject(UnityEngine.GameObject obj) { Object = obj.transform.Find("Dropdown").GetComponent<TMPro.TMP_Dropdown>(); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void LoadSetting() { Value = UnityEngine.PlayerPrefs.GetInt(Name, Value); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SaveSetting() { UnityEngine.PlayerPrefs.SetInt(Name, Value); }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void SetButtonValue() { Object.value = Value; }

	//----------------------------------------------------------------------------------------------------
	/// <inheritdoc />
	public override void GetButtonValue() { Value = Object.value; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Updates the list of the Dropdown. </summary>
	/// <param name="list"> The list. </param>
	public void UpdateList(System.Collections.Generic.List<string> list)
	{
		Object.ClearOptions();
		Object.AddOptions(list);
	}
}
}
