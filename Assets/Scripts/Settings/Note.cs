﻿using System.Linq;

namespace Settings
{
/// <summary> Classe For Note Setting. </summary>
[System.Serializable] public class Note
{
	public string Name;
	public int    Stimulation;
	public int    Visual;
	public int    Sound;
	public int    Color;

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes a new instance of the <see cref="Note"/> class. </summary>
	/// <param name="name">The name of the note.</param>
	/// <param name="stimulation">The stimulation to send.</param>
	/// <param name="visual">The visual to display.</param>
	/// <param name="sound">The sound to play.</param>
	/// <param name="color">The color of the note.</param>
	public Note(string name = "Instruction", int stimulation = 0, int visual = 0, int sound = 0, int color = 0)
	{
		Name        = name;
		Stimulation = stimulation;
		Visual      = visual;
		Sound       = sound;
		Color       = color;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts to string. </summary>
	/// <returns> A <see cref="System.String" /> that represents this instance. </returns>
	public override string ToString()
	{
		return "[Name: " + Name + ", Stimulation: " + Stimulation + ", Visual: " + Visual + ", Sound: " + Sound + ", Color: " + Color + "]";
	}
}

//[System.Serializable] public class Notes : List<Note> doesn't work so need to redefine some functions to simplify utilization
/// <summary> Class for Note LIst (overload needed for JsonUtility). </summary>
[System.Serializable] public class ListNotes
{
	public System.Collections.Generic.List<Note> Notes = new System.Collections.Generic.List<Note>();

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets or sets the <see cref="Note"/> with the specified i (Overloads the square brackets to provide array-like access.). </summary>
	/// <value> The <see cref="Note"/>. </value>
	/// <param name="i">The i.</param>
	/// <returns><see cref="Note"/></returns>
	public Note this[int i] { get => Notes[i]; set => Notes[i] = value; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Overloads List.Count function. </summary>
	/// <returns> int. </returns>
	public int Count() { return Notes.Count; }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Overloads List.Clear function. </summary>
	public void Clear() { Notes.Clear(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Overloads List.Add function. </summary>
	/// <param name="i">The index.</param>
	public void Add(Note i) { Notes.Add(i); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Overloads List.RemoveAt function. </summary>
	/// <param name="i">The index.</param>
	public void RemoveAt(int i) { Notes.RemoveAt(i); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Overloads List.Last function. </summary>
	/// <returns><see cref="Note"/></returns>
	public Note Last() { return Notes.Last(); }

	//----------------------------------------------------------------------------------------------------
	/// <summary> Converts to string. </summary>
	/// <returns> A <see cref="System.String" /> that represents this instance. </returns>
	public override string ToString() { return Notes.Aggregate("\t", (res, n) => res + (n + "\n\t")); }
}
} // namespace Settings
