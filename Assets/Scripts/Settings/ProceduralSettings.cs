﻿using System.Linq;

namespace Settings
{
/// <summary> Class to manage Procedural Mode Setting (save, load, view). </summary>
public static class ProceduralSettings
{
	#region Members

	private static UnityEngine.GameObject _parent = null;

	private static readonly UI.Toggle[] Toggles =
	{
		new UI.Toggle("Note Sound")
	};

	private static readonly UI.InputInt[] InputInts =
	{
		new UI.InputInt("Note Number",    2),
		new UI.InputInt("Trial Per Note", 10)
	};

	private static readonly UI.InputFloat[] InputFloats =
	{
		new UI.InputFloat("Speed",          3.0F),
		new UI.InputFloat("Cross Duration", 1.0F),
		new UI.InputFloat("Note Duration",  3.0F)
	};

	private static readonly UI.InputIntervalFloat[] InputIntervalFloats =
	{
		new UI.InputIntervalFloat("Feedback Duration", 3.75F, 3.75F),
		new UI.InputIntervalFloat("Rest Duration",     1.5F,  3.5F)
	};

	#endregion

	#region Initialization

	//----------------------------------------------------------------------------------------------------
	/// <summary> Initializes the procedural mode setting UI. </summary>
	/// <param name="parent"> The parent GameObject. </param>
	public static void InitUI(UnityEngine.GameObject parent)
	{
		System.Diagnostics.Debug.Assert(parent != null, "Missing gameObject of Procedural Setting UI.");
		_parent = parent;
		//FindGameObjects(_parent.transform.Find("Scroll View/Viewport/Content");
		FindGameObjects(_parent.transform);
		SetButtonValues();
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Finds the procedural mode setting GameObjects. </summary>
	/// <param name="parent"> The parent GameObject. </param>
	private static void FindGameObjects(UnityEngine.Transform parent)
	{
		foreach (var t in Toggles) { t.FindObject(parent.Find(t.Name).gameObject); }
		foreach (var i in InputInts) { i.FindObject(parent.Find(i.Name).gameObject); }
		foreach (var i in InputFloats) { i.FindObject(parent.Find(i.Name).gameObject); }
		foreach (var i in InputIntervalFloats) { i.FindObject(parent.Find(i.Name).gameObject); }
	}

	#endregion

	#region Getter/Setter

	//----------------------------------------------------------------------------------------------------
	/// <summary> Generic getter for all procedural mode setting. </summary>
	/// <param name="name"> The name of the setting. </param>
	/// <returns> The setting in generic Type <see cref="System.IConvertible"/>. </returns>
	public static System.IConvertible GetSetting(string name)
	{
		switch (name) {
			case "Note Sound":            return Toggles[0].Value;
			case "Note Number":           return InputInts[0].Value;
			case "Trial Per Note":        return InputInts[1].Value;
			case "Speed":                 return InputFloats[0].Value;
			case "Cross Duration":        return InputFloats[1].Value;
			case "Note Duration":         return InputFloats[2].Value;
			case "Feedback Duration Min": return InputIntervalFloats[0].Value;
			case "Feedback Duration Max": return InputIntervalFloats[0].ValueMax;
			case "Rest Duration Min":     return InputIntervalFloats[1].Value;
			case "Rest Duration Max":     return InputIntervalFloats[1].ValueMax;
		}
		UnityEngine.Debug.Log("Setting with name \"" + name + "\" doesn't exist in Procedural settings.");
		return false;
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Sets the value on button. </summary>
	private static void SetButtonValues()
	{
		foreach (var t in Toggles) { t.SetButtonValue(); }
		foreach (var i in InputInts) { i.SetButtonValue(); }
		foreach (var i in InputFloats) { i.SetButtonValue(); }
		foreach (var i in InputIntervalFloats) { i.SetButtonValue(); }
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Gets the value on button. </summary>
	private static void GetButtonValues()
	{
		foreach (var t in Toggles) { t.GetButtonValue(); }
		foreach (var i in InputInts) { i.GetButtonValue(); }
		foreach (var i in InputFloats) { i.GetButtonValue(); }
		foreach (var i in InputIntervalFloats) { i.GetButtonValue(); }
	}

	#endregion

	#region Save/Load/Reset

	//----------------------------------------------------------------------------------------------------
	/// <summary> Resets the settings. </summary>
	public static void ResetSettings()
	{
		if (!_parent.activeSelf) { return; }
		foreach (var t in Toggles) { t.ResetSetting(); }
		foreach (var i in InputInts) { i.ResetSetting(); }
		foreach (var i in InputFloats) { i.ResetSetting(); }
		foreach (var i in InputIntervalFloats) { i.ResetSetting(); }
		SetButtonValues();
		Log("Reset Procedural Settings");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Loads the settings. </summary>
	public static void LoadSettings()
	{
		foreach (var t in Toggles) { t.LoadSetting(); }
		foreach (var i in InputInts) { i.LoadSetting(); }
		foreach (var i in InputFloats) { i.LoadSetting(); }
		foreach (var i in InputIntervalFloats) { i.LoadSetting(); }
		Log("Load Procedural Settings");
	}

	//----------------------------------------------------------------------------------------------------
	/// <summary> Saves the settings. </summary>
	public static void SaveSettings()
	{
		if (!_parent.activeSelf) { return; }
		GetButtonValues();
		foreach (var t in Toggles) { t.SaveSetting(); }
		foreach (var i in InputInts) { i.SaveSetting(); }
		foreach (var i in InputFloats) { i.SaveSetting(); }
		foreach (var i in InputIntervalFloats) { i.SaveSetting(); }
		Log("Save Procedural Settings");
	}


	//----------------------------------------------------------------------------------------------------
	/// <summary> Logs for Debug. </summary>
	/// <param name="first"> The first line of the message before setting list. </param>
	private static void Log(string first = "")
	{
		var begin   = first.Length == 0 ? "" : "\t";
		var message = first.Length == 0 ? "" : first + "\n";
		message = Toggles.Aggregate(message, (current,             t) => current + begin + t + "\n");
		message = InputInts.Aggregate(message, (current,           i) => current + begin + i + "\n");
		message = InputFloats.Aggregate(message, (current,         i) => current + begin + i + "\n");
		message = InputIntervalFloats.Aggregate(message, (current, i) => current + begin + i + "\n");
		UnityEngine.Debug.Log(message);
	}

	#endregion
}
} // namespace Settings
