
# Brain Hero

<div style="text-align:center"><img src="Documentation/resources/Visuels/Banniere.png" /></div>  

[![pipeline status](https://gitlab.inria.fr/openvibe/unity-games/brain-hero/badges/master/pipeline.svg)](https://gitlab.inria.fr/openvibe/unity-games/brain-hero/pipelines)
[![Documentation](https://img.shields.io/badge/Documentation-deploy-success)](https://openvibe.gitlabpages.inria.fr/unity-games/brain-hero/)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)  
[![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=inria%3Aopenvibe%3Agames%3ABrainHero%3Amaster&metric=alert_status)](https://sonarqube.inria.fr/sonarqube/dashboard?id=inria:openvibe:games:BrainHero:master)
[![Bugs](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=inria%3Aopenvibe%3Agames%3ABrainHero%3Amaster&metric=bugs)](https://sonarqube.inria.fr/sonarqube/component_measures?id=inria:openvibe:games:BrainHero:master&metric=bugs)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=inria%3Aopenvibe%3Agames%3ABrainHero%3Amaster&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/component_measures?id=inria:openvibe:games:BrainHero:master&metric=vulnerabilities)
[![Code Smells](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=inria%3Aopenvibe%3Agames%3ABrainHero%3Amaster&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/component_measures?id=inria:openvibe:games:BrainHero:master&metric=code_smells)

## Description

This game is based on the principle of rhythm games like Guitar Hero. He uses use LSL to communicate with external software and in our case with [OpenViBE](http://openvibe.inria.fr/).  
In parallel, we create an OpenViBE box derived from the LSL export box for the game. This makes it easier to use the two tools. But it is always possible to use the previous LSL Import/Export box.  
We can use it to gamify Mental Task Brain Computer Interface during research protocol.

## Dependencies

- [LSL4Unity](https://gitlab.inria.fr/openvibe/unity-games/LSL4Unity)
- [Unity Localization 0.10.0-preview](https://docs.unity3d.com/Packages/com.unity.localization@0.10/manual/index.html) ([How to Install](https://docs.unity3d.com/Packages/com.unity.localization@0.10/manual/Installation.html))

## Screenshots

|          |          |
|:--------:|:--------:|
| ![Main Menu](Documentation/resources/2-Main_Menu.png) | ![Setting Example](Documentation/resources/3-Setting_General.png) |
| Main Menu | Setting Example |
| ![In Game Beginning](Documentation/resources/6-InGame_01_Beginning.png) | ![In Game Instruction](Documentation/resources/6-InGame_03_Visual.png) |
| In Game Beginning | In Game Instruction |
| ![In Game Feedback](Documentation/resources/6-InGame_05_Good_Feedback.png) | ![In Game Final Sheet](Documentation/resources/6-Screenshot_07_Final.png) |
| In Game Feedback | In Game Final Sheet |

## OpenViBE Scenario Example

<div style="text-align:center"><img src="Documentation/resources/Scenario.png" /></div>
